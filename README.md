# Problem solving

## Basics

Implementation of basic algorithms, data structures and sort algorithms.

### Algorithms

- A* algorithm
- Dijkstra algorithm
- Rabin Karp algorithm
- Topologican sort

### Data Structures

- AVL Tree
- Binary Search Tree
- Graph
- Array List
- Hash Table
- Linked List
- Queue
- Stack
- StringBuilder
- Weighted Graph

###  Sort Algorithms

- Bubble sort
- Insertion sort
- Merge sort
- Quick sort
- Radix sort
- Selection sort

## Cracking Coding Interviews

Problems of the book Cracking Coding Interviews.

## GFG

Geeks For Geeks problems.

### Articles

#### 1. Sum of bit differences among all pairs

__[Link](https://www.geeksforgeeks.org/sum-of-bit-differences-among-all-pairs/)__

__Complexity:__ O(n)

__Interesting things:__

- [Bitwise Operators](https://code.tutsplus.com/articles/understanding-bitwise-operators--active-11301)

#### 2. Modular Exponentiation (Power in Modular Arithmetic)

__[Link](https://www.geeksforgeeks.org/modular-exponentiation-power-in-modular-arithmetic/)__

__Complexity:__ O(log n)

__Interesting things:__

- [Exponentiation by squaring](https://en.wikipedia.org/wiki/Exponentiation_by_squaring)

#### 3. Inplace rotate square matrix by 90 degrees | Set 1

__[Link](https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/)__

__Complexity:__ O(m n)

#### 4. Backtracking | Set 7 (Sudoku)

__[Link](https://www.geeksforgeeks.org/backtracking-set-7-suduku/)__

#### 5. Meta Strings 

Check if two strings can become same after a swap in one string.

__[Link](https://www.geeksforgeeks.org/meta-strings-check-two-strings-can-become-swap-one-string/)__

__Complexity:__ O(n)

#### 6. Find largest word in dictionary by deleting some characters of given string

__[Link](https://www.geeksforgeeks.org/find-largest-word-dictionary-deleting-characters-given-string/)__

__Complexity:__ O(n)

__Interesting things:__

- [Longest Common Subsequence](https://www.geeksforgeeks.org/longest-common-subsequence/)

#### 7. Count of strings that can be formed using a, b and c under given constraints

__[Link](https://www.geeksforgeeks.org/count-strings-can-formed-using-b-c-given-constraints/)__

__Complexity:__ O(1)

#### 8. Count BST nodes that lie in a given range

__[Link](https://www.geeksforgeeks.org/count-bst-nodes-that-are-in-a-given-range/)__

__Complexity:__ O(h + k)

#### 9. Sum of all the numbers that are formed from root to leaf paths

__[Link](https://www.geeksforgeeks.org/sum-numbers-formed-root-leaf-paths/)__

__Complexity:__ O(n)

#### 10. Merge two BSTs with limited extra space

__[Link](https://www.geeksforgeeks.org/merge-two-bsts-with-limited-extra-space/)__

__Complexity:__ O(m + n)

__Interesting things:__

- [Inorder Tree Traversal](https://www.geeksforgeeks.org/?p=5592)

#### 11. Find all triplets with zero sum

__[Link](https://www.geeksforgeeks.org/find-triplets-array-whose-sum-equal-zero/)__

__Complexity:__ O(n2)

#### 12. The Celebrity Problem

__[Link](https://www.geeksforgeeks.org/the-celebrity-problem/)__

__Complexity:__ O(n)

#### 13. Optimal Strategy for a Game | DP-31

__[Link](https://www.geeksforgeeks.org/optimal-strategy-for-a-game-dp-31/)__

__Complexity:__ O(n)

#### 14. Given a sorted dictionary of an alien language, find order of characters

__[Link](https://www.geeksforgeeks.org/given-sorted-dictionary-find-precedence-characters/)__

__Complexity:__ O(n + alpha) (Topological sorting)

__Interesting things:__

- [Topological sorting](https://www.geeksforgeeks.org/topological-sorting/)

#### 15. Check if a Binary Tree contains duplicate subtrees of size 2 or more

__[Link](https://www.geeksforgeeks.org/check-binary-tree-contains-duplicate-subtrees-size-2/)__

__Complexity:__ O(n)

#### 16. Allocate minimum number of pages

__[Link](https://www.geeksforgeeks.org/allocate-minimum-number-pages/)__

__Complexity:__ O(n log m) (n = numbers of books, m = numbers of pages)

#### 17. Given an array arr[], find the maximum j – i such that arr[j] > arr[i]

__[Link](https://www.geeksforgeeks.org/given-an-array-arr-find-the-maximum-j-i-such-that-arrj-arri/)__

__Complexity:__ O(n)

#### 18. Hungarian Algorithm for Assignment Problem | Set 1 (Introduction)

__[Link](https://www.geeksforgeeks.org/hungarian-algorithm-assignment-problem-set-1-introduction/)__

__Algorithm:__

1. For each row of the matrix, find the smallest element and subtract it from every element in its row.
2. Do the same (as step 1) for all columns.
3. Cover all zeros in the matrix using minimum number of horizontal and vertical lines.
4. Test for Optimality: If the minimum number of covering lines is n, an optimal assignment is possible and we are finished. Else if lines are lesser than n, we haven’t found the optimal assignment, and must proceed to step 5.
5. Determine the smallest entry not covered by any line. Subtract this entry from each uncovered row, and then add it to each covered column. Return to step 3.

#### 19. LRU Cache Implementation

__[Link](https://www.geeksforgeeks.org/lru-cache-implementation/)__

#### 20. Length of the longest valid substring

__[Link](https://www.geeksforgeeks.org/length-of-the-longest-valid-substring/)__

__Complexity:__ O(n)

#### 21. Median in a stream of integers (running integers)

__[Link](https://www.geeksforgeeks.org/median-of-stream-of-integers-running-integers/)__

__Complexity:__ O(n log n)

#### 22. Word Break Problem using Backtracking

__[Link](https://www.geeksforgeeks.org/word-break-problem-using-backtracking/)__

__Complexity:__ O(n s)

#### 23. Boggle | Set 2 (Using Trie)

__[Link](http://www.geeksforgeeks.org/boggle-set-2-using-trie/)__

__Complexity:__ O(s) with s max length of dictionary words

#### 24. Dynamic Programming | Set 33 (Find if a string is interleaved of two other strings

__[Link](http://www.geeksforgeeks.org/check-whether-a-given-string-is-an-interleaving-of-two-other-given-strings-set-2/)__

__Complexity:__ O(nm) with n size of a and m size of b

#### 25. Dynamic Programming | Set 11 (Egg Dropping Puzzle)

__[Link](http://www.geeksforgeeks.org/dynamic-programming-set-11-egg-dropping-puzzle/)__

__Complexity:__ O()

#### 26. Dynamic Programming | Set 28 (Minimum insertions to form a palindrome)

__[Link](http://www.geeksforgeeks.org/dynamic-programming-set-28-minimum-insertions-to-form-a-palindrome/)__

__Complexity:__ O(2^n)

#### 27. Find four elements that sum to a given value | Set 2 ( O(n^2Logn) Solution)

__[Link](http://www.geeksforgeeks.org/find-four-elements-that-sum-to-a-given-value-set-2/)__

__Complexity:__ O(n^2)

#### 28. Given a matrix of ‘O’ and ‘X’, replace 'O' with 'X' if surrounded by 'X'

__[Link](http://www.geeksforgeeks.org/given-matrix-o-x-replace-o-x-surrounded-x/)__

__Complexity:__ O(nm)

#### 29. How to print maximum number of A's using given four keys

__[Link](http://www.geeksforgeeks.org/how-to-print-maximum-number-of-a-using-given-four-keys/)__

__Complexity:__ O(n^2)

#### 30. Maximum absolute difference between sum of two contiguous sub-arrays

__[Link](http://www.geeksforgeeks.org/maximum-absolute-difference-between-sum-of-two-contiguous-sub-arrays/)__

__Complexity:__ O(n)

__Algorithm:__ Kadane Algorithm

#### 31. Merge Overlapping Intervals

__[Link](http://www.geeksforgeeks.org/merging-intervals/)__

__Complexity:__ O(n log n)

#### 32. Paper Cut into Minimum Number of Squares | Set 2

__[Link](http://www.geeksforgeeks.org/paper-cut-minimum-number-squares-set-2/)__

__Complexity:__ O()

#### 33. Generate all binary strings from given pattern

__[Link](http://www.geeksforgeeks.org/generate-all-binary-strings-from-given-pattern/)__

__Complexity:__ O(2^n)

#### 34. Find subarray with given sum | Set 1 (Nonnegative Numbers)

__[Link](http://www.geeksforgeeks.org/find-subarray-with-given-sum/)__

__Complexity:__ O(n)

#### 35. Find the longest substring with k unique characters in a given string

__[Link](http://www.geeksforgeeks.org/find-the-longest-substring-with-k-unique-characters-in-a-given-string/)__

__Complexity:__ O(n)

#### 36. Find the two non-repeating elements in an array of repeating elements

__[Link](http://www.geeksforgeeks.org/find-two-non-repeating-elements-in-an-array-of-repeating-elements/)__

__Complexity:__ O(n)

#### 37. Flood fill Algorithm - how to implement fill() in paint?

__[Link](http://www.geeksforgeeks.org/flood-fill-algorithm-implement-fill-paint/)__

__Complexity:__ O(nm)

#### 38. Print all Jumping Numbers smaller than or equal to a given value

__[Link](http://www.geeksforgeeks.org/print-all-jumping-numbers-smaller-than-or-equal-to-a-given-value/)__

__Complexity:__ O(n)

#### 39. Unbounded Knapsack (Repetition of items allowed)

__[Link](http://www.geeksforgeeks.org/unbounded-knapsack-repetition-items-allowed/)__

__Complexity:__ O()

### Problems

#### 1. Subarray with given sum

__[Link](https://practice.geeksforgeeks.org/problems/subarray-with-given-sum/0)__

__Complexity:__ O(n)

#### 2. Finding the numbers

__[Link](https://practice.geeksforgeeks.org/problems/finding-the-numbers/0)__

__Complexity:__ O(n)

#### 3. Word Boggle

__[Link](https://practice.geeksforgeeks.org/problems/word-boggle/0)__

__Complexity:__ O(n3)

#### 4. Connect Nodes at Same Level

__[Link](https://practice.geeksforgeeks.org/problems/connect-nodes-at-same-level/1)__

__Complexity:__ O(n)

#### 5. Find triplets with zero sum

__[Link](https://practice.geeksforgeeks.org/problems/find-triplets-with-zero-sum/1)__

__Complexity:__ O(n)

#### 6. Save Ironman

__[Link](https://practice.geeksforgeeks.org/problems/save-ironman/0)__

__Complexity:__ O(n)

## Spoj

Problems from [www.spoj.com].

### Problem 1

[http://www.spoj.com/problems/PRIME1/]

### Problem 2

[ONP - Transform the Expression](http://www.spoj.com/problems/ONP/)

### Problem 3

[PALIN - The Next Palindrome](http://www.spoj.com/problems/PALIN/)

### Problem 4

[ARITH - Simple Arithmetics](http://www.spoj.com/problems/ARITH/)

### Problem 5

Max product using K elements of the array.
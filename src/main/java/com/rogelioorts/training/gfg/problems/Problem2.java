package com.rogelioorts.training.gfg.problems;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Problem2 {
  
  private static final int MAX_NUMBERS = 2;

  public Integer[] getNotUniqueNumbers(int[] array) {
    Set<Integer> candidates = new HashSet<>();
    
    for(int i = 0; i < array.length; i++) {
      int number = array[i];
      if(candidates.contains(number)) {
        candidates.remove(number);
      } else {
        candidates.add(number);
      }
    }
    
    if(candidates.size() > 2) {
      throw new RuntimeException("Not valid input.");
    }

    Integer result[] = candidates.toArray(new Integer[MAX_NUMBERS]);
    Arrays.sort(result);
    
    return result;
  }
  
}

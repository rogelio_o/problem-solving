package com.rogelioorts.training.gfg.problems;

import java.util.LinkedList;
import java.util.Queue;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Problem4 {

    public void addsNextRight(BTreeNode<Integer> root) {
        Queue<BTreeNode<Integer>> q = new LinkedList<>();
        q.offer(root);

        while (!q.isEmpty()) {
            BTreeNode<Integer> prev = null;
            int size = q.size();

            while (size > 0) {
                BTreeNode<Integer> current = q.poll();

                if (current.getLeft() != null)
                    q.offer(current.getLeft());
                if (current.getRight() != null)
                    q.offer(current.getRight());

                if (prev != null)
                    prev.setNextRight(current);

                prev = current;

                size--;
            }

            prev.setNextRight(null);
        }
    }

}

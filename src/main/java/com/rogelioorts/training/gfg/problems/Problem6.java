package com.rogelioorts.training.gfg.problems;

import java.util.HashMap;
import java.util.Map;

public class Problem6 {

  public boolean isPalindrome(String word) {
    Map<Character, Integer> numOcurrences = new HashMap<>();
    
    int count = 0;
    for(char c : word.toLowerCase().toCharArray()) {
      if(isAlphaNumeric(c)) {
        count++;
        
        if(numOcurrences.containsKey(c)) {
          numOcurrences.put(c, numOcurrences.get(c) + 1);
        } else {
          numOcurrences.put(c, 1);
        }
      }
    }
    
    int maxOdds = count % 2 == 0 ? 0 : 1;
    for(int ocurrence : numOcurrences.values()) {
      if(ocurrence % 2 != 0) {
        if(maxOdds == 0) {
          return false;
        } else {
          maxOdds--;
        }
      }
    }
    
    return true;
  }
  
  private boolean isAlphaNumeric(char c) {
    return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z');
  }
  
}

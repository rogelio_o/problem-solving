package com.rogelioorts.training.gfg.problems;

public class Problem1 {

  public int[] findFirstSubArrayWithSum(int[] array, int sum) {
    if(array.length == 0) {
      return null;
    }
    
    int start = 0;
    int end = 0;
    int auxSum = array[0];
    
    while(end < array.length) {
      if(auxSum == sum) {
        break;
      } else if(auxSum < sum) {
        end++;
        if(end < array.length) {
          auxSum += array[end];
        }
      } else if(start == end) {
        start++;
        end++;
        auxSum = array[start];
      } else {
        auxSum -= array[start];
        start++;
      }
    }
    
    if(auxSum == sum) {
      int[] result = new int[end - start + 1];
      int index = 0;
      for(int i = start; i <= end; i++) {
        result[index] = array[i];
        index++;
      }
      
      return result;
    }
    return null;
  }
  
}

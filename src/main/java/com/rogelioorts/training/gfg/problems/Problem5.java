package com.rogelioorts.training.gfg.problems;

public class Problem5 {

  public boolean hasTripletsZeroSum(int[] array, int n) {
    if(array.length < 3) {
      return false;
    }
    
    for(int i = 0; i < n - 2; i++) {
      if(array[i] + array[i + 1] + array[i + 2] == 0) {
        return true;
      }
    }
    
    return false;
  }
  
}

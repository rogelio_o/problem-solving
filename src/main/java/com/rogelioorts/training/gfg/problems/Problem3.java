package com.rogelioorts.training.gfg.problems;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Problem3 {

  public List<String> findWordsInBoggle(String[] dictionary, char[][] boggle, int n, int m) {
    List<String> result = new ArrayList<>();
    for(String word : dictionary) {
      if(isWordInBoggle(word, boggle, n, m, new Point(0, 0), new Point(n - 1, m - 1), new HashSet<>())) {
        result.add(word);
      }
    }
    return result;
  }
  
  private boolean isWordInBoggle(String word, char[][] boggle, int n, int m, Point start, Point end, Set<Point> visited) {
    if(word.length() == 0) {
      return true;
    }
    
    char first = word.charAt(0);
    String remaining = word.substring(1);
    for(int i = start.i; i <= end.i; i++) {
      for(int j = start.j; j <= end.j; j++) {
        Point point = new Point(i, j);
        if(first == boggle[i][j] && !visited.contains(point)) {
          Set<Point> visitedCopy = new HashSet<>(visited);
          visitedCopy.add(point);
          if(isWordInBoggle(remaining, boggle, n, m, createStart(i, j), createEnd(i, j, n, m), visitedCopy)) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private Point createStart(int i, int j) {
    return new Point(Math.max(0, i - 1), Math.max(0, j - 1));
  }
  
  private Point createEnd(int i, int j, int n, int m) {
    return new Point(Math.min(n - 1, i + 1), Math.min(m - 1, j + 1));
  }
  
  private class Point {
    
    private int i;
    
    private int j;
    
    public Point(int i, int j) {
      this.i = i;
      this.j = j;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + getOuterType().hashCode();
      result = prime * result + i;
      result = prime * result + j;
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      Point other = (Point) obj;
      if (!getOuterType().equals(other.getOuterType()))
        return false;
      if (i != other.i)
        return false;
      if (j != other.j)
        return false;
      return true;
    }

    private Problem3 getOuterType() {
      return Problem3.this;
    }
    
    @Override
    public String toString() {
      return i + ", " + j;
    }
    
  }
  
}

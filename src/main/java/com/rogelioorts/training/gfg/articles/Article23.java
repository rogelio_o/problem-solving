package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;

import com.rogelioorts.training.gfg.utils.TrieNode;

public class Article23 {

    public ArrayList<String> getWordsInBoggle(String[] dictionary, char[][] boggle) {
        TrieNode root = createTrie(dictionary);

        ArrayList<String> results = new ArrayList<>();
        for (int i = 0; i < boggle.length; i++) {
            for (int j = 0; j < boggle[0].length; j++) {
                TrieNode child = findNode(root, boggle[i][j]);

                if (child != null) {
                    addWords(child, boggle, new Point(i, j), results, new boolean[boggle.length][boggle[0].length],
                            child.getValue() + "");
                }
            }
        }

        return results;
    }

    private TrieNode findNode(TrieNode root, char c) {
        for (TrieNode child : root.getChildren()) {
            if (child.getValue() == c) {
                return child;
            }
        }

        return null;
    }

    private void addWords(TrieNode node, char[][] boggle, Point p, ArrayList<String> results, boolean[][] visited,
            String word) {
        if (node.isLeaf()) {
            results.add(word);
            return;
        }

        visited[p.i][p.j] = true;

        for (TrieNode child : node.getChildren()) {
            for (Point childP : childPoints(p)) {
                if (isValidPointAndNotVisited(childP, visited) && boggle[childP.i][childP.j] == child.getValue()) {
                    addWords(child, boggle, childP, results, visited, word + child.getValue());
                }
            }
        }
    }

    private ArrayList<Point> childPoints(Point p) {
        ArrayList<Point> result = new ArrayList<>();

        result.add(new Point(p.i - 1, p.j + 1));
        result.add(new Point(p.i, p.j + 1));
        result.add(new Point(p.i + 1, p.j + 1));
        result.add(new Point(p.i + 1, p.j));
        result.add(new Point(p.i + 1, p.j - 1));
        result.add(new Point(p.i, p.j - 1));
        result.add(new Point(p.i - 1, p.j - 1));
        result.add(new Point(p.i - 1, p.j));

        return result;
    }

    private boolean isValidPointAndNotVisited(Point p, boolean[][] visited) {
        return p.i >= 0 && p.j >= 0 && p.i < visited.length && p.j < visited[0].length && !visited[p.i][p.j];
    }

    private TrieNode createTrie(String[] dictionary) {
        TrieNode root = new TrieNode('-');

        for (String word : dictionary) {
            TrieNode aux = root;

            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                TrieNode newNode = findNode(aux, c);
                if (newNode == null) {
                    newNode = new TrieNode(c);
                    aux.addChildren(newNode);
                }
                aux = newNode;
            }
        }

        return root;
    }

    private class Point {

        private int i;

        private int j;

        public Point(int i, int j) {
            this.i = i;
            this.j = j;
        }

    }

}

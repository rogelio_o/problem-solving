package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.rogelioorts.training.gfg.utils.Graph;

public class Article14 {

    public List<Character> solve(String[] dictionary, int alpha) {
        Graph graph = new Graph(alpha);

        for (int i = 0; i < dictionary.length - 1; i++) {
            String word1 = dictionary[i];
            String word2 = dictionary[i + 1];

            for (int j = 0; j < Math.min(word1.length(), word2.length()); j++) {
                if (word1.charAt(j) != word2.charAt(j)) {
                    graph.addEdge(word1.charAt(j) - 'a', word2.charAt(j) - 'a');
                    break;
                }
            }
        }

        List<Character> result = getCharactersFromTopologicalSortGrapth(graph);
        Collections.reverse(result);
        return result;
    }

    private List<Character> getCharactersFromTopologicalSortGrapth(Graph graph) {
        List<Character> result = new ArrayList<>();
        boolean[] visited = new boolean[graph.size()];
        for (int i = 0; i < graph.size(); i++) {
            visited[i] = false;
        }

        for (int i = 0; i < graph.size(); i++) {
            if (!visited[i]) {
                getCharactersFromTopologicalSortGrapth(graph, i, visited, result);
            }
        }

        return result;
    }

    private void getCharactersFromTopologicalSortGrapth(Graph graph, int i, boolean[] visited, List<Character> result) {
        visited[i] = true;

        for (int adjacentVertex : graph.getAdjacencyListOfVertex(i)) {
            if (!visited[adjacentVertex]) {
                getCharactersFromTopologicalSortGrapth(graph, adjacentVertex, visited, result);
            }
        }

        result.add(Character.valueOf((char) ('a' + i)));
    }

}

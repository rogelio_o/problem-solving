package com.rogelioorts.training.gfg.articles;

public class Article1 {
  
  private static int NUM_BITS_IN_INT = 32;

  public int solve(int[] input, int n) {
    int result = 0;
    
    for (int i = 0; i < NUM_BITS_IN_INT; i++) {
      int numZerosInPosition = 0;
      for (int j = 0; j < n; j++) {
        if((input[j] & (1 << i)) == 0) {
          numZerosInPosition++; 
        }
      }
      
      int numOnesInPosition = n - numZerosInPosition;
      
      result += 2 * numZerosInPosition * numOnesInPosition;
    }
    
    return result;
  }
  
}

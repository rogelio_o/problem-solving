package com.rogelioorts.training.gfg.articles;

public class Article6 {

  public String solve(String[] dictionary, String str) {
    String longestSubsequence = "";
    
    for(int i = 0; i < dictionary.length; i++) {
      String subsequence = dictionary[i];
      if(subsequence.length() > longestSubsequence.length() && isSubsequence(subsequence, str)) {
        longestSubsequence = subsequence;
      }
    }
    
    return longestSubsequence;
  }
  
  private boolean isSubsequence(String dictionaryWord, String str) {
    if(str.length() < dictionaryWord.length()) {
      return false;
    }
    
    int dIndex = 0;
    for(int sIndex = 0; dIndex < dictionaryWord.length() && sIndex < str.length(); sIndex++) {
      if(dictionaryWord.charAt(dIndex) == str.charAt(sIndex)) {
        dIndex++;
      }
    }
    
    return dIndex == dictionaryWord.length();
  }
  
}

package com.rogelioorts.training.gfg.articles;

import com.rogelioorts.training.gfg.utils.Point;

public class Article4 {

    private static final int SUDOKU_SIZE = 9;

    private static final int UNASSIGNED = 0;

    public boolean solve(int[][] board) {
        Point point = findEmptyCell(board);
        if (point == null) {
            return true;
        }

        for (int num = 1; num <= SUDOKU_SIZE; num++) {
            if (canBeAdded(board, point, num)) {
                board[point.getX()][point.getY()] = num;

                if (solve(board)) {
                    return true;
                }

                board[point.getX()][point.getY()] = UNASSIGNED;
            }
        }

        return false;
    }

    private Point findEmptyCell(int[][] board) {
        for (int i = 0; i < SUDOKU_SIZE; i++) {
            for (int j = 0; j < SUDOKU_SIZE; j++) {
                if (board[i][j] == UNASSIGNED) {
                    return new Point(i, j);
                }
            }
        }

        return null;
    }

    private boolean canBeAdded(int[][] board, Point point, int num) {
        return canBeAddedInRow(board, point, num) && canBeAddedInColumn(board, point, num)
                && canBeAddedInSection(board, point, num);
    }

    private boolean canBeAddedInRow(int[][] board, Point point, int num) {
        for (int j = 0; j < SUDOKU_SIZE; j++) {
            if (board[point.getX()][j] == num) {
                return false;
            }
        }

        return true;
    }

    private boolean canBeAddedInColumn(int[][] board, Point point, int num) {
        for (int i = 0; i < SUDOKU_SIZE; i++) {
            if (board[i][point.getY()] == num) {
                return false;
            }
        }

        return true;
    }

    private boolean canBeAddedInSection(int[][] board, Point point, int num) {
        int iOffset = point.getX() - (point.getX() % 3);
        int jOffset = point.getY() - (point.getY() % 3);

        for (int i = iOffset; i < iOffset + 3; i++) {
            for (int j = jOffset; j < jOffset + 3; j++) {
                if (board[i][j] == num) {
                    return false;
                }
            }
        }

        return true;
    }

}

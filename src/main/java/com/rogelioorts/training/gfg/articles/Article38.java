package com.rogelioorts.training.gfg.articles;

public class Article38 {

  public boolean isJumpNumber(int number) {
    String str = String.valueOf(number);
    
    for(int i = 1; i < str.length(); i++) {
      int digitA = Character.digit(str.charAt(i - 1), 10);
      int digitB = Character.digit(str.charAt(i), 10);
      
      if(!isJumpDigit(digitA, digitB)) {
        return false;
      }
    }
    
    return true;
  }
  
  private boolean isJumpDigit(int digitA, int digitB) {
    return Math.abs(digitA - digitB) == 1;
  }
  
}

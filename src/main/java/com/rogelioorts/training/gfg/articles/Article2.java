package com.rogelioorts.training.gfg.articles;

public class Article2 {

  public int solve(int x, int y, int p) {
    if (y == 0) {
      return 1;
    } else if(y == 1) {
      return x % p;
    } else if(y % 2 == 0) {
      return solve(x * x % p, y / 2, p);
    } else {
      return x * solve(x * x % p, (y - 1) / 2, p) % p;
    }
  }
}

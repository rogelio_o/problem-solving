package com.rogelioorts.training.gfg.articles;

import java.util.HashSet;
import java.util.Set;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article15 {

    private static final String MARKER = "$";

    public boolean solve(BTreeNode<Character> root) {
        return internalSolving(new HashSet<>(), root) == null;
    }

    private String internalSolving(Set<String> subtrees, BTreeNode<Character> root) {
        if (root == null) {
            return MARKER;
        }

        String left = internalSolving(subtrees, root.getLeft());
        if (left == null) {
            return null;
        }

        String right = internalSolving(subtrees, root.getRight());
        if (right == null) {
            return null;
        }

        String key = root.getValue() + left + right;
        if (key.length() > 3 && subtrees.contains(key)) {
            return null;
        }

        subtrees.add(key);

        return key;
    }

    public boolean solve2(BTreeNode<Character> root) {
        return solve2(root, root.getLeft()) || solve2(root, root.getRight());
    }

    private boolean solve2(BTreeNode<Character> main, BTreeNode<Character> root) {
        if (root == null) {
            return false;
        } else {
            return isSubtree(main, root) || solve2(main, root.getLeft()) || solve2(main, root.getRight());
        }
    }

    private boolean isSubtree(BTreeNode<Character> main, BTreeNode<Character> root) {
        return isTwoLevelsAtLeast(root) && countSubtrees(main, root) > 1;
    }

    private boolean isTwoLevelsAtLeast(BTreeNode<Character> root) {
        return root.getLeft() != null || root.getRight() != null;
    }

    private int countSubtrees(BTreeNode<Character> main, BTreeNode<Character> root) {
        if (root == null) {
            return 0;
        } else if (main == null) {
            return 0;
        } else if (areIdentical(main, root)) {
            return 1;
        } else {
            return countSubtrees(main.getLeft(), root) + countSubtrees(main.getRight(), root);
        }
    }

    private boolean areIdentical(BTreeNode<Character> main, BTreeNode<Character> root) {
        if (main == null && root == null) {
            return true;
        } else if (main == null || root == null) {
            return false;
        } else {
            return main.getValue().equals(root.getValue()) && areIdentical(main.getLeft(), root.getLeft())
                    && areIdentical(main.getRight(), root.getRight());
        }
    }

}

package com.rogelioorts.training.gfg.articles;

public class Article3 {

  public void solve(int[][] input, int n) {
    for(int i = 0; i < n / 2; i++) {
      int last = n - i - 1;
      for(int j = i; j < last; j++) {
        int aux = input[i][j];
        input[i][j] = input[j][last];
        input[j][last] = input[last][n - 1 - j];
        input[last][n - 1 - j] = input[n - 1 - j][i];
        input[n - 1 - j][i] = aux;
      }
    }
  }
  
}

package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.Collections;

public class Article31 {
  
  public ArrayList<Interval> intervals(ArrayList<Interval> intervals) {
    Collections.sort(intervals);
    
    Interval prev = intervals.get(0);
    ArrayList<Interval> result = new ArrayList<>();
    for(int i = 1; i < intervals.size(); i++) {
      Interval current = intervals.get(i);
      if(prev.overlap(current)) {
        prev = new Interval(prev.start, current.end);
      } else {
        result.add(prev);
        prev = current;
      }
    }
    
    result.add(prev);
    
    return result;
  }

  public static class Interval implements Comparable<Interval> {
    
    private int start;
    
    private int end;

    public Interval(int start, int end) {
      super();
      this.start = start;
      this.end = end;
    }

    public int getStart() {
      return start;
    }

    public int getEnd() {
      return end;
    }
    
    public boolean overlap(Interval o) {
      return start < o.end && o.start < end;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + end;
      result = prime * result + start;
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      Interval other = (Interval) obj;
      if (end != other.end)
        return false;
      if (start != other.start)
        return false;
      return true;
    }

    @Override
    public int compareTo(Interval o) {
      int result = start - o.start;
      if(result == 0) {
        return end - o.end;
      }
      return result;
    }
    
  }
  
}

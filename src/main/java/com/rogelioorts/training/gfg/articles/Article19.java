package com.rogelioorts.training.gfg.articles;

import java.util.HashMap;
import java.util.Map;

import com.rogelioorts.training.gfg.utils.CustomLinkedList;

public class Article19 {

    private int size;

    private CustomLinkedList<Integer> queue;

    private Map<Integer, CustomLinkedList.Node<Integer>> table;

    public Article19(int size) {
        this.size = size;
        this.queue = new CustomLinkedList<Integer>();
        this.table = new HashMap<>(size);
    }

    public void reference(int page) {
        if (!table.containsKey(page)) {
            if (queue.size() == size) {
                Integer last = queue.poll();
                table.remove(last);
            }
        } else {
            queue.remove(table.get(page));
        }

        CustomLinkedList.Node<Integer> node = queue.add(page);
        table.put(page, node);
    }

    public int[] getStatus() {
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = -1;
        }

        int index = 0;
        for (int page : queue) {
            result[index] = page;
            index++;
        }

        return result;
    }

    public int getSize() {
        return size;
    }

}

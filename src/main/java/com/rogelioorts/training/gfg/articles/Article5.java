package com.rogelioorts.training.gfg.articles;

public class Article5 {

  public boolean solve(String strA, String strB) {
    if(strA.length() != strB.length()) {
      return false;
    }
    
    int n = strA.length();
    
    int numDiff = 0;
    int previous = -1;
    int current = -1;
    
    for(int i = 0; i < n; i++) {
      if(strA.charAt(i) != strB.charAt(i)) {
        numDiff++;
        
        if(numDiff > 2) {
          return false;
        }
        
        previous = current;
        current = i;
      }
    }
    
    return numDiff == 0 || (numDiff == 2 && strA.charAt(current) == strB.charAt(previous) && strA.charAt(previous) == strB.charAt(current));
  }
  
}

package com.rogelioorts.training.gfg.articles;

import java.util.HashSet;

public class Article36 {

  public int duplicated(int[] arr) {
    HashSet<Integer> numbers = new HashSet<>();
    
    for(int num : arr) {
      if(!numbers.add(num)) {
        return num;
      }
    }
    
    return -1;
  }
  
}

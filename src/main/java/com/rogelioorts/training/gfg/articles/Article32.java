package com.rogelioorts.training.gfg.articles;

public class Article32 {

  public int numSquares(int n, int m) {
    int[][] cache = new int[n + 1][m + 1];
    
    return numSquares(n, m, cache);
  }
  
  private int numSquares(int n, int m, int[][] cache) {
    if(n == m) {
      return 1;
    } else if(cache[n][m] != 0) {
      return cache[n][m];
    }
    
    int verticalMin = Integer.MAX_VALUE;
    int horizontalMin = Integer.MAX_VALUE;
    
    for(int i = 1; i <= n / 2; i++) {
      verticalMin = Math.min(numSquares(i, m, cache) + numSquares(n - i, m, cache), verticalMin);
    }
    
    for(int j = 1; j <= m / 2; j++) {
      horizontalMin = Math.min(numSquares(n, j, cache) + numSquares(n, m - j, cache), horizontalMin);
    }
    
    cache[n][m] = Math.min(verticalMin, horizontalMin);
    
    return cache[n][m];
  }
  
}

package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Article11 {

  public List<int[]> solve(int[] input, int n) {
    List<int[]> result = new ArrayList<>();
    
    Arrays.sort(input);
    
    for(int i = 0; i < n - 1; i++) {
      int l = i + 1;
      int r = n - 1;
      
      while(l < r) {
        int a = input[i];
        int b = input[l];
        int c = input[r];
        int sum = a + b + c;
        
        if(sum == 0) {
          result.add(new int[] {a, b, c});
          l++;
          r--;
        } else if(sum < 0) {
          l++;
        } else {
          r--;
        }
      }
    }
    
    return result;
  }
  
}

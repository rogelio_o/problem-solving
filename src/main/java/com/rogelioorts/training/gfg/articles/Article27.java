package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.Arrays;

public class Article27 {

  public ArrayList<int[]> possibleSumsOf4(int[] arr, int sum) {
    PartialResult[] aux = new PartialResult[arr.length * arr.length];
    
    int index = 0;
    for(int i = 0; i < arr.length; i++) {
      for(int j = 0; j < arr.length; j++) {
        aux[index++] = new PartialResult(arr[i], arr[j]);
      }
    }
    
    ArrayList<int[]> result = new ArrayList<>();
    
    Arrays.sort(aux, (a, b) -> {
      return a.sum() - b.sum();
    });
    
    for(int i = 0, j = aux.length - 1; i < j;) {
      PartialResult p1 = aux[i];
      PartialResult p2 = aux[j];
      
      int s = p1.sum() + p2.sum();
      if(p1.a != p1.b && p2.a != p2.b && s == sum) {
        result.add(new int[] {p1.a, p1.b, p2.a, p2.b});
        i++;
        j--;
      } else if(s < sum) {
        i++;
      } else {
        j--;
      }
    }
    
    return result;
  }
  
  private class PartialResult {
    
    private int a;
    
    private int b;
    
    public PartialResult(int a, int b) {
      this.a = a;
      this.b = b;
    }
    
    public int sum() {
      return a + b;
    }
    
  }
  
}

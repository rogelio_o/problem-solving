package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Article35 {

  public ArrayList<String> maxSubstring(String str, int k) {
    HashMap<Character, Integer> ocurrences = new HashMap<>();
    int max = 0;
    int start = 0;
    ArrayList<String> result = new ArrayList<>();
    
    for(int i = 0; i < str.length(); i++) {
      char c = str.charAt(i);
      ocurrences.put(c, ocurrences.getOrDefault(c, 0) + 1);
      int numChars = countNumChars(ocurrences);
      
      if(numChars > k) {
        int numStartChar;
        char startCharacter;
        do {
          startCharacter = str.charAt(start);
          ocurrences.put(startCharacter, ocurrences.get(startCharacter) - 1);
          numStartChar = ocurrences.get(startCharacter);
          start++;
        } while(numStartChar > 0);
        
        ocurrences.remove(startCharacter);
      } else if(numChars == k) {
        int size = i - start + 1;
        
        if(size >= max) {
          if(size != max) {
            result.clear();
          }
          max = size;
          
          result.add(str.substring(start, i + 1));
        }
      }
    }
    
    return result;
  }
  
  private int countNumChars(Map<Character, Integer> ocurrences) {
    return ocurrences.keySet().size();
  }
  
}

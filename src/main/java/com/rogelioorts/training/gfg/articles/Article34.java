package com.rogelioorts.training.gfg.articles;

public class Article34 {

  public int[] getRangeOfSum(int[] arr, int sum) {
    int start = 0;
    int end = 0;
    int acc = arr[0];
    while(end < arr.length) {
      if(acc == sum) {
        return new int[] {start, end};
      } else if(acc < sum) {
        end++;
        if(end < arr.length) {
          acc += arr[end];
        }
      } else {
        acc -= arr[start];
        start++;
        if(start > end) {
          end++;
        }
      }
    }
    
    return null;
  }
  
}

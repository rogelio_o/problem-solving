package com.rogelioorts.training.gfg.articles;

public class Article26 {

  public int numInsertionsToPalindrome(String str) {
    return numInsertionsToPalindrome(str, 0, str.length() - 1);
  }
  
  private int numInsertionsToPalindrome(String str, int indexLeft, int indexRight) {
    if(indexLeft >= str.length() || indexRight < 0) {
      return 0;
    } else if(indexLeft == indexRight) {
      return 0;
    } else if(str.charAt(indexLeft) == str.charAt(indexRight)) {
      return numInsertionsToPalindrome(str, indexLeft + 1, indexRight - 1);
    } else {
      return 1 + Math.min(
          numInsertionsToPalindrome(str, indexLeft + 1, indexRight), 
          numInsertionsToPalindrome(str, indexLeft, indexRight - 1)
      );
    }
  }
  
}

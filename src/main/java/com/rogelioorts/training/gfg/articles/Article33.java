package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;

public class Article33 {

  public ArrayList<String> findBinariesFromWildcard(String str) {
    ArrayList<String> results = new ArrayList<>();
    findBinariesFromWildcard(str, 0, "", results);
    return results;
  }
  
  private void findBinariesFromWildcard(String str, int index, String result, ArrayList<String> results) {
    if(index >= str.length()) {
      results.add(result);
      return;
    }
    
    char c = str.charAt(index);
    
    if(c == '?') {
      findBinariesFromWildcard(str, index + 1, result + "0", results);
      findBinariesFromWildcard(str, index + 1, result + "1", results);
    } else {
      findBinariesFromWildcard(str, index + 1, result + c, results);
    }
  }
  
}

package com.rogelioorts.training.gfg.articles;

public class Article20 {

  public int solve(String input) {
    int result = 0;
    int acc = 0;
    int l = 0;
    int r = 0;
    
    for(int i = 0; i < input.length(); i++) {
      if(input.charAt(i) == '(') {
        if(i > 0 && input.charAt(i - 1) == ')') { // new start
          if(l <= r) {
            acc += l * 2;
          } else {
            result = Math.max(result, acc);
            acc = Math.min(l, r) * 2;
          }
          
          l = 0;
          r = 0;
        }
        
        l++;
      } else {
        r++;
      }
    }
    
    if(l <= r) {
      acc += l * 2;
    } else {
      acc = Math.min(l, r) * 2;
    }
    result = Math.max(result, acc);
    
    return result;
  }
  
}

package com.rogelioorts.training.gfg.articles;

public class Article24 {

  public boolean isInterleaving(String a, String b, String c) {
    if(a.length() + b.length() != c.length()) {
      return false;
    }
    
    int n = a.length();
    int m = b.length();
    boolean[][] results = new boolean[n + 1][m + 1];
    for(int i = 0; i <= n; i++) {
      for(int j = 0; j <= m; j++) {
        if(i == 0 && j == 0) {
          results[i][j] = true;
        } else if(i == 0 && b.charAt(j - 1) == c.charAt(j - 1)) {
          results[i][j] = results[i][j - 1];
        } else if(j == 0 && a.charAt(i - 1) == c.charAt(i - 1)) {
          results[i][j] = results[i - 1][j];
        } else if(i > 0 && a.charAt(i - 1) == c.charAt(i + j - 1) && (j == 0 || b.charAt(j - 1) != c.charAt(i + j - 1))) {
          results[i][j] = results[i - 1][j];
        } else if((i == 0 || a.charAt(i - 1) != c.charAt(i + j - 1)) && j > 0 && b.charAt(j - 1) == c.charAt(i + j - 1)) {
          results[i][j] = results[i][j - 1];
        } else if(i > 0 && j > 0 && a.charAt(i - 1) == c.charAt(i + j - 1) && b.charAt(j - 1) == c.charAt(i + j - 1)) {
          results[i][j] = (results[i - 1][j]) || (results[i][j - 1]);
        }
      }
    }
    
    return results[n][m];
  }
  
}

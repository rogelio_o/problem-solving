package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Article22 {

  public List<String> solve(String[] dictionary, String input) {
    List<String> results = new ArrayList<>();
    Set<String> words = new HashSet<>(Arrays.asList(dictionary));
    
    solve(words, input, "", results);
    
    return results;
  }
  
  public void solve(Set<String> words, String input, String result, List<String> results) {
    for(int i = 1; i <= input.length(); i++) {
      String prefix = input.substring(0, i);
      
      if(words.contains(prefix)) {
        if(i == input.length()) {
          results.add(result + prefix);
        } else {
          solve(words, input.substring(i), result + prefix + " ", results);
        }
      }
    }
  }
  
}

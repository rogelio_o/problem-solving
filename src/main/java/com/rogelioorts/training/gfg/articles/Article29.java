package com.rogelioorts.training.gfg.articles;

public class Article29 {

  public int maxAs(int numPress) {
    if(numPress <= 6) {
      return numPress;
    }
    
    int[] cache = new int[numPress];
    for (int i = 1; i <= 6; i++) {
      cache[i - 1] = i;
    }
    
    for(int i = 7; i <= numPress; i++) {
      cache[i - 1] = 0;
      
      for(int j = i - 3; j >= 1; j--) {
        int curr = (i - j - 1) * cache[j - 1];
        if(curr > cache[i - 1]) {
            cache[i - 1] = curr;
        }
      }
    }
    
    return cache[numPress - 1];
  }
  
}

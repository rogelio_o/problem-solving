package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.List;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article9 {

    public int solve(BTreeNode<Integer> root) {
        return findNumbers(root).stream().reduce(0, (a, b) -> a + b);
    }

    private List<Integer> findNumbers(BTreeNode<Integer> root) {
        List<Integer> results = new ArrayList<>();

        if (root.getLeft() == null && root.getRight() == null) {
            results.add(nodeToNumber(root, 0));
        }

        if (root.getLeft() != null) {
            results.addAll(findNumbers(root.getLeft()));
        }

        if (root.getRight() != null) {
            results.addAll(findNumbers(root.getRight()));
        }

        return results;
    }

    private int nodeToNumber(BTreeNode<Integer> root, int base) {
        int result = (int) (root.getValue() * Math.pow(10, base));
        if (root.getParent() != null) {
            result += nodeToNumber(root.getParent(), base + 1);
        }

        return result;
    }

}

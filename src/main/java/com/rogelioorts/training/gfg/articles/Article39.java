package com.rogelioorts.training.gfg.articles;

public class Article39 {

  public int[] getNumItems(int[] values, int[] weights, int w) {
    return getNumItems(values, weights, 0, w);
  }
  
  private int[] getNumItems(int[] values, int[] weights, int index, int w) {
    if(index >= values.length) {
      return new int[values.length];
    }
    
    int maxValue = 0;
    int[] result = new int[values.length];
    int localWeight = 0;
    for(int i = 0; localWeight <= w; i++) {
      localWeight = weights[index] * i;
      
      int[] localResult = getNumItems(values, weights, index + 1, w - localWeight);
      localResult[index] = i;
      if(calcWeights(weights, localResult) == w) {
        int value = calcValue(values, localResult);
        if(value > maxValue) {
          maxValue = value;
          result = localResult;
        }
      }
    }
    
    return result;
  }
  
  private int calcValue(int[] values, int[] items) {
    int result = 0;
    for(int i = 0; i < items.length; i++) {
      result += items[i] * values[i];
    }
    return result;
  }
  
  private int calcWeights(int[] weights, int[] items) {
    int result = 0;
    for(int i = 0; i < items.length; i++) {
      result += items[i] * weights[i];
    }
    return result;
  }
  
}

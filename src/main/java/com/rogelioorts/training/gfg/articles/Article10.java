package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article10 {

    public List<Integer> solve(BTreeNode<Integer> rootA, BTreeNode<Integer> rootB) {
        List<Integer> results = new ArrayList<>();

        if (rootA == null) {
            addInOrder(results, rootB);
        } else if (rootB == null) {
            addInOrder(results, rootA);
        } else {
            BTreeNode<Integer> currentA = rootA;
            BTreeNode<Integer> currentB = rootB;
            Stack<BTreeNode<Integer>> stackA = new Stack<>();
            Stack<BTreeNode<Integer>> stackB = new Stack<>();

            while (currentA != null || currentB != null || !stackA.isEmpty() || !stackB.isEmpty()) {
                if (currentA != null || currentB != null) {
                    if (currentA != null) {
                        stackA.push(currentA);
                        currentA = currentA.getLeft();
                    }

                    if (currentB != null) {
                        stackB.push(currentB);
                        currentB = currentB.getLeft();
                    }
                } else {
                    if (stackA.isEmpty()) {
                        while (!stackB.isEmpty()) {
                            currentB = stackB.pop();
                            currentB.setLeft(null);
                            addInOrder(results, currentB);
                        }
                        currentB = null;
                    } else if (stackB.isEmpty()) {
                        while (!stackA.isEmpty()) {
                            currentA = stackA.pop();
                            currentA.setLeft(null);
                            addInOrder(results, currentA);
                        }
                        currentA = null;
                    } else {
                        currentA = stackA.pop();
                        currentB = stackB.pop();

                        if (currentA.getValue() < currentB.getValue()) {
                            results.add(currentA.getValue());
                            currentA = currentA.getRight();
                            stackB.push(currentB);
                            currentB = null;
                        } else {
                            results.add(currentB.getValue());
                            currentB = currentB.getRight();
                            stackA.push(currentA);
                            currentA = null;
                        }
                    }
                }
            }
        }

        return results;
    }

    private void addInOrder(List<Integer> results, BTreeNode<Integer> root) {
        if (root != null) {
            addInOrder(results, root.getLeft());
            results.add(root.getValue());
            addInOrder(results, root.getRight());
        }
    }

}

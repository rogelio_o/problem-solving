package com.rogelioorts.training.gfg.articles;

public class Article17 {

  public int solve(int[] input, int n) {
    int[] rMax = new int[n];
    int[] lMin = new int[n];
    
    lMin[0] = input[0];
    for(int i = 1; i < n; i++) {
      lMin[i] = Math.min(lMin[i - 1], input[i]);
    }
    
    rMax[n - 1] = input[n - 1];
    for(int i = n - 2; i >= 0; i--) {
      rMax[i] = Math.max(rMax[i + 1], input[i]);
    }

    int result = Integer.MIN_VALUE;
    int i = 0;
    int j = 0;
    while(i < n && j < n) {
      if(lMin[i] < rMax[j]) {
        result = Math.max(result, j - i);
        j++;
      } else {
        i++;
      }
    }
    
    return result;
  }
  
}

package com.rogelioorts.training.gfg.articles;

public class Article30 {

  public int findMaxDiffBetweenSubarrays(int[] arr) {
    int[] leftMax = new int[arr.length];
    maxLeftSubArraySum(arr, leftMax);
    
    int[] rightMax = new int[arr.length];
    maxRightSubArraySum(arr, rightMax);
    
    int[] inverseArr = new int[arr.length];
    for(int i = 0; i < arr.length; i++) {
      inverseArr[i] = -arr[i];
    }
    
    int[] leftMin = new int[arr.length];
    maxLeftSubArraySum(inverseArr, leftMin);
    for(int i = 0; i < leftMin.length; i++) {
      leftMin[i] = -leftMin[i];
    }
    
    int[] rightMin = new int[arr.length];
    maxRightSubArraySum(inverseArr, rightMin);
    for(int i = 0; i < rightMin.length; i++) {
      rightMin[i] = -rightMin[i];
    }
    
    int result = Integer.MIN_VALUE;
    for(int i = 0; i < arr.length - 1; i++) {
      int partialResult = Math.max(
          Math.abs(leftMax[i] - rightMin[i + 1]), 
          Math.abs(leftMin[i] - rightMax[i + 1])
      );
      if(result < partialResult) {
        result = partialResult;
      }
    }
    
    return result;
  }
  
  private void maxLeftSubArraySum(int arr[], 
      int sum[]) {
    int maxSoFar = 0;
    int currentMax = arr[0];
    sum[0] = maxSoFar;
    
    for(int i = 1; i < arr.length; i++) {
      currentMax = Math.max(arr[i], currentMax + arr[i]);
      maxSoFar = Math.max(maxSoFar, currentMax);
      sum[i] = maxSoFar;
    }
  }
  
  private void maxRightSubArraySum(int arr[], 
      int sum[]) {
    int maxSoFar = arr[arr.length - 1];
    int currentMax = arr[arr.length - 1];
    sum[arr.length - 1] = maxSoFar;
    
    for(int i = arr.length - 2; i >= 0; i--) {
      currentMax = Math.max(arr[i], currentMax + arr[i]);
      maxSoFar = Math.max(maxSoFar, currentMax);
      sum[i] = maxSoFar;
    }
  }
  
}

package com.rogelioorts.training.gfg.articles;

public class Article13 {

  public int solve(int[] coins, int n) {
    return calculateForRange(coins, 0, n - 1);
  }
  
  public int calculateForRange(int[] coins, int i, int j) {
    if(i == j) {
      return coins[i];
    } else if(i + 1 == j) {
      return Math.max(coins[i], coins[j]);
    } else {
      return Math.max(
          coins[i] + Math.min(calculateForRange(coins, i + 2, j), calculateForRange(coins, i + 1, j - 1)),
          coins[j] + Math.min(calculateForRange(coins, i, j - 2), calculateForRange(coins, i + 1, j - 1))
      );
    }
  }
  
}

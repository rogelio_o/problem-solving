package com.rogelioorts.training.gfg.articles;

import java.util.Collections;
import java.util.PriorityQueue;

public class Article21 {
  
  private PriorityQueue<Integer>  left = new PriorityQueue<>(Collections.reverseOrder());
  
  private PriorityQueue<Integer> right = new PriorityQueue<>();
  
  private int median;

  public void add(int number) {
    if(left.size() > right.size()) {
      if(number < median) {
        right.add(left.poll());
        
        left.add(number);
      } else {
        right.add(number);
      }
      
      median = (left.peek() + right.peek()) / 2;
    } else if(left.size() < right.size()) {
      if(number > median) {
        left.add(right.poll());
        
        right.add(number);
      } else {
        left.add(number);
      }
      
      median = (left.peek() + right.peek()) / 2;
    } else {
      if(number < median) {
        left.add(number);
        
        median = left.peek();
      } else {
        right.add(number);
        
        median = right.peek();
      }
    }
  }
  
  public int getMedian() {
    return median;
  }
  
}

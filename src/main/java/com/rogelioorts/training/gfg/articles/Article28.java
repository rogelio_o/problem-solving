package com.rogelioorts.training.gfg.articles;

public class Article28 {

  public void transformSurroundedO(char[][] board) {
    replaceWith(board, 'O', '-');
    
    for(int i = 0; i < board.length; i++) {
      floodFill(board, i, 0, '-', 'O');
      floodFill(board, i, board[0].length - 1, '-', 'O');
    }
    
    for(int j = 0; j < board.length; j++) {
      floodFill(board, 0, j, '-', 'O');
      floodFill(board, board.length - 1, j, '-', 'O');
    }
    
    replaceWith(board, '-', 'X');
  }
  
  private void replaceWith(char[][] board, char oldChar, char newChar) {
    for(int i = 0; i < board.length; i++) {
      for(int j = 0; j < board.length; j++) {
        if(board[i][j] == oldChar) {
          board[i][j] = newChar;
        }
      }
    }
  }
  
  private void floodFill(char[][] board, int i, int j, char oldChar, char newChar) {
   if(!isValidPoint(board, i, j) || board[i][j] != oldChar) {
     return;
   }
   
   board[i][j] = newChar;
   
   floodFill(board, i, j + 1, oldChar, newChar);
   floodFill(board, i + 1, j, oldChar, newChar);
   floodFill(board, i, j - 1, oldChar, newChar);
   floodFill(board, i - 1, j, oldChar, newChar);
  }
  
  private boolean isValidPoint(char[][] board, int i, int j) {
    return i >= 0 && j >= 0 && i < board.length && j < board[0].length;
  }
  
}

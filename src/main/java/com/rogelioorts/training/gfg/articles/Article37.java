package com.rogelioorts.training.gfg.articles;

public class Article37 {

  public void fill(int[][] screen, int i, int j, int newColor) {
    fill(screen, i, j, newColor, screen[i][j]);
  }
  
  private void fill(int[][] screen, int i, int j, int newColor, int oldColor) {
    if(!isValidPoint(screen, i, j) || screen[i][j] != oldColor) {
      return;
    }
    
    screen[i][j] = newColor;
    
    fill(screen, i + 1, j, newColor, oldColor);
    fill(screen, i - 1, j, newColor, oldColor);
    fill(screen, i, j + 1, newColor, oldColor);
    fill(screen, i, j - 1, newColor, oldColor);
  }
  
  private boolean isValidPoint(int[][] screen, int i, int j) {
    return i >= 0 && j >= 0 && i < screen.length && j < screen[0].length;
  }
  
}

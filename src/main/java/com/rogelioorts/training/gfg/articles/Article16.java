package com.rogelioorts.training.gfg.articles;


public class Article16 {

  public int solve(int[] pages, int n, int m) {
    int totalPages = 0;
    for(int i = 0; i < n; i++) {
      totalPages += pages[i];
    }
    
    int start = 0;
    int end = totalPages;
    int result = Integer.MAX_VALUE;
    
    while(start <= end) {
      int mid = (start + end) / 2;
      
      if(isPossible(pages, n, m, mid)) {
        result = Math.min(result, mid);
        
        end = mid - 1;
      } else {
        start = mid + 1;
      }
    }
    
    return result;
  }
  
  private boolean isPossible(int[] pages, int n, int m, int maxNumPages) {
    int studentsRequired = 1;
    int currentSum = 0;
    
    for(int i = 0; i < n; i++) {
      if(pages[i] > maxNumPages) {
        return false;
      }
      
      if(currentSum + pages[i] > maxNumPages) {
        studentsRequired++;
        currentSum = pages[i];
        
        if(studentsRequired > m) {
          return false;
        }
      } else {
        currentSum += pages[i];
      }
    }
    
    return true;
  }
  
}

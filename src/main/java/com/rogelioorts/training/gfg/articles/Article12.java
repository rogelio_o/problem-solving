package com.rogelioorts.training.gfg.articles;

public class Article12 {

  public int solve(int[][] matrix, int n) {
    int a = 0;
    int b = n - 1;
    
    while(a < b) {
      if(knows(matrix, a, b)) {
        a++;
      } else {
        b--;
      }
    }
    
    for(int i = 0; i < n; i++) {
      if(i != a && (!knows(matrix, i, a) || knows(matrix, a, i))) {
        return -1;
      }
    }
    
    return a;
  }
  
  private boolean knows(int[][] matrix, int a, int b) {
    return matrix[a][b] == 1;
  }
  
}

package com.rogelioorts.training.gfg.articles;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article8 {

    public int solve(BTreeNode<Integer> root, int min, int max) {
        if (root == null) {
            return 0;
        }

        if (root.getValue() >= min && root.getValue() <= max) {
            return 1 + solve(root.getRight(), min, max) + solve(root.getLeft(), min, max);
        } else if (root.getValue() < min) {
            return solve(root.getRight(), min, max);
        } else {
            return solve(root.getLeft(), min, max);
        }
    }

}

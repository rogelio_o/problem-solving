package com.rogelioorts.training.gfg.utils;

import java.util.Iterator;

public class CustomLinkedList<T> implements Iterable<T> {

  private Node<T> tail;
  
  private Node<T> head;
  
  private int size;
  
  public CustomLinkedList() {
    this.size = 0;
  }
  
  public Node<T> add(T value) {
    Node<T> node = new Node<>(value, null, head);
    if(head != null) {
      head.previous = node;
    }
    head = node;
    if(tail == null) {
      tail = node;
    }
    
    this.size++;
    
    return node;
  }
  
  public T poll() {
    Node<T> result = tail;
    if(result != null) {
      remove(result);
      return result.getValue();
    } else {
      return null;
    }
  }
  
  public void remove(Node<T> node) {
    if(head.equals(node)) {
      head = node.next;
    }
    if(tail.equals(node)) {
      tail = node.previous;
    }
    if(node.next != null) {
      node.next.previous = node.previous;
    }
    if(node.previous != null) {
      node.previous.next = node.next;
    }
    this.size--;
  }
  
  public int size() {
    return size;
  }

  @Override
  public Iterator<T> iterator() {
    return new Iterator<T>() {
      
      private Node<T> next = head;

      @Override
      public boolean hasNext() {
        return next != null;
      }

      @Override
      public T next() {
        T result = next.getValue();
        next = next.next;
        return result;
      }
      
    };
  }
  
  public static class Node<T> {
    
    private T value;
    
    private Node<T> previous;
    
    private Node<T> next;
    
    public Node(T value, Node<T> previous, Node<T> next) {
      this.value = value;
      this.previous = previous;
      this.next = next;
    }
    
    public T getValue() {
      return value;
    }
    
    @Override
    public String toString() {
      return "NODE(" + value + ")";
    }
    
  }
  
}

package com.rogelioorts.training.gfg.utils;

public class BTreeNode<T> {
  
  private T value;
  
  private BTreeNode<T> left;
  
  private BTreeNode<T> right;
  
  private BTreeNode<T> nextRight;
  
  private BTreeNode<T> parent;

  public BTreeNode(T value) {
    this.value = value;
  }

  public BTreeNode<T> getLeft() {
    return left;
  }

  public void setLeft(BTreeNode<T> left) {
    this.left = left;
    if(left != null) {
      left.parent = this;
    }
  }

  public BTreeNode<T> getRight() {
    return right;
  }

  public void setRight(BTreeNode<T> right) {
    this.right = right;
    if(right != null) {
      right.parent = this;
    }
  }

  public T getValue() {
    return value;
  }
  
  public BTreeNode<T> getParent() {
    return parent;
  }  
  
  public BTreeNode<T> getNextRight() {
    return nextRight;
  }

  public void setNextRight(BTreeNode<T> nextRight) {
    this.nextRight = nextRight;
  }

  @Override
  public String toString() {
    return "[" + value + "]";
  }
  
}

package com.rogelioorts.training.gfg.utils;

import java.util.Arrays;

public final class Utils {

  private Utils() {
  }
  
  public static int[][] copyMatrix(int[][] input) {
    int[][] result = new int[input.length][];
    
    for(int i = 0; i < input.length; i++) {
      result[i] = Arrays.copyOf(input[i], input[i].length);
    }
    
    return result;
  }
  
}

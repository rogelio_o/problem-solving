package com.rogelioorts.training.gfg.utils;

import java.util.ArrayList;

public class TrieNode {
  
  private char value;

  private ArrayList<TrieNode> children;
  
  public TrieNode(char value) {
    this.value = value;
    this.children = new ArrayList<>();
  }
  
  public void addChildren(char c) {
    addChildren(new TrieNode(c));
  }
  
  public void addChildren(TrieNode node) {
    children.add(node);
  }
  
  public boolean isLeaf() {
    return children.isEmpty();
  }
  
  public char getValue() {
    return value;
  }
  
  public ArrayList<TrieNode> getChildren() {
    return children;
  }
  
}

package com.rogelioorts.training.gfg.utils;

import java.util.LinkedList;
import java.util.List;

public class Graph {

  private List<Integer>[] adjacencyList;
  
  @SuppressWarnings("unchecked")
  public Graph(int nVertices) {
    adjacencyList = new LinkedList[nVertices];
    for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) {
        adjacencyList[vertexIndex] = new LinkedList<>();
    }
  }
  
  public void addEdge(int addVertex, int endVertex) {
    adjacencyList[addVertex].add(endVertex);
  }
  
  public int size() {
    return adjacencyList.length;
  }
  
  public List<Integer> getAdjacencyListOfVertex(int vertex) {
    return this.adjacencyList[vertex];
  }
  
}

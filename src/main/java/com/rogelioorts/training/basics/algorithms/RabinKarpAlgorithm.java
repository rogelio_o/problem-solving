package com.rogelioorts.training.basics.algorithms;

public class RabinKarpAlgorithm {

    private static final int HASH_SALT = 26;

    public int indexOf(String text, String pattern) {
        if (text.length() < pattern.length()) {
            return -1;
        }

        int patternSize = pattern.length();
        int textSize = text.length();
        int lookedHash = calculateHash(pattern);
        String initialSubstr = text.substring(0, patternSize);
        int sumHashes = calculateHash(initialSubstr);

        if (sumHashes == lookedHash && initialSubstr.equals(pattern)) {
            return 0;
        }

        for (int i = patternSize; i < textSize; i++) {
            sumHashes -= calculateHash(text.charAt(i - patternSize)) * Math.pow(HASH_SALT, patternSize - 1);
            sumHashes *= HASH_SALT;
            sumHashes += calculateHash(text.charAt(i));

            if (sumHashes == lookedHash && text.substring(i - patternSize + 1, i + 1).equals(pattern)) {
                return i - patternSize + 1;
            }
        }

        return -1;
    }

    private int calculateHash(String str) {
        int result = 0;
        char[] cs = str.toCharArray();
        for (int i = 0; i < cs.length; i++) {
            char c = cs[i];
            result += calculateHash(c) * Math.pow(HASH_SALT, (cs.length - i - 1));
        }

        return result;
    }

    private int calculateHash(char c) {
        return c;
    }

}

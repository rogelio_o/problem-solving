package com.rogelioorts.training.basics.algorithms;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;

import com.rogelioorts.training.basics.data.structures.WeightedGraph;
import com.rogelioorts.training.basics.data.structures.WeightedGraph.Edge;

public class DijkstraAlgorithm {

    public LinkedList<Character> findShortestPath(WeightedGraph<Character> g, char start, char end) {
        HashMap<Character, Integer> pathWeight = new HashMap<>();
        pathWeight.put(start, 0);
        HashMap<Character, Character> previous = new HashMap<>();
        PriorityQueue<Character> remaining = new PriorityQueue<>((a, b) -> pathWeight.get(a) - pathWeight.get(b));

        for (Character c : g.getVertices()) {
            pathWeight.putIfAbsent(c, Integer.MAX_VALUE);
            remaining.add(c);
        }

        while (!remaining.isEmpty()) {
            Character c = remaining.poll();
            int weight = pathWeight.get(c);

            for (Edge<Character> e : g.getAdjacents(c)) {
                if (pathWeight.get(e.getNode()) > weight + e.getWeight()) {
                    pathWeight.put(e.getNode(), weight + e.getWeight());
                    previous.put(e.getNode(), c);
                }
            }
        }

        LinkedList<Character> result = new LinkedList<>();
        char c = end;
        result.addFirst(c);
        while (c != start) {
            c = previous.get(c);
            result.addFirst(c);
        }
        return result;
    }

}

package com.rogelioorts.training.basics.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;

public class AStarAlgorithm {

    public LinkedList<Point> findShortestPath(boolean[][] grid, Point start, Point end) {
        HashMap<Point, Integer> gScore = new HashMap<>();
        HashMap<Point, Integer> fScore = new HashMap<>();
        HashMap<Point, Point> cameFrom = new HashMap<>();
        PriorityQueue<Point> openSet = new PriorityQueue<>(
                (a, b) -> fScore.getOrDefault(a, Integer.MAX_VALUE) - fScore.getOrDefault(b, Integer.MAX_VALUE));
        HashSet<Point> closedSet = new HashSet<>();

        gScore.put(start, 0);
        fScore.put(start, calculateHeuristic(start, end));
        openSet.add(start);

        while (!openSet.isEmpty()) {
            Point current = openSet.poll();

            if (current.equals(end)) {
                return reconstructPath(cameFrom, start, end);
            }

            closedSet.add(current);

            for (Point child : getChildren(grid, current)) {
                if (!closedSet.contains(child)) {
                    int tentativeGScore = gScore.get(current) + distanceBetween(current, child);

                    if (openSet.contains(child)) {
                        if (tentativeGScore >= gScore.get(child)) {
                            continue;
                        }
                    } else {
                        openSet.add(child);
                    }

                    cameFrom.put(child, current);
                    gScore.put(child, tentativeGScore);
                    fScore.put(child, tentativeGScore + calculateHeuristic(child, end));
                }
            }
        }

        return null;
    }

    private ArrayList<Point> getChildren(boolean[][] grid, Point current) {
        ArrayList<Point> result = new ArrayList<>();

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (Math.abs(i) != Math.abs(j)) {
                    Point newPoint = new Point(current.x + i, current.y + j);
                    if (isValidPoint(grid, newPoint)) {
                        result.add(newPoint);
                    }
                }
            }
        }

        return result;
    }

    private boolean isValidPoint(boolean[][] grid, Point p) {
        return p.x >= 0 && p.y >= 0 && p.x < grid.length && p.y < grid[0].length && !grid[p.x][p.y];
    }

    private int calculateHeuristic(Point child, Point end) {
        return Math.abs(child.x - end.x) + Math.abs(child.y - end.y);
    }

    private int distanceBetween(Point current, Point child) {
        return 1;
    }

    private LinkedList<Point> reconstructPath(Map<Point, Point> cameFrom, Point start, Point end) {
        LinkedList<Point> result = new LinkedList<>();
        Point current = end;
        result.addFirst(end);

        while (current != start) {
            current = cameFrom.get(current);
            result.addFirst(current);
        }

        return result;
    }

    public static class Point {

        private int x;

        private int y;

        public Point(int x, int y) {
            super();
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + x;
            result = prime * result + y;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Point other = (Point) obj;
            if (x != other.x)
                return false;
            if (y != other.y)
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }

    }

}

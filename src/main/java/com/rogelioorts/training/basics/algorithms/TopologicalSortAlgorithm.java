package com.rogelioorts.training.basics.algorithms;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.rogelioorts.training.basics.data.structures.Graph;

public class TopologicalSortAlgorithm {

    public LinkedList<Character> sort(Graph<Character> graph) {
        LinkedList<Character> result = new LinkedList<>();
        LinkedList<Character> processNext = new LinkedList<>();
        HashMap<Character, Integer> inbound = countInbound(graph);

        for (Map.Entry<Character, Integer> entry : inbound.entrySet()) {
            if (entry.getValue() == 0) {
                processNext.add(entry.getKey());
            }
        }

        while (!processNext.isEmpty()) {
            Character c = processNext.removeFirst();
            for (Character c2 : graph.getAdjacents(c)) {
                int newInbound = inbound.get(c2) - 1;
                inbound.put(c2, newInbound);
                if (newInbound == 0) {
                    processNext.add(c2);
                }
            }
            result.add(c);
        }

        return result;
    }

    private HashMap<Character, Integer> countInbound(Graph<Character> graph) {
        HashMap<Character, Integer> result = new HashMap<>();

        for (Character c : graph.getVertices()) {
            int inbound = 0;
            for (Character c2 : graph.getVertices()) {
                if (c != c2 && graph.getAdjacents(c2).contains(c)) {
                    inbound++;
                }
            }
            result.put(c, inbound);
        }

        return result;
    }

}

package com.rogelioorts.training.basics.data.structures;

public class MyHashTable<T, W> {

    private static final int DEFAULT_SIZE = 20;

    private MyLinkedList<Entry<T, W>>[] table;

    @SuppressWarnings("unchecked")
    public MyHashTable(int initialCapacity) {
        table = new MyLinkedList[initialCapacity];
    }

    public MyHashTable() {
        this(DEFAULT_SIZE);
    }

    public W get(T key) {
        int index = getIndex(key);
        MyLinkedList<Entry<T, W>> list = table[index];

        Entry<T, W> entry = findEntryByKey(list, key);
        return entry == null ? null : entry.getValue();
    }

    private int getIndex(T key) {
        int hash = key == null ? 0 : key.hashCode();
        return hash % table.length;
    }

    public void put(T key, W value) {
        int index = getIndex(key);

        MyLinkedList<Entry<T, W>> list = table[index];
        if (list == null) {
            list = new MyLinkedList<>();
            table[index] = list;
        }

        list.add(new Entry<>(key, value));
    }

    public void remove(T key) {
        int index = getIndex(key);

        MyLinkedList<Entry<T, W>> list = table[index];
        Entry<T, W> entry = findEntryByKey(list, key);
        if (entry != null) {
            list.remove(entry);
        }
    }

    private Entry<T, W> findEntryByKey(MyLinkedList<Entry<T, W>> list, T key) {
        Entry<T, W> result = null;

        if (list != null) {
            for (Entry<T, W> entry : list) {
                if (areEqualsKeys(key, entry.getKey())) {
                    result = entry;
                    break;
                }
            }
        }

        return result;
    }

    private boolean areEqualsKeys(T key1, T key2) {
        return (key1 == null && key2 == null) || (key1 != null && key1.equals(key2));
    }

    public static class Entry<T, W> {

        private T key;

        private W value;

        public Entry(T key, W value) {
            this.key = key;
            this.value = value;
        }

        public T getKey() {
            return key;
        }

        public W getValue() {
            return value;
        }

    }

}

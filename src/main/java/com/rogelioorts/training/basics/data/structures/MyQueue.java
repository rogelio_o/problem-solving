package com.rogelioorts.training.basics.data.structures;

import java.util.NoSuchElementException;

public class MyQueue<T> {

    private Node head;

    private Node tail;

    public void add(T value) {
        Node node = new Node(value);

        if (head != null) {
            head.prev = node;
        }
        if (tail == null) {
            tail = node;
        }

        head = node;
    }

    public T remove() {
        if (tail == null) {
            throw new NoSuchElementException();
        }

        T value = tail.value;

        if (head == tail) {
            head = null;
        }

        tail = tail.prev;

        return value;
    }

    private class Node {

        private T value;

        private Node prev;

        public Node(T value) {
            this.value = value;
        }

    }

}

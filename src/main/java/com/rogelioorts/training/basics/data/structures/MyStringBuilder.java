package com.rogelioorts.training.basics.data.structures;

public class MyStringBuilder {

    private MyArrayList<Character> str;

    public MyStringBuilder(int initialSize) {
        str = new MyArrayList<>(initialSize);
    }

    public void append(Character c) {
        str.add(c);
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < str.size(); i++) {
            result += str.get(i);
        }

        return result;
    }

    MyArrayList<Character> getStr() {
        return str;
    }

}

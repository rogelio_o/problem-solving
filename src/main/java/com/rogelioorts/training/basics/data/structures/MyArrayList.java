package com.rogelioorts.training.basics.data.structures;

public class MyArrayList<T> {

    private static final int DEFAULT_SIZE = 20;

    private Object[] array;

    private int index = 0;

    public MyArrayList(int initialSize) {
        array = new Object[initialSize];
    }

    public MyArrayList() {
        this(DEFAULT_SIZE);
    }

    public void add(T value) {
        checkSize();

        array[index] = value;
        index++;
    }

    private void checkSize() {
        if (index == array.length) {
            Object[] copy = array;
            array = new Object[copy.length * 2];
            for (int i = 0; i < copy.length; i++) {
                array[i] = copy[i];
            }
        }
    }

    public int size() {
        return index;
    }

    @SuppressWarnings("unchecked")
    public T remove(int removeIndex) {
        if (removeIndex >= index) {
            throw new IndexOutOfBoundsException();
        }

        T result = (T) array[removeIndex];

        for (int i = removeIndex; i < index; i++) {
            if (i + 1 < array.length) {
                array[i] = array[i + 1];
            } else {
                array[i] = null;
            }
        }

        index--;

        return result;
    }

    @SuppressWarnings("unchecked")
    public T get(int getIndex) {
        if (getIndex >= index) {
            throw new IndexOutOfBoundsException();
        }

        return (T) array[getIndex];
    }

    Object[] getArray() {
        return array;
    }

}

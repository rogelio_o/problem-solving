package com.rogelioorts.training.basics.data.structures;

import java.util.NoSuchElementException;

public class MyStack<T> {

    private Node head;

    public void push(T value) {
        Node n = new Node(value, head);
        head = n;
    }

    public T pop() {
        if (head == null) {
            throw new NoSuchElementException();
        }

        T value = head.value;
        head = head.next;
        return value;
    }

    private class Node {

        private T value;

        private Node next;

        public Node(T value, Node next) {
            this.value = value;
            this.next = next;
        }

    }

}

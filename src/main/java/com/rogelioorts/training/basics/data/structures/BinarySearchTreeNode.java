package com.rogelioorts.training.basics.data.structures;

public class BinarySearchTreeNode<T extends Comparable<T>> {

    private T data;

    private BinarySearchTreeNode<T> left;

    private BinarySearchTreeNode<T> right;

    private BinarySearchTreeNode<T> parent;

    private int size;

    private int leftSize;

    private int rightSize;

    public BinarySearchTreeNode(T data) {
        this.data = data;
        this.size = 1;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public BinarySearchTreeNode<T> getLeft() {
        return left;
    }

    public void setLeft(T left) {
        setLeft(new BinarySearchTreeNode<>(left));
    }

    public void setLeft(BinarySearchTreeNode<T> left) {
        if (this.left == null) {
            leftSize = 1;
        }
        if (left != null) {
            left.parent = this;
            this.size++;
        }
        this.left = left;
    }

    public BinarySearchTreeNode<T> getRight() {
        return right;
    }

    public void setRight(T right) {
        setRight(new BinarySearchTreeNode<>(right));
    }

    public void setRight(BinarySearchTreeNode<T> right) {
        if (this.right == null) {
            rightSize = 1;
        }
        if (right != null) {
            right.parent = this;
            this.size++;
        }
        this.right = right;
    }

    public BinarySearchTreeNode<T> getParent() {
        return parent;
    }

    public int getSize() {
        return size;
    }

    public int getLeftSize() {
        return leftSize;
    }

    public int getRightSize() {
        return rightSize;
    }

    public void setParent(BinarySearchTreeNode<T> parent) {
        this.parent = parent;
    }

    public void insert(T newData) {
        insert(new BinarySearchTreeNode<>(newData));
    }

    protected void insert(BinarySearchTreeNode<T> newNode) {
        if (newNode.data.compareTo(data) < 0) {
            if (left == null) {
                setLeft(newNode);
            } else {
                left.insert(newNode);
                this.size++;
                this.leftSize++;
            }
        } else {
            if (right == null) {
                setRight(newNode);
            } else {
                right.insert(newNode);
                this.size++;
                this.rightSize++;
            }
        }
    }

    public BinarySearchTreeNode<T> find(T lookedUpData) {
        if (lookedUpData.compareTo(data) == 0) {
            return this;
        } else if (lookedUpData.compareTo(data) < 0) {
            return left == null ? null : left.find(lookedUpData);
        } else {
            return right == null ? null : right.find(lookedUpData);
        }
    }

    public boolean isRoot() {
        return parent == null;
    }

}

package com.rogelioorts.training.basics.data.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Graph<T> {

    private Map<T, ArrayList<T>> adjacencyList = new HashMap<>();

    public void addVertex(T vertex) {
        adjacencyList.put(vertex, new ArrayList<>());
    }

    public ArrayList<T> getAdjacents(T vertex) {
        return adjacencyList.get(vertex);
    }

    public void addEdge(T from, T to) {
        adjacencyList.get(from).add(to);
    }

    public Set<T> getVertices() {
        return adjacencyList.keySet();
    }

}

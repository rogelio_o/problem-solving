package com.rogelioorts.training.basics.data.structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedList<T> implements Iterable<T> {

    private Node head;

    private int size;

    public void add(T value) {
        Node newNode = new Node(value, head);
        head = newNode;
        size++;
    }

    public T remove() {
        if (head == null) {
            throw new NoSuchElementException();
        }

        T result = head.data;
        head = head.next;

        size--;

        return result;
    }

    public void remove(T value) {
        Node p = head;
        if (p != null && value.equals(p.data)) {
            head = p.next;
        } else {
            while (p != null) {
                if (p.next != null && value.equals(p.next.data)) {
                    p.next = p.next.next;
                    break;
                }

                p = p.next;
            }
        }

        size--;
    }

    public T peek() {
        if (head == null) {
            throw new NoSuchElementException();
        }

        return head.data;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private Node current = head;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                if (current == null) {
                    throw new NoSuchElementException();
                }
                T result = current.data;
                current = current.next;
                return result;
            }

        };
    }

    public int size() {
        return this.size;
    }

    private class Node {

        private Node next;

        private T data;

        public Node(T data, Node next) {
            this.data = data;
            this.next = next;
        }

    }

}

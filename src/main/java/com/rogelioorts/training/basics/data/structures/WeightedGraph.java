package com.rogelioorts.training.basics.data.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WeightedGraph<T> {

    private Map<T, ArrayList<Edge<T>>> adjacencyList = new HashMap<>();

    public void addVertex(T vertex) {
        adjacencyList.put(vertex, new ArrayList<>());
    }

    public ArrayList<Edge<T>> getAdjacents(T vertex) {
        return adjacencyList.get(vertex);
    }

    public void addEdge(T from, T to, int weight) {
        adjacencyList.get(from).add(new Edge<>(to, weight));
    }

    public Set<T> getVertices() {
        return adjacencyList.keySet();
    }

    public static class Edge<T> {

        private T node;

        private int weight;

        public Edge(T node, int weight) {
            this.node = node;
            this.weight = weight;
        }

        public T getNode() {
            return node;
        }

        public int getWeight() {
            return weight;
        }

    }

}

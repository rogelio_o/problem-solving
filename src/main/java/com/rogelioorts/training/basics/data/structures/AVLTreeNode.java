package com.rogelioorts.training.basics.data.structures;

public class AVLTreeNode<T extends Comparable<T>> extends BinarySearchTreeNode<T> {

    private int size = 1;

    public AVLTreeNode(T data) {
        super(data);
    }

    @Override
    public void insert(T newData) {
        insert(new AVLTreeNode<>(newData));
    }

    @Override
    protected void insert(BinarySearchTreeNode<T> node) {
        super.insert(node);
        size++;

        balance();
    }

    private void balance() {
        if (!isBalanced()) {
            AVLTreeNode<T> z = this;
            AVLTreeNode<T> y = (AVLTreeNode<T>) (balanceFactor() > 0 ? getRight() : getLeft());
            AVLTreeNode<T> x = (AVLTreeNode<T>) (y.balanceFactor() > 0 ? y.getRight() : y.getLeft());

            if (isLeftLeftCase(z, y, x)) {
                rightRotate(z, y);
            } else if (isLeftRightCase(z, y, x)) {
                leftRotate(y, x);
                rightRotate(z, y);
            } else if (isRightRightCase(z, y, x)) {
                leftRotate(z, y);
            } else if (isRightLeftCase(z, y, x)) {
                rightRotate(y, x);
                leftRotate(z, y);
            }
        }
    }

    private boolean isLeftLeftCase(AVLTreeNode<T> z, AVLTreeNode<T> y, AVLTreeNode<T> x) {
        return y == z.getLeft() && x == y.getLeft();
    }

    private boolean isLeftRightCase(AVLTreeNode<T> z, AVLTreeNode<T> y, AVLTreeNode<T> x) {
        return y == z.getLeft() && x == y.getRight();
    }

    private boolean isRightRightCase(AVLTreeNode<T> z, AVLTreeNode<T> y, AVLTreeNode<T> x) {
        return y == z.getRight() && x == y.getRight();
    }

    private boolean isRightLeftCase(AVLTreeNode<T> z, AVLTreeNode<T> y, AVLTreeNode<T> x) {
        return y == z.getRight() && x == y.getLeft();
    }

    private void rightRotate(AVLTreeNode<T> z, AVLTreeNode<T> y) {
        swapNodes(z, y);
        AVLTreeNode<T> aux = y;
        y = z;
        z = aux;

        z.setLeft(y.getRight());
        y.setRight(z);
    }

    private void leftRotate(AVLTreeNode<T> z, AVLTreeNode<T> y) {
        swapNodes(z, y);
        AVLTreeNode<T> aux = y;
        y = z;
        z = aux;

        z.setRight(y.getLeft());
        y.setLeft(z);
    }

    private boolean isBalanced() {
        return Math.abs(balanceFactor()) <= 1;
    }

    private int balanceFactor() {
        return rightHeight() - leftHeight();
    }

    private int leftHeight() {
        return getLeft() == null ? 0 : ((AVLTreeNode<T>) getLeft()).size;
    }

    private int rightHeight() {
        return getRight() == null ? 0 : ((AVLTreeNode<T>) getRight()).size;
    }

    private static <T extends Comparable<T>> void swapNodes(AVLTreeNode<T> a, AVLTreeNode<T> b) {
        AVLTreeNode<T> leftAux = (AVLTreeNode<T>) a.getLeft();
        a.setLeft(b.getLeft());
        b.setLeft(leftAux);

        AVLTreeNode<T> rightAux = (AVLTreeNode<T>) a.getRight();
        a.setRight(b.getRight());
        b.setRight(rightAux);

        AVLTreeNode<T> parentAux = (AVLTreeNode<T>) a.getParent();
        a.setParent(b.getParent());
        b.setParent(parentAux);

        int auxSize = a.size;
        a.size = b.size;
        b.size = auxSize;

        T aux = a.getData();
        a.setData(b.getData());
        b.setData(aux);
    }

}

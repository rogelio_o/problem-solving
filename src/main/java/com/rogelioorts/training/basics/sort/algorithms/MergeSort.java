package com.rogelioorts.training.basics.sort.algorithms;

public class MergeSort {

    public static void sort(int[] array) {
        sort(array, 0, array.length);
    }

    private static void sort(int[] array, int start, int end) {
        int size = end - start;
        if (size <= 1) {
            return;
        }

        int m = size / 2;

        sort(array, start, start + m);
        sort(array, start + m, end);

        int[] firstMiddle = copyArray(array, start, start + m);
        int mainIndex = start;
        int firstMiddleIndex = 0;
        int lastMiddleIndex = start + m;
        while (firstMiddleIndex < m && lastMiddleIndex < end) {
            if (firstMiddle[firstMiddleIndex] < array[lastMiddleIndex]) {
                array[mainIndex] = firstMiddle[firstMiddleIndex];
                firstMiddleIndex++;
            } else {
                array[mainIndex] = array[lastMiddleIndex];
                lastMiddleIndex++;
            }
            mainIndex++;
        }
        while (firstMiddleIndex < m) {
            array[mainIndex] = firstMiddle[firstMiddleIndex];
            mainIndex++;
            firstMiddleIndex++;
        }
    }

    private static int[] copyArray(int[] array, int start, int end) {
        int[] result = new int[end - start];

        int index = 0;
        for (int i = start; i < end; i++) {
            result[index] = array[i];
            index++;
        }

        return result;
    }

}

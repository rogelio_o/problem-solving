package com.rogelioorts.training.basics.sort.algorithms;

import java.util.ArrayList;

public class RadixSort {

    public static void sort(int[] array) {
        int max = getMax(array);

        for (int i = 0; i < countNumDigits(max); i++) {
            sort(array, i);
        }
    }

    private static int getMax(int[] array) {
        int result = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > result) {
                result = array[i];
            }
        }

        return result;
    }

    private static int countNumDigits(int num) {
        return (int) (Math.log10((double) num) + 1);
    }

    @SuppressWarnings("unchecked")
    private static void sort(int[] array, int digitIndex) {
        ArrayList<Integer>[] output = new ArrayList[10];
        for (int i = 0; i < output.length; i++) {
            output[i] = new ArrayList<>();
        }

        for (int i = 0; i < array.length; i++) {
            int d = getDigit(array[i], digitIndex);
            output[d].add(array[i]);
        }

        for (int i = 0, j = 0; i < output.length; i++) {
            for (Integer num : output[i]) {
                array[j] = num;
                j++;
            }
        }
    }

    private static int getDigit(int num, int digitIndex) {
        return (int) ((num / Math.pow(10, digitIndex)) % 10);
    }

}

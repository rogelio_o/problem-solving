package com.rogelioorts.training.basics.sort.algorithms;

public class BubbleSort {

    public static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean swapped = false;
            for (int j = array.length - 1; j > i; j--) {
                if (array[j] < array[j - 1]) {
                    int aux = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = aux;

                    swapped = true;
                }
            }

            if (!swapped) {
                break;
            }
        }
    }

}

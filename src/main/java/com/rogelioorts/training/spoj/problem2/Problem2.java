package com.rogelioorts.training.spoj.problem2;

import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numProblems = scanner.nextInt();
        scanner.nextLine();
        String[] formulas = new String[numProblems];
        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            formulas[problemIndex] = scanner.nextLine();
        }

        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            String formula = formulas[problemIndex];
            System.out.println(transformToRPN(formula));
        }

        scanner.close();
    }

    private static String transformToRPN(String formula) {
        String wihoutParentheses = removeParentheses(formula);

        if (wihoutParentheses.length() == 1) {
            return wihoutParentheses;
        } else {
            String operandA = getOperand(wihoutParentheses);
            String operation = wihoutParentheses.substring(operandA.length(), operandA.length() + 1);
            String operandB = getOperand(wihoutParentheses.substring(operandA.length() + 1));

            return transformToRPN(operandA) + transformToRPN(operandB) + operation;
        }
    }

    private static String removeParentheses(String formula) {
        if (formula.startsWith("(") && formula.endsWith(")")) {
            return formula.substring(1, formula.length() - 1);
        } else {
            return formula;
        }
    }

    private static String getOperand(String formula) {
        if (formula.startsWith("(")) {
            int lastParenthesisIndex = findLastParenthesis(formula);
            return formula.substring(0, lastParenthesisIndex + 1);
        } else {
            return formula.substring(0, 1);
        }
    }

    private static int findLastParenthesis(String formula) {
        int open = 1;
        for (int i = 1; i < formula.length(); i++) {
            if (formula.charAt(i) == '(') {
                open++;
            } else if (formula.charAt(i) == ')') {
                open--;
            }

            if (open == 0) {
                return i;
            }
        }

        return -1;
    }

}

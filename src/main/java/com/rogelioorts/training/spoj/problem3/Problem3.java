package com.rogelioorts.training.spoj.problem3;

import java.math.BigInteger;
import java.util.Scanner;

public class Problem3 {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int numProblems = scanner.nextInt();
        String[] problems = new String[numProblems];
        scanner.nextLine();
        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            problems[problemIndex] = scanner.nextLine();
        }

        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            String num = problems[problemIndex];
            System.out.println(getNextPalindrome(num));
        }

        scanner.close();
    }

    private static String getNextPalindrome(String num) {
        int len = num.length();
        String left = num.substring(0, len / 2);
        String middle = num.substring(len / 2, len - len / 2);
        String right = num.substring(len - len / 2);

        if (right.compareTo(reverse(left)) < 0)
            return left + middle + reverse(left);

        String next = new BigInteger(left + middle).add(BigInteger.ONE).toString();
        return next.substring(0, left.length() + middle.length()) + reverse(next).substring(middle.length());
    }

    private static String reverse(String s) {
        return new StringBuilder(s).reverse().toString();
    }

}

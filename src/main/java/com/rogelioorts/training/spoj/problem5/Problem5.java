package com.rogelioorts.training.spoj.problem5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.Stream;

public class Problem5 {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        String strArray = scanner.next();
        int k = scanner.nextInt();

        System.out.println(
                getMaxProductOfArray(Stream.of(strArray.split(",")).map(Integer::valueOf).toArray(Integer[]::new), k));

        scanner.close();
    }

    private static int getMaxProductOfArray(Integer[] arr, int k) {
        Comparator<Integer> comparator = Comparator.comparingInt(Math::abs);
        Arrays.sort(arr, comparator.reversed());

        Integer[] maxNumbers = Arrays.copyOfRange(arr, 0, k);

        if (!hasEvenNegativeNumbers(maxNumbers)) {
            int negativeNumberIndex = -1;
            for (int i = maxNumbers.length - 1; i >= 0; i--) {
                if (maxNumbers[i] < 0) {
                    negativeNumberIndex = i;
                    break;
                }
            }

            if (negativeNumberIndex >= 0) {
                for (int i = k; i < arr.length; i++) {
                    if (arr[i] >= 0) {
                        maxNumbers[negativeNumberIndex] = arr[i];
                        break;
                    }
                }
            }
        }

        int result = 1;
        for (int i = 0; i < maxNumbers.length; i++) {
            result *= maxNumbers[i];
        }

        return result;
    }

    private static boolean hasEvenNegativeNumbers(Integer[] maxNumbers) {
        int numNegatives = Stream.of(maxNumbers).mapToInt(n -> n < 0 ? 1 : 0).sum();
        return numNegatives % 2 == 0;
    }

}

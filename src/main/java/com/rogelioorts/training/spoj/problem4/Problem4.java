package com.rogelioorts.training.spoj.problem4;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem4 {

    private static final Pattern FORMULA_PATTERN = Pattern.compile("(\\d+)(\\+|\\-|\\*|\\/)(\\d+)");

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int numProblems = scanner.nextInt();
        String[] problems = new String[numProblems];
        scanner.nextLine();
        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            problems[problemIndex] = scanner.nextLine();
        }

        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            String num = problems[problemIndex];
            Matcher m = FORMULA_PATTERN.matcher(num);
            if (m.find()) {
                System.out.println(getExtendedFormula(m.group(1), m.group(2), m.group(3)));
                System.out.println("");
            }
        }

        scanner.close();
    }

    private static String getExtendedFormula(String aOperand, String operation, String bOperand) {
        StringBuilder result = new StringBuilder();
        String operationResult = getOperationResult(aOperand, operation, bOperand);
        int firstLineBelowSize = getFirstLineBelowSize(aOperand, operation, bOperand, operationResult);
        int operandPartMaxSize = Math.max(firstLineBelowSize, Math.max(aOperand.length(), bOperand.length() + 1));
        int multiplyPartMaxSize = calcMultiplyPartMaxSize(aOperand, operation, bOperand, operationResult);
        int numWhiteSpaces = Math.max(operandPartMaxSize, multiplyPartMaxSize);

        result.append(generateLine(numWhiteSpaces, aOperand)).append(generateLine(numWhiteSpaces, operation + bOperand))
                .append(generateLine(numWhiteSpaces,
                        generateCharacters("-", Math.max(firstLineBelowSize, bOperand.length() + 1))));

        if (hasNotMultiplyPart(operation, bOperand)) {
            result.append(generateLineWithoutLineBreak(numWhiteSpaces, operationResult));
        } else {
            BigInteger a = new BigInteger(aOperand);
            for (int i = bOperand.length() - 1; i >= 0; i--) {
                int mult = Character.getNumericValue(bOperand.charAt(i));
                String line = a.multiply(BigInteger.valueOf(mult)).toString();
                int partWhiteSpaces = numWhiteSpaces - (bOperand.length() - (i + 1));
                result.append(generateLine(partWhiteSpaces, line));
            }

            result.append(generateLine(numWhiteSpaces, generateCharacters("-", multiplyPartMaxSize)))
                    .append(generateLineWithoutLineBreak(numWhiteSpaces, operationResult));
        }

        return result.toString();
    }

    private static String generateLineWithoutLineBreak(int numWhiteSpaces, String line) {
        StringBuilder result = new StringBuilder();
        return result.append(generateWhiteSpaces(numWhiteSpaces - line.length())).append(line).toString();
    }

    private static String generateLine(int numWhiteSpaces, String line) {
        StringBuilder result = new StringBuilder();
        return result.append(generateWhiteSpaces(numWhiteSpaces - line.length())).append(line).append("\n").toString();
    }

    private static String generateWhiteSpaces(int numWhiteSpaces) {
        return generateCharacters(" ", numWhiteSpaces);
    }

    private static String generateCharacters(String character, int repetition) {
        return IntStream.range(0, repetition).mapToObj(i -> character).collect(Collectors.joining(""));
    }

    private static String getOperationResult(String aOperand, String operation, String bOperand) {
        BigInteger a = new BigInteger(aOperand);
        BigInteger b = new BigInteger(bOperand);

        switch (operation) {
        case "+":
            return a.add(b).toString();
        case "-":
            return a.subtract(b).toString();
        case "*":
            return a.multiply(b).toString();
        default:
            throw new IllegalArgumentException("Operation " + operation + " not valid.");
        }
    }

    private static int getFirstLineBelowSize(String aOperand, String operation, String bOperand,
            String operationResult) {
        if (hasNotMultiplyPart(operation, bOperand)) {
            return operationResult.length();
        } else {
            int firstDigitB = Integer.valueOf(bOperand.substring(bOperand.length() - 1, bOperand.length()));
            BigInteger firstPartNum = new BigInteger(aOperand).multiply(BigInteger.valueOf(firstDigitB));

            return firstPartNum.toString().length();
        }
    }

    private static int calcMultiplyPartMaxSize(String aOperand, String operation, String bOperand,
            String operationResult) {
        if (hasNotMultiplyPart(operation, bOperand)) {
            return 0;
        } else {
            int lastDigitB = Integer.valueOf(bOperand.substring(0, 1));
            BigInteger lastPartNum = new BigInteger(aOperand).multiply(BigInteger.valueOf(lastDigitB));

            return Math.max(operationResult.length(), String.valueOf(lastPartNum).length() + bOperand.length() - 1);
        }
    }

    private static boolean hasNotMultiplyPart(String operation, String bOperand) {
        return !"*".equals(operation) || bOperand.length() == 1;
    }
}

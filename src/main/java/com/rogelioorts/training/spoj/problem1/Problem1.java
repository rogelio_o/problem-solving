package com.rogelioorts.training.spoj.problem1;

import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numProblems = scanner.nextInt();
        Bounds[] problems = new Bounds[numProblems];
        scanner.nextLine();
        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            String boundsStr = scanner.nextLine();
            String[] bounds = boundsStr.split(" ");

            problems[problemIndex] = new Bounds(Integer.valueOf(bounds[0]), Integer.valueOf(bounds[1]));
        }

        for (int problemIndex = 0; problemIndex < numProblems; problemIndex++) {
            Bounds bounds = problems[problemIndex];

            generatePrimes(bounds.from, bounds.to);
        }

        scanner.close();
    }

    private static void generatePrimes(int from, int to) {
        int fromOdd;
        if (from <= 2) {
            System.out.println("2");
            fromOdd = 3;
        } else {
            fromOdd = from % 2 == 0 ? from + 1 : from;
        }
        for (int num = fromOdd; num <= to; num += 2) {
            if (isPrime(num)) {
                System.out.println(num);
            }
        }
        System.out.println("");
    }

    private static boolean isPrime(int num) {
        for (int candidate = 3; candidate <= (int) Math.sqrt(num); candidate++) {
            if (num % candidate == 0) {
                return false;
            }
        }

        return true;
    }

    private static class Bounds {

        private int from;

        private int to;

        public Bounds(int from, int to) {
            this.from = from;
            this.to = to;
        }

    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

public class Problem4 {
  
  private static final int DEFAULT_END = 5;
  
  public int findElement(Listy list, int element) {
    return findElement(list, element, 0, DEFAULT_END);
  }
  
  private int findElement(Listy list, int element, int start, int end) {
    if(start > end) {
      return -1;
    }
    
    int middle = (start + end) / 2;
    int middleValue = list.elementAt(middle);
    
    if(middleValue == -1) {
      return findElement(list, element, start, middle - 1);
    } else if(middleValue == element) {
      return middle;
    } else if(middleValue < element) {
      if(list.elementAt(end) > -1 && list.elementAt(end) < element) {
        return findElement(list, element, start, end * 2);
      } else {
        return findElement(list, element, middle + 1, end);
      }
    } else {
      return findElement(list, element, start, middle - 1);
    }
  }

  public static class Listy {
    
    private int[] arr;
    
    public Listy(int[] arr) {
      this.arr = arr;
    }
    
    public int elementAt(int index) {
      if(arr.length > index) {
        return arr[index];
      } else {
        return -1;
      }
    }
    
  }
  
}

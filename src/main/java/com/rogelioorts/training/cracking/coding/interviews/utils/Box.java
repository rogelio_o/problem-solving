package com.rogelioorts.training.cracking.coding.interviews.utils;

public class Box {

    private int weight;

    private int height;

    private int depth;

    public Box(int weight, int height, int depth) {
        this.weight = weight;
        this.height = height;
        this.depth = depth;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem10 {

  private BinarySearchTreeNode<Integer> root;

  public void track(int num) {
    if (root == null) {
      root = new BinarySearchTreeNode<>(num);
    } else {
      root.insert(num);
    }
  }

  public int getRankOfNumber(int num) {
    BinarySearchTreeNode<Integer> node = root;
    int result = 0;
    while (node != null) {
      if (node.getData().equals(num)) {
        result += node.getLeftSize();
        return result;
      } else if (node.getData().compareTo(num) > 0) {
        node = node.getLeft();
      } else {
        result += node.getLeftSize() + 1;
        node = node.getRight();
      }
    }
    return -1;
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Problem2 {

  public void sortAnagrams(String[] arr) {
    HashMap<String, ArrayList<String>> map = new HashMap<>();
    
    for(String word : arr) {
      addToMap(map, sortedChars(word), word);
    }
    
    int index = 0;
    for(String key : map.keySet()) {
      for(String word : map.get(key)) {
        arr[index++] = word;
      }
    }
  }
  
  private void addToMap(HashMap<String, ArrayList<String>> map, String key, String word) {
    if(map.containsKey(key)) {
      map.get(key).add(word);
    } else {
      ArrayList<String> words = new ArrayList<>();
      words.add(word);
      map.put(key, words);
    }
  }
  
  private String sortedChars(String word) {
    char[] result = word.toCharArray();
    Arrays.sort(result);
    return new String(result);
  }
  
}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.rogelioorts.training.cracking.coding.interviews.utils.Box;

public class Problem13 {

    public int getMaxStackHeight(ArrayList<Box> stack) {
        Collections.sort(stack, new BoxComparator());

        int maxHeight = 0;
        int[] cache = new int[stack.size()];
        for (int i = 0; i < stack.size(); i++) {
            int localMax = getMaxStackHeight(stack, i, cache);
            if (localMax > maxHeight) {
                maxHeight = localMax;
            }
        }

        return maxHeight;
    }

    private int getMaxStackHeight(ArrayList<Box> stack, int bottomIndex, int[] cache) {
        if (bottomIndex < stack.size() && cache[bottomIndex] > 0) {
            return cache[bottomIndex];
        }

        Box bottomBox = stack.get(bottomIndex);
        int maxHeight = 0;
        for (int i = bottomIndex + 1; i < stack.size(); i++) {
            if (canBeAbove(bottomBox, stack.get(i))) {
                int localHeight = getMaxStackHeight(stack, i, cache);
                if (maxHeight < localHeight) {
                    maxHeight = localHeight;
                }
            }
        }

        maxHeight += bottomBox.getHeight();
        cache[bottomIndex] = maxHeight;
        return maxHeight;
    }

    private boolean canBeAbove(Box bottomBox, Box aboveBox) {
        return aboveBox.getDepth() < bottomBox.getDepth() && aboveBox.getHeight() < bottomBox.getHeight()
                && aboveBox.getWeight() < bottomBox.getWeight();
    }

    private class BoxComparator implements Comparator<Box> {

        @Override
        public int compare(Box o1, Box o2) {
            return o2.getHeight() - o1.getHeight();
        }

    }

}

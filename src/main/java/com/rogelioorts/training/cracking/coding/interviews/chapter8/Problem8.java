package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.ArrayList;
import java.util.HashSet;

public class Problem8 {

  public ArrayList<String> getPermutations(String str) {
    HashSet<Character> previous = new HashSet<>();
    ArrayList<String> results = new ArrayList<>();
    results.add("");
    
    for(char c : str.toCharArray()) {
      if(previous.add(c)) {
        ArrayList<String> subresults = new ArrayList<>();
        for(String s : results) {
          subresults.addAll(charPermutation(c, s));
        }
        results = subresults;
      }
    }
    
    return results;
  }
  
  private ArrayList<String> charPermutation(char c, String s) {
    ArrayList<String> result = new ArrayList<>();
    
    for(int i = 0; i < s.length() + 1; i++) {
      result.add(s.substring(0, i) + c + s.substring(i, s.length()));
    }
    
    return result;
  }
  
}

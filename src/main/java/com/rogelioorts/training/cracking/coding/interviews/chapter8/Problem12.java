package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.ArrayList;

public class Problem12 {

  public ArrayList<Integer[]> solveQueens(int n) {
    ArrayList<Integer[]> result = new ArrayList<>();
    Integer[] columns = new Integer[n];
    
    solveQueens(n, columns, 0, result);
    
    return result;
  }
  
  private void solveQueens(int n, Integer[] columns, int row, ArrayList<Integer[]> result) {
    if(row == n) {
      result.add(columns.clone());
      return;
    }
    
    for(int i = 0; i < n; i++) {
      if(isValidColumn(n, columns, row, i)) {
        columns[row] = i;
        solveQueens(n, columns, row + 1, result);
      }
    }
  }
  
  private boolean isValidColumn(int n, Integer[] columns, int row1, int column1) {
    for(int row2 = 0; row2 < row1; row2++) {
      int column2 = columns[row2];
      
      if(column1 == column2) {
        return false;
      } else if(Math.abs(column1 - column2) == Math.abs(row1 - row2)) {
        return false;
      }
    }
    
    return true;
  }
  
}

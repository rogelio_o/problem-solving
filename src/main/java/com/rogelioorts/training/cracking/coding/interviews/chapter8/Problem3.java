package com.rogelioorts.training.cracking.coding.interviews.chapter8;

public class Problem3 {

  public int findFirstMagicIndex(int[] arr) {
    return findFirstMagicIndex(arr, 0, arr.length - 1);
  }

  private int findFirstMagicIndex(int[] arr, int start, int end) {
    if (start > end) {
      return -1;
    }

    int midIndex = (start + end) / 2;
    int midValue = arr[midIndex];
    if (midValue == midIndex) {
      return midIndex;
    }

    int leftIndex = Math.min(midIndex - 1, midValue);
    int left = findFirstMagicIndex(arr, start, leftIndex);
    if (left >= 0) {
      return left;
    }

    int rightIndex = Math.max(midIndex + 1, midValue);
    int right = findFirstMagicIndex(arr, rightIndex, end);

    return right;
  }

}

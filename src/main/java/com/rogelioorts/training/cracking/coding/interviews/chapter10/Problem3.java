package com.rogelioorts.training.cracking.coding.interviews.chapter10;

public class Problem3 {

  public int findIndex(int[] arr, int element) {
    return findIndex(arr, element, 0, arr.length - 1);
  }
  
  private int findIndex(int[] arr, int element, int start, int end) {
    if(start > end) {
      return -1;
    }
    
    int middle = (start + end) / 2;
    
    if(arr[middle] == element) {
      return middle;
    } else if(arr[start] < arr[middle]) {
      if(arr[middle] < element) {
        return findIndex(arr, element, middle + 1, end);
      } else {
        return findIndex(arr, element, start, middle - 1);
      }
    } else {
      if(arr[middle] < element) {
        return findIndex(arr, element, middle + 1, end);
      } else {
        return findIndex(arr, element, start, middle - 1);
      }
    }
  }
  
}

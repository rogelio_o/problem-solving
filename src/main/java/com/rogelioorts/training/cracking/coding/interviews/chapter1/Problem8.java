package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import java.util.ArrayList;

public class Problem8 {

  public void zerify(int[][] matrix) {
    if (matrix.length == 0) {
      return;
    }

    ArrayList<Integer> rows = new ArrayList<>();
    ArrayList<Integer> columns = new ArrayList<>();

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[0].length; j++) {
        if (matrix[i][j] == 0) {
          rows.add(i);
          columns.add(j);
        }
      }
    }

    for (int i : rows) {
      zerifyRow(matrix, i);
    }

    for (int j : columns) {
      zerifyColumn(matrix, j);
    }
  }

  private void zerifyRow(int[][] matrix, int i) {
    for (int j = 0; j < matrix[i].length; j++) {
      matrix[i][j] = 0;
    }
  }

  private void zerifyColumn(int[][] matrix, int j) {
    for (int i = 0; i < matrix.length; i++) {
      matrix[i][j] = 0;
    }
  }

}

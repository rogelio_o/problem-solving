package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.LinkedList;

public class Problem6 {

  public void solveHanoi(LinkedList<Integer> tower1, LinkedList<Integer> tower2, LinkedList<Integer> tower3) {
    solveHanoi(tower1.size(), tower1, tower3, tower2);
  }
  
  private void solveHanoi(int n, LinkedList<Integer> src, LinkedList<Integer> dest, LinkedList<Integer> buffer) {
    if(n == 0) {
      return;
    }
    
    solveHanoi(n - 1, src, buffer, dest);
    moveToTop(src, dest);
    solveHanoi(n - 1, buffer, dest, src);
  }
  
  private void moveToTop(LinkedList<Integer> src, LinkedList<Integer> dest) {
    int disk = src.removeFirst();
    dest.addFirst(disk);
  }
  
}

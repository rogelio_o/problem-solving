package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem2 {

    public int findLast(ListNode<Integer> head, int k) {
        if (head == null) {
            return -1;
        }

        ListNode<Integer> faster = head;
        ListNode<Integer> slower = head;

        int fasterIndex = k;
        while (fasterIndex > 0 && faster != null) {
            faster = faster.getNext();
            fasterIndex--;
        }

        if (fasterIndex > 0) {
            return -1;
        }

        while (faster != null) {
            faster = faster.getNext();
            slower = slower.getNext();
        }

        return slower.getData();
    }

}

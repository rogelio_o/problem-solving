package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.HashMap;

public class Problem14 {

  public int countNumWaysToAddParentheses(String booleanExp, boolean expectedResult) {
    return countNumWaysToAddParentheses(booleanExp, expectedResult, new HashMap<>());
  }
  
  private int countNumWaysToAddParentheses(String booleanExp, boolean expectedResult, HashMap<String, Integer> cache) {
    if(booleanExp.length() == 0) {
      return 0;
    } else if(booleanExp.length() == 1) {
      return booleanResult(booleanExp) == expectedResult ? 1 : 0;
    } else if(cache.containsKey(expectedResult + booleanExp)) {
      return cache.get(expectedResult + booleanExp);
    }
    
    int ways = 0;
    for(int i = 1; i < booleanExp.length(); i += 2) {
      char c = booleanExp.charAt(i);
      String left = booleanExp.substring(0, i);
      String right = booleanExp.substring(i + 1, booleanExp.length());
      int leftTrue = countNumWaysToAddParentheses(left, true, cache);
      int leftFalse = countNumWaysToAddParentheses(left, false, cache);
      int rightTrue = countNumWaysToAddParentheses(right, true, cache);
      int rightFalse = countNumWaysToAddParentheses(right, false, cache);
      int total = (leftTrue + leftFalse) * (rightTrue + rightFalse);
      
      int totalTrue = 0;
      if(c == '^') {
        totalTrue = (leftTrue * rightFalse) + (leftFalse + rightTrue);
      } else if(c == '&') {
        totalTrue = leftTrue * rightTrue;
      } else {
        totalTrue = (leftTrue * rightTrue) + (leftTrue + rightFalse)
            + (leftFalse * rightTrue);
      }
      
      ways += expectedResult ? totalTrue : total - totalTrue;
    }
    
    cache.put(expectedResult + booleanExp, ways);
    
    return ways;
  }
  
  private boolean booleanResult(String str) {
    return str.equals("1") ? true : false;
  }
  
}

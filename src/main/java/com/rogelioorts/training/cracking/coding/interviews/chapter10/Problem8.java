package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import com.rogelioorts.training.cracking.coding.interviews.utils.BitMap;

// 32.000 posibilities < 2^15
// 4kB memory: 2^15 bits
// We have enough memory to map the results in memory
public class Problem8 {

    public int findDuplicated(int[] arr) {
        BitMap map = new BitMap(2 ^ 15);

        for (int num : arr) {
            if (map.get(num)) {
                return num;
            }

            map.set(num);
        }

        return -1;
    }

}

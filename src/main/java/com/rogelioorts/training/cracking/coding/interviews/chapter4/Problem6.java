package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem6 {

    public BinarySearchTreeNode<Integer> findSuccesor(BinarySearchTreeNode<Integer> node) {
        if (node == null) {
            return null;
        }

        if (node.getRight() != null) {
            return leftMostChild(node.getRight());
        } else {
            BinarySearchTreeNode<Integer> q = node;
            BinarySearchTreeNode<Integer> x = q.getParent();

            while (x != null && x.getLeft() != q) {
                q = x;
                x = x.getParent();
            }

            return x;
        }
    }

    private BinarySearchTreeNode<Integer> leftMostChild(BinarySearchTreeNode<Integer> node) {
        if (node == null) {
            return null;
        } else {
            BinarySearchTreeNode<Integer> result = node;
            while (node.getLeft() != null) {
                result = node.getLeft();
            }
            return result;
        }
    }

}

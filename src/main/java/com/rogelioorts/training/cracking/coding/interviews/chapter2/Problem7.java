package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem7 {

    public ListNode<Integer> findIntersection(ListNode<Integer> head1, ListNode<Integer> head2) {
        SizeResult size1 = getSize(head1);
        SizeResult size2 = getSize(head2);

        if (size1.last != size2.last) {
            return null;
        }

        ListNode<Integer> p1 = head1;
        ListNode<Integer> p2 = head2;
        if (size1.size > size2.size) {
            p1 = offset(head1, size1.size - size2.size);
        } else if (size2.size > size1.size) {
            p2 = offset(head2, size2.size - size1.size);
        }

        while (p1 != null && p2 != null) {
            if (p1 == p2) {
                return p1;
            }

            p1 = p1.getNext();
            p2 = p2.getNext();
        }

        return null;
    }

    private SizeResult getSize(ListNode<Integer> head) {
        int result = 1;
        ListNode<Integer> p = head;
        while (p.getNext() != null) {
            result++;
            p = p.getNext();
        }

        return new SizeResult(result, p);
    }

    private ListNode<Integer> offset(ListNode<Integer> head, int k) {
        ListNode<Integer> p = head;
        for (int i = 0; i < k; i++) {
            p = p.getNext();
        }

        return p;
    }

    private class SizeResult {

        private int size;

        private ListNode<Integer> last;

        public SizeResult(int size, ListNode<Integer> last) {
            this.size = size;
            this.last = last;
        }

    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter5;

public class Problem3 {

  public int getLengthOfMathOnesSequence(int n) {
    int max = 0;
    int prevMax = 0;
    int currentMax = 0;

    for (int i = 0; i < 32; i++) {
      if ((n & (1 << i)) == 0) {
        max = Math.max(prevMax, max);
        prevMax = currentMax + 1;
        currentMax = 0;
      } else {
        prevMax++;
        currentMax++;
      }
    }

    max = Math.max(prevMax, max);

    return max;
  }

}

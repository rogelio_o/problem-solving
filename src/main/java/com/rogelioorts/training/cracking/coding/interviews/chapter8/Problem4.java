package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.ArrayList;
import java.util.LinkedList;

public class Problem4 {

  public ArrayList<LinkedList<Integer>> getSubsets(LinkedList<Integer> set) {
    ArrayList<LinkedList<Integer>> result = new ArrayList<>();

    for (Integer num : set) {
      ArrayList<LinkedList<Integer>> subresult = new ArrayList<>();
      for (LinkedList<Integer> r : result) {
        LinkedList<Integer> clone = new LinkedList<>(r);
        clone.add(num);
        subresult.add(clone);
      }
      result.addAll(subresult);

      LinkedList<Integer> own = new LinkedList<>();
      own.add(num);
      result.add(own);
    }

    return result;
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

public class Problem1 {

  public int countWaysToRunningUpAStaircase(int steps) {
    if (steps == 0) {
      return 0;
    } else if (steps == 1) {
      return 1;
    } else if (steps == 2) {
      return 2;
    } else if (steps == 3) {
      return 3;
    }

    int c = 1;
    int b = 2;
    int a = 3;

    for (int i = 0; i < steps - 3; i++) {
      int d = a + b + c;
      c = b;
      b = a;
      a = d;
    }

    return a;
  }

}

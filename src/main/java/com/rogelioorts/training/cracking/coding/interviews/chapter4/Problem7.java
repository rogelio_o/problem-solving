package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import java.util.HashSet;
import java.util.LinkedList;

import com.rogelioorts.training.basics.data.structures.Graph;

public class Problem7 {

    public LinkedList<Character> getProjectsOrder(Graph<Character> g) {
        LinkedList<Character> result = new LinkedList<>();
        HashSet<Character> visited = new HashSet<>();

        for (Character c : g.getVertices()) {
            getProjectsOrder(g, c, visited, result);
        }

        return result;
    }

    private void getProjectsOrder(Graph<Character> g, Character c, HashSet<Character> visited,
            LinkedList<Character> result) {
        if (visited.add(c)) {
            for (Character s : g.getAdjacents(c)) {
                getProjectsOrder(g, s, visited, result);
            }

            result.addFirst(c);
        }
    }

}

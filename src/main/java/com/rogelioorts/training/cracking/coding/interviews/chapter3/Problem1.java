package com.rogelioorts.training.cracking.coding.interviews.chapter3;

public class Problem1 {

  private int[] array;
  
  private int size;
  
  private int indexes[];
  
  public Problem1(int size) {
    this.size = size;
    this.indexes = new int[] {0, size, size * 2};
    this.array = new int[size * 3];
  }
  
  public void push(int stackNum, int element) {
    checkStackNum(stackNum);
    checkStackFull(stackNum);
    
    array[getIndexOfStack(stackNum)] = element;
    moveForwardIndex(stackNum);
  }
  
  public int pop(int stackNum) {
    checkStackNum(stackNum);
    checkStackEmpty(stackNum);
    
    int index = getIndexOfStack(stackNum) - 1;
    int result = array[index];
    array[index] = 0;
    moveBackIndex(stackNum);
    
    return result;
  }
  
  public int peek(int stackNum) {
    checkStackNum(stackNum);
    checkStackEmpty(stackNum);
    
    return array[getIndexOfStack(stackNum) - 1]; 
  }
  
  private void checkStackNum(int stackNum) {
    if(stackNum < 1 || stackNum > 3) {
      throw new IllegalArgumentException("The stack number should be between 1 and 3.");
    }
  }
  
  private void checkStackFull(int stackNum) {
    if(getIndexOfStack(stackNum) == (size * stackNum)) {
      throw new IllegalStateException("The stack is full.");
    }
  }
  
  private void checkStackEmpty(int stackNum) {
    int index = getIndexOfStack(stackNum);
    
    if(index == (size * (stackNum - 1))) {
      throw new IllegalStateException("The stack is empty.");
    }
  }
  
  private int getIndexOfStack(int stackNum) {
    return indexes[stackNum - 1];
  }
  
  private void moveBackIndex(int stackNum) {
    indexes[stackNum - 1]--;
  }
  
  private void moveForwardIndex(int stackNum) {
    indexes[stackNum - 1]++;
  }
  
}

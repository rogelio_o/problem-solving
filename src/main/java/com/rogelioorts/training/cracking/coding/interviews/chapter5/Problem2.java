package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import java.math.BigDecimal;

public class Problem2 {

  public String printBinary(double num) {
    if (num >= 1 || num <= 0) {
      return "ERROR";
    }

    StringBuilder builder = new StringBuilder();
    builder.append(".");
    BigDecimal currentNum = BigDecimal.valueOf(num);
    while (currentNum.compareTo(BigDecimal.ZERO) > 0) {
      if (builder.length() >= 32) {
        return "ERROR";
      }

      BigDecimal r = currentNum.multiply(BigDecimal.valueOf(2));
      if (r.compareTo(BigDecimal.ONE) >= 0) {
        builder.append("1");
        currentNum = r.subtract(BigDecimal.ONE);
      } else {
        builder.append("0");
        currentNum = r;
      }
    }

    return builder.toString();
  }

}

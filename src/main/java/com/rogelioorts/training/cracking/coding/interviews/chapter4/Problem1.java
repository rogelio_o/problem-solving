package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import java.util.HashSet;
import java.util.LinkedList;

import com.rogelioorts.training.basics.data.structures.Graph;

public class Problem1 {

    public boolean existsPath(Graph<Character> g, Character origin, Character destination) {
        HashSet<Character> visited = new HashSet<>();

        LinkedList<Character> stack = new LinkedList<>();
        stack.addAll(g.getAdjacents(origin));

        while (!stack.isEmpty()) {
            Character c = stack.removeLast();
            if (visited.add(c)) {
                if (c == destination) {
                    return true;
                }

                for (Character s : g.getAdjacents(c)) {
                    stack.addLast(s);
                }
            }
        }

        return false;
    }

}

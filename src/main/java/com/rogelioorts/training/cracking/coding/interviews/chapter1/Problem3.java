package com.rogelioorts.training.cracking.coding.interviews.chapter1;

public class Problem3 {

  public void urlify(char[] str, int n) {
    for (int i = str.length - 1, j = n - 1; i >= 0 & j >= 0; i--, j--) {
      if (str[j] == ' ') {
        str[i] = '0';
        str[--i] = '2';
        str[--i] = '%';
      } else {
        str[i] = str[j];
      }
    }
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

public class Problem5 {

	public int search(String[] arr, String element) {
		return search(arr, element, 0, arr.length - 1);
	}

	private int search(String[] arr, String element, int start, int end) {
		int movedStart = moveStart(arr, start);
		int movedEnd = moveEnd(arr, end);

		if (movedStart > movedEnd) {
			return -1;
		}

		int middle = (start + end) / 2;
		int movedMiddle = moveStart(arr, middle);
		String middleValue = arr[movedMiddle];

		if (middleValue.equals(element)) {
			return movedMiddle;
		} else if (middleValue.compareTo(element) < 0) {
			return search(arr, element, movedMiddle + 1, movedEnd);
		} else {
			return search(arr, element, start, middle - 1);
		}
	}

	private int moveStart(String[] arr, int start) {
		int result = start;

		while (result < arr.length && arr[result].isEmpty()) {
			result++;
		}

		return result;
	}

	private int moveEnd(String[] arr, int end) {
		int result = end;

		while (result >= 0 && arr[result].isEmpty()) {
			result--;
		}

		return result;
	}

}

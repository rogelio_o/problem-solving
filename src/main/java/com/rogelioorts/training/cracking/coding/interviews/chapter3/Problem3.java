package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import java.util.ArrayList;
import java.util.LinkedList;

public class Problem3 {

  private int maxSize;
  
  private int stackIndex;
  
  private ArrayList<LinkedList<Integer>> lists;
  
  public Problem3(int maxSize) {
    this.maxSize = maxSize;
    this.stackIndex = -1;
    this.lists = new ArrayList<>();
  }
  
  public void push(int element) {
    if(stackIndex == -1 || isFullStack()) {
      lists.add(new LinkedList<>());
      stackIndex++;
    }
    
    lists.get(stackIndex).push(element);
  }
  
  public int pop() {
    if(stackIndex == -1) {
      throw new IllegalStateException("Empty stack.");
    }
    
    int result = lists.get(stackIndex).pop();
    
    removeIfNedded();
    
    return result;
  }
  
  public int popAt(int stackNum) {
    if(stackNum < 0 || stackNum > stackIndex) {
      throw new IllegalArgumentException("Stack number not valid.");
    }
    
    int result = lists.get(stackNum).pop();
    
    if(stackNum != stackIndex) {
      lists.get(stackNum).push(popAt(stackNum + 1));
    } else {
      removeIfNedded();
    }
    
    return result;
  }
  
  public int size() {
    return lists.stream().mapToInt(LinkedList::size).sum();
  }
  
  public int stacksSize() {
    return lists.size();
  }
  
  private void removeIfNedded() {
    if(isEmptyStack()) {
      lists.remove(stackIndex);
      stackIndex--;
    }
  }
  
  private boolean isFullStack() {
    return lists.get(stackIndex).size() == maxSize;
  }
  
  private boolean isEmptyStack() {
    return lists.get(stackIndex).isEmpty();
  }
  
}

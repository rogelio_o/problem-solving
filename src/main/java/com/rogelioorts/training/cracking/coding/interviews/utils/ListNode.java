package com.rogelioorts.training.cracking.coding.interviews.utils;

public class ListNode<T> {

  private ListNode<T> next;

  private T data;

  public ListNode(T data) {
    this.data = data;
  }

  public ListNode<T> add(T data) {
    add(new ListNode<>(data));

    return this;
  }

  private void add(ListNode<T> newNode) {
    if (next == null) {
      next = newNode;
    } else {
      next.add(newNode);
    }
  }

  public T getData() {
    return data;
  }

  public ListNode<T> getNext() {
    return next;
  }

  public void setNext(ListNode<T> next) {
    this.next = next;
  }

  public void setData(T data) {
    this.data = data;
  }

  @Override
  public String toString() {
    try {
      return data + (next != null ? " -> " + next.toString() : "");
    } catch(StackOverflowError e) {
      return "";
    }
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import java.util.HashSet;

public class Problem1 {

  public boolean isUnique(String str) {
    HashSet<Character> set = new HashSet<>();

    for (int i = 0; i < str.length(); i++) {
      if (!set.add(str.charAt(i))) {
        return false;
      }
    }

    return true;
  }

  public boolean isUniqueWithoutExtraDataStructure(String str) {
    for (int i = 0; i < str.length(); i++) {
      for (int j = i + 1; j < str.length(); j++) {
        if (str.charAt(i) == str.charAt(j)) {
          return false;
        }
      }
    }

    return true;
  }

}

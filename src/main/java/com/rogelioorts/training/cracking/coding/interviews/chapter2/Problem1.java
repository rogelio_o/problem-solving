package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import java.util.HashSet;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem1 {

    public void removeRepeated(ListNode<Integer> head) {
        if (head == null) {
            return;
        }

        HashSet<Integer> set = new HashSet<>();

        ListNode<Integer> p = head;
        set.add(p.getData());
        while (p.getNext() != null) {
            if (!set.add(p.getNext().getData())) {
                p.setNext(p.getNext().getNext());
            } else {
                p = p.getNext();
            }
        }
    }

    public void removeRepeatedWithoutDataStructure(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            ListNode<Integer> p2 = p;

            while (p2.getNext() != null) {
                if (p.getData().equals(p2.getNext().getData())) {
                    p2.setNext(p2.getNext().getNext());
                } else {
                    p2 = p2.getNext();
                }
            }

            p = p.getNext();
        }
    }

}

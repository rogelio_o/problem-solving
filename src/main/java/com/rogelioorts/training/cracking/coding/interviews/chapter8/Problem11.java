package com.rogelioorts.training.cracking.coding.interviews.chapter8;

public class Problem11 {

  public int countWaysToObtain(int n) {
    int[] coins = new int[] {25, 10, 5, 1};
    int[][] cache = new int[n + 1][coins.length];
    
    return countWaysToObtain(n, coins, 0, cache);
  }
  
  private int countWaysToObtain(int n, int[] coins, int index, int[][] cache) {
    if(cache[n][index] > 0) {
      return cache[n][index];
    } else if(index >= coins.length - 1) { // 1 coin remaining
      return 1;
    }
    
    int ways = 0;
    int coin = coins[index];
    for(int i = 0; i * coin <= n; i++) {
      int remaining = n - (i * coin);
      
      ways += countWaysToObtain(remaining, coins, index + 1, cache);
    }
    
    cache[n][index] = ways;
    
    return ways;
  }
  
}

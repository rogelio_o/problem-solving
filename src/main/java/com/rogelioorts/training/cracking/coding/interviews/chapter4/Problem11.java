package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import java.util.Random;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem11 {

    public Integer getRandomNode(BinarySearchTreeNode<Integer> node) {
        if (node == null) {
            return null;
        } else if (node.getLeft() == null && node.getRight() == null) {
            return node.getData();
        } else if (node.getLeft() == null) {
            return getRandomNode(node.getRight());
        } else if (node.getRight() == null) {
            return getRandomNode(node.getLeft());
        }

        Random random = new Random();
        int num = random.nextInt(node.getSize());

        if (num == node.getLeft().getSize()) {
            return node.getData();
        } else if (num < node.getLeft().getSize()) {
            return getRandomNode(node.getLeft());
        } else {
            return getRandomNode(node.getRight());
        }
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem2 {

    public BinarySearchTreeNode<Integer> createMinBSTree(int[] arr) {
        return createMinBSTree(arr, 0, arr.length);
    }

    private BinarySearchTreeNode<Integer> createMinBSTree(int[] arr, int start, int end) {
        if (start == end - 1) {
            return new BinarySearchTreeNode<>(arr[start]);
        } else if (start > end - 1) {
            return null;
        } else {
            int middle = (start + end) / 2;

            BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(arr[middle]);
            root.setLeft(createMinBSTree(arr, start, middle));
            root.setRight(createMinBSTree(arr, middle + 1, end));

            return root;
        }
    }

}

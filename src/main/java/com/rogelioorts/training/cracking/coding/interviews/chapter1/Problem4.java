package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import java.util.HashMap;

public class Problem4 {

  public boolean hasPalindrome(String str) {
    String formattedStr = str.replaceAll("\\s", "").toLowerCase();
    HashMap<Character, Integer> map = new HashMap<>();

    for (char c : formattedStr.toCharArray()) {
      map.put(c, map.getOrDefault(c, 0) + 1);
    }

    int maxOdds = formattedStr.length() % 2 == 0 ? 0 : 1;
    for (Integer num : map.values()) {
      if (num % 2 != 0) {
        if (maxOdds == 0) {
          return false;
        } else {
          maxOdds--;
        }
      }
    }

    return true;
  }

}

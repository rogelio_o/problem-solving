package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import java.util.HashMap;

public class Problem2 {

  public boolean isPermutation(String s1, String s2) {
    if (s1.length() != s2.length()) {
      return false;
    }

    HashMap<Character, Integer> ocurrences = new HashMap<>();

    for (char c : s1.toCharArray()) {
      if (ocurrences.containsKey(c)) {
        ocurrences.put(c, ocurrences.get(c) + 1);
      } else {
        ocurrences.put(c, 1);
      }
    }

    for (char c : s2.toCharArray()) {
      if (ocurrences.containsKey(c)) {
        ocurrences.put(c, ocurrences.get(c) - 1);
      } else {
        return false;
      }
    }

    for (Integer n : ocurrences.values()) {
      if (n != 0) {
        return false;
      }
    }

    return true;
  }

}

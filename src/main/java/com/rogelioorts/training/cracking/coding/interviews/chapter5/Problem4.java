package com.rogelioorts.training.cracking.coding.interviews.chapter5;

public class Problem4 {

  public int getNextLargest(int n) {
    int currentNum = n;

    int numZeros = 0;
    while (!bitAt(currentNum, 0) && currentNum != 0) {
      numZeros++;
      currentNum >>= 1;
    }

    int numOnes = 0;
    while (bitAt(currentNum, 0) && currentNum != 0) {
      numOnes++;
      currentNum >>= 1;
    }

    if (numZeros + numOnes == 31 || numZeros + numOnes == 0) {
      return -1;
    }

    int p = numOnes + numZeros;
    int result = n;
    result |= (1 << p);
    result &= ~((1 << p) - 1);
    result |= (1 << (numOnes - 1)) - 1;

    return result;
  }

  public int getNextSmallest(int n) {
    int currentNum = n;

    int numOnes = 0;
    while (bitAt(currentNum, 0) && currentNum != 0) {
      numOnes++;
      currentNum >>= 1;
    }

    int numZeros = 0;
    while (!bitAt(currentNum, 0) && currentNum != 0) {
      numZeros++;
      currentNum >>= 1;
    }

    if (numZeros + numOnes == 31 || numZeros + numOnes == 0) {
      return -1;
    }

    int p = numOnes + numZeros;
    int result = n;
    result &= ~(1 << p);
    result |= ((1 << p) - 1);
    result &= ((~0) << (numZeros - 1));

    return result;
  }

  private boolean bitAt(int n, int i) {
    return (n & (1 << i)) != 0;
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import java.util.Stack;

public class Problem5 {

  private Stack<Integer> stack;
  
  private Stack<Integer> auxStack;
  
  public Problem5() {
    stack = new Stack<>();
    auxStack = new Stack<>();
  }
  
  public int pop() {
    return stack.pop();
  }
  
  public void push(int element) {
    while(!stack.isEmpty() && stack.peek() < element) {
      auxStack.push(stack.pop());
    }
    stack.push(element);
    while(!auxStack.isEmpty()) {
      stack.push(auxStack.pop());
    }
  }
  
}

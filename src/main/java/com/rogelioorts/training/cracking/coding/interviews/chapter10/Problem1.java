package com.rogelioorts.training.cracking.coding.interviews.chapter10;

public class Problem1 {

  public void sortedMerge(int[] a, int[] b) {
    int aSize = a.length - b.length;
    
    for(int indexA = aSize - 1, indexB = b.length - 1, index = a.length - 1; index >= 0; index--) {
      if(indexA >= 0 && (indexB < 0 || a[indexA] > b[indexB])) {
        a[index] = a[indexA];
        indexA--;
      } else {
        a[index] = b[indexB];
        indexB--;
      }
    }
  }
  
}

package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem10 {

    public boolean isSubtree(BinarySearchTreeNode<Integer> root1, BinarySearchTreeNode<Integer> root2) {
        return (root1 == null && root2 == null) || (root1 != null && root2 != null
                && ((root1.getData() == root2.getData() && isSubtree(root1.getLeft(), root2.getLeft())
                        && isSubtree(root1.getRight(), root2.getRight())) || isSubtree(root1.getLeft(), root2)
                        || isSubtree(root1.getRight(), root2)));
    }

}

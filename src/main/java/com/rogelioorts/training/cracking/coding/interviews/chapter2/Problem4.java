package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem4 {

    public ListNode<Integer> partition(ListNode<Integer> head, int x) {
        ListNode<Integer> lowerHead = null;
        ListNode<Integer> lowerTail = null;
        ListNode<Integer> higherHead = null;

        ListNode<Integer> p = head;
        while (p != null) {
            ListNode<Integer> nextAux = p.getNext();

            if (p.getData() < x) {
                p.setNext(lowerHead);
                lowerHead = p;

                if (lowerTail == null) {
                    lowerTail = p;
                }
            } else {
                p.setNext(higherHead);
                higherHead = p;
            }

            p = nextAux;
        }

        if (lowerTail != null) {
            lowerTail.setNext(higherHead);
        }

        return lowerHead;
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter5;

public class Problem8 {

  public void drawLine(byte[] screen, int width, int x1, int x2, int y) {
    int indexStartOfRow = (y * (width / 8));
    int startColumnOffset = x1 / 8;
    int startIndex = indexStartOfRow + startColumnOffset;
    int endColumnOffset = x2 / 8;
    int endIndex = indexStartOfRow + endColumnOffset;

    int maskStart = (1 << (8 - (x1 % 8))) - 1;
    int maskEnd = ((~0) << (7 - (x2 % 8)));

    if (startColumnOffset != endColumnOffset) {
      screen[startIndex] |= maskStart;

      // Fill middle positions
      for (int i = startIndex + 1; i < endIndex; i++) {
        screen[i] = (byte) 0xff;
      }

      screen[endIndex] |= maskEnd;
    } else {
      screen[startIndex] |= (maskStart & maskEnd);
    }
  }

}

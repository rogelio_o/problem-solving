package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem3 {

    public boolean deleteMiddleNode(ListNode<Character> toDelete) {
        if (toDelete == null || toDelete.getNext() == null) {
            return false;
        }

        ListNode<Character> next = toDelete.getNext();
        toDelete.setData(next.getData());
        toDelete.setNext(next.getNext());

        return true;
    }

}

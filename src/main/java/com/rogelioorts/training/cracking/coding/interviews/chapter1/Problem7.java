package com.rogelioorts.training.cracking.coding.interviews.chapter1;

public class Problem7 {

  public void rotate(int[][] screen) {
    int n = screen.length;

    for (int layer = 0; layer < n / 2; layer++) {
      int start = layer;
      int end = n - layer;

      for (int i = start; i < end - 1; i++) {
        int offset = i - start;

        int aux = screen[start][i];
        screen[start][i] = screen[i][end - 1];
        screen[i][end - 1] = screen[end - 1][end - 1 - offset];
        screen[end - 1][end - 1 - offset] = screen[end - 1 - offset][start];
        screen[end - 1 - offset][start] = aux;
      }
    }
  }

}

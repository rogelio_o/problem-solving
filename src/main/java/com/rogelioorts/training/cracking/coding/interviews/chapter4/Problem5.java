package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem5 {

    public boolean isBinarySearchTree(BinarySearchTreeNode<Integer> root) {
        return isBinarySearchTree(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private boolean isBinarySearchTree(BinarySearchTreeNode<Integer> root, int min, int max) {
        if (root == null) {
            return true;
        }

        int value = root.getData();

        if (value < min || value > max) {
            return false;
        }

        return isBinarySearchTree(root.getLeft(), min, value) && isBinarySearchTree(root.getRight(), value, max);
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter1;

public class Problem9 {

  public boolean isRotation(String str1, String str2) {
    if (str1.isEmpty() || str1.length() != str2.length()) {
      return false;
    }

    return (str1 + str1).contains(str2);
  }

}

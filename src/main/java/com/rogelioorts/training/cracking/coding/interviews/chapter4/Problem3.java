package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import java.util.ArrayList;
import java.util.LinkedList;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem3 {

    public ArrayList<LinkedList<Integer>> getDataOfEachLevel(BinarySearchTreeNode<Integer> root) {
        LinkedList<BinarySearchTreeNode<Integer>> queue = new LinkedList<>();
        queue.addFirst(root);
        int n = 1;
        ArrayList<LinkedList<Integer>> result = new ArrayList<>();

        while (!queue.isEmpty()) {
            LinkedList<Integer> level = new LinkedList<>();
            result.add(level);
            for (int i = 0; i < n; i++) {
                BinarySearchTreeNode<Integer> node = queue.removeLast();
                level.add(node.getData());

                if (node.getLeft() != null) {
                    queue.addFirst(node.getLeft());
                }
                if (node.getRight() != null) {
                    queue.addFirst(node.getRight());
                }
            }

            n = queue.size();
        }

        return result;
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.Queue;

public class Problem6 {
  
  private Queue<Dog> dogs;
  
  private Queue<Cat> cats;
  
  public Problem6() {
    dogs = new ArrayDeque<>();
    cats = new ArrayDeque<>();
  }

  public void enqueue(Dog dog) {
    dog.setAddTime(LocalDateTime.now());
    dogs.add(dog);
  }
  
  public void enqueue(Cat cat) {
    cat.setAddTime(LocalDateTime.now());
    cats.add(cat);
  }

  public Animal dequeueAny() {
    if(cats.isEmpty() && dogs.isEmpty()) {
      throw new IllegalStateException("Stack empty.");
    } else if(dogs.isEmpty()) {
      return cats.remove();
    } else if(cats.isEmpty()) {
      return dogs.remove();
    }
    
    return cats.peek().getAddTime().isBefore(dogs.peek().getAddTime()) ? cats.remove() : dogs.remove();
  }
  
  public Dog dequeueDog() {
    return dogs.remove();
  }
  
  public Cat dequeueCat() {
    return cats.remove();
  }
  
  public static abstract class Animal {
    
    private LocalDateTime addTime;
    
    private String name;
    
    public Animal(String name) {
      this.name = name;
    }
    
    public String getName() {
      return name;
    }
    
    public LocalDateTime getAddTime() {
      return addTime;
    }
    
    public void setAddTime(LocalDateTime addTime) {
      this.addTime = addTime;
    }
    
  }
  
  public static class Dog extends Animal {
    
    public Dog(String name) {
      super(name);
    }
    
  }
  
  public static class Cat extends Animal {
    
    public Cat(String name) {
      super(name);
    }
    
  }
  
}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

public class Problem5 {

  public int multiply(int a, int b) {
    return multiplyRec(a > b ? a : b, a > b ? b : a);
  }
  
  private int multiplyRec(int bigger, int smaller) {
    if(smaller == 0) {
      return 0;
    } else if(smaller == 1) {
      return bigger;
    }
    
    int middle = smaller >> 1;
    int rest = smaller % 2;
    
    int result = multiplyRec(bigger, middle);
    if(rest == 0) {
      return result + result;
    } else {
      return result + result + bigger;
    }
  }
  
}

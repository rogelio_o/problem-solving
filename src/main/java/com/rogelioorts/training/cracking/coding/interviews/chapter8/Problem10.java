package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import com.rogelioorts.training.cracking.coding.interviews.utils.Coordinate;

public class Problem10 {

    public void fill(int[][] picture, Coordinate point, int newColor) {
        fill(picture, point, picture[point.getI()][point.getJ()], newColor);
    }

    private void fill(int[][] picture, Coordinate point, int oldColor, int newColor) {
        if (!isValidPoint(picture, point) || picture[point.getI()][point.getJ()] != oldColor) {
            return;
        }

        picture[point.getI()][point.getJ()] = newColor;

        fill(picture, Coordinate.create(point.getI() + 1, point.getJ()), oldColor, newColor);
        fill(picture, Coordinate.create(point.getI() - 1, point.getJ()), oldColor, newColor);
        fill(picture, Coordinate.create(point.getI(), point.getJ() + 1), oldColor, newColor);
        fill(picture, Coordinate.create(point.getI(), point.getJ() - 1), oldColor, newColor);
    }

    private boolean isValidPoint(int[][] picture, Coordinate point) {
        return point.getI() >= 0 && point.getI() < picture.length && point.getJ() >= 0
                && point.getJ() < picture[0].length;
    }

}

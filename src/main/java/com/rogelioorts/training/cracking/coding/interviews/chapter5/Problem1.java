package com.rogelioorts.training.cracking.coding.interviews.chapter5;

public class Problem1 {

  public int insertNumber(int n, int m, int i, int j) {
    int nMask = ~(((~0) << i) & ~((~0) << j));
    int cleanedN = n & nMask;

    return cleanedN | (m << i);
  }

}

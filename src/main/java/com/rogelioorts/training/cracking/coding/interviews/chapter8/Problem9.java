package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.HashSet;

public class Problem9 {

  public HashSet<String> getParenthesesCombinations(int n) {
    HashSet<String> results = new HashSet<>();
    
    if(n == 1) {
      results.add("()");
    } else {
      HashSet<String> prevResults = getParenthesesCombinations(n - 1);
      
      for(String prevResult : prevResults) {
        results.add("(" + prevResult + ")");
        results.add("()" + prevResult);
        results.add(prevResult + "()");
      }
    }
    
    return results;
  }
  
}

package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import java.util.LinkedList;

public class Problem2 {
  
  private LinkedList<Node> nodes;
  
  public Problem2() {
    nodes = new LinkedList<>();
  }

  public int pop() {
    return nodes.pop().value;
  }
  
  public void push(int element) {
    nodes.push(new Node(element, nodes.isEmpty() ? element : Math.min(element, min())));
  }
  
  public int min() {
    return nodes.peek().min;
  }
  
  private class Node {
    
    private int value;
    
    private int min;
    
    public Node(int value, int min) {
      this.value = value;
      this.min = min;
    }
    
  }
  
}

package com.rogelioorts.training.cracking.coding.interviews.chapter5;

public class Problem7 {

  public int swapOddsAndEvens(int n) {
    int evensMask = 0xaaaaaaaa;
    int oddsMask = 0x55555555;

    return ((n & evensMask) >> 1) | ((n & oddsMask) << 1);
  }

}

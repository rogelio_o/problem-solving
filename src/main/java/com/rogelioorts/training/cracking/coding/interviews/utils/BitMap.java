package com.rogelioorts.training.cracking.coding.interviews.utils;

public class BitMap {

    private byte[] map;

    public BitMap(int size) {
        map = new byte[size / 8];
    }

    public void set(int i) {
        map[i / 8] |= (1 << (i % 8));
    }

    public boolean get(int i) {
        return (map[i / 8] & (1 << (i % 8))) != 0;
    }

}

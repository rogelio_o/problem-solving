package com.rogelioorts.training.cracking.coding.interviews.chapter10;

public class Problem11 {

	public void convertPeaksAndValleys(int[] arr) {
		for (int i = 1; i < arr.length - 1; i += 2) {
			int biggestValue = Math.max(arr[i - 1], Math.max(arr[i], arr[i + 1]));
			int aux = arr[i];

			if (arr[i - 1] == biggestValue) {
				arr[i] = arr[i - 1];
				arr[i - 1] = aux;
			} else if (arr[i + 1] == biggestValue) {
				arr[i] = arr[i + 1];
				arr[i + 1] = aux;
			}
		}
	}

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import com.rogelioorts.training.cracking.coding.interviews.utils.Coordinate;

public class Problem9 {

    public Coordinate findElement(int[][] matrix, int element) {
        for (int i = 0; i < matrix.length; i++) {
            int result = findElement(matrix[i], element, 0, matrix[i].length);
            if (result != -1) {
                return Coordinate.create(i, result);
            }
        }

        return null;
    }

    private int findElement(int[] arr, int element, int start, int end) {
        if (start >= end) {
            return -1;
        }

        int m = (start + end) / 2;
        int mValue = arr[m];

        if (mValue == element) {
            return m;
        } else if (mValue < element) {
            return findElement(arr, element, m + 1, end);
        } else {
            return findElement(arr, element, start, m - 1);
        }
    }

}

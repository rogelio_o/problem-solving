package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.LinkedList;

import com.rogelioorts.training.cracking.coding.interviews.utils.Coordinate;

public class Problem2 {

    public LinkedList<Coordinate> goToEnd(int[][] grid) {
        LinkedList<Coordinate> result = new LinkedList<>();
        if (goToEnd(grid, Coordinate.create(0, 0), result)) {
            return result;
        } else {
            return null;
        }
    }

    private boolean goToEnd(int[][] grid, Coordinate current, LinkedList<Coordinate> result) {
        if (current.getI() == grid.length - 1 && current.getJ() == grid[0].length - 1) {
            result.addFirst(current);
            return true;
        } else if (!isValidPosition(grid, current)) {
            return false;
        }

        boolean right = goToEnd(grid, Coordinate.create(current.getI(), current.getJ() + 1), result);
        if (right) {
            result.addFirst(current);
            return true;
        }

        boolean bottom = goToEnd(grid, Coordinate.create(current.getI() + 1, current.getJ()), result);
        if (bottom) {
            result.addFirst(current);
            return true;
        }

        return false;
    }

    private boolean isValidPosition(int[][] grid, Coordinate current) {
        return current.getI() < grid.length && current.getI() >= 0 && current.getJ() < grid[0].length
                && current.getJ() >= 0 && grid[current.getI()][current.getJ()] == 0;
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem5 {

    public ListNode<Integer> sum(ListNode<Integer> head1, ListNode<Integer> head2) {
        ListNode<Integer> result = null;
        ListNode<Integer> head = null;

        ListNode<Integer> p1 = head1;
        ListNode<Integer> p2 = head2;
        int carry = 0;

        while (p1 != null && p2 != null) {
            int n = p1.getData() + p2.getData() + carry;

            ListNode<Integer> node = new ListNode<>(n % 10);
            if (head != null) {
                head.setNext(node);
            }
            head = node;
            if (result == null) {
                result = node;
            }

            carry = n >= 10 ? 1 : 0;

            p1 = p1.getNext();
            p2 = p2.getNext();
        }

        if (p1 != null) {
            while (p1 != null) {
                int n = p1.getData() + carry;

                ListNode<Integer> node = new ListNode<>(n % 10);
                if (head != null) {
                    head.setNext(node);
                }
                head = node;
                if (result == null) {
                    result = node;
                }

                carry = n >= 10 ? 1 : 0;

                p1 = p1.getNext();
            }
        } else if (p2 != null) {
            while (p2 != null) {
                int n = p2.getData() + carry;

                ListNode<Integer> node = new ListNode<>(n % 10);
                if (head != null) {
                    head.setNext(node);
                }
                head = node;
                if (result == null) {
                    result = node;
                }

                carry = n >= 10 ? 1 : 0;

                p2 = p2.getNext();
            }
        }

        if (carry > 0) {
            ListNode<Integer> node = new ListNode<>(carry);
            if (head != null) {
                head.setNext(node);
            }
            head = node;
        }

        return result;
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem8 {

    public BinarySearchTreeNode<Integer> searchCommonAncestor(BinarySearchTreeNode<Integer> node1,
            BinarySearchTreeNode<Integer> node2) {
        if (node1 == null || node2 == null) {
            return null;
        } else if (contains(node1, node2)) {
            return node1;
        } else if (contains(node2, node1)) {
            return node2;
        }

        BinarySearchTreeNode<Integer> p = node1;
        while (p != null && p.getParent() != null) {
            BinarySearchTreeNode<Integer> oposite = getOpositeNode(p);
            if (contains(oposite, node2)) {
                return p.getParent();
            }

            p = p.getParent();
        }

        return null;
    }

    private BinarySearchTreeNode<Integer> getOpositeNode(BinarySearchTreeNode<Integer> p) {
        return p == p.getParent().getLeft() ? p.getParent().getRight() : p.getParent().getLeft();
    }

    private boolean contains(BinarySearchTreeNode<Integer> container, BinarySearchTreeNode<Integer> content) {
        if (container == null) {
            return false;
        }

        if (container == content) {
            return true;
        } else {
            return contains(container.getLeft(), content) || contains(container.getRight(), content);
        }
    }

}

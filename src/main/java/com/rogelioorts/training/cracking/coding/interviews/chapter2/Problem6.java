package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem6 {

    public boolean isPalindrome(ListNode<Character> str) {
        if (str == null) {
            return false;
        }

        int n = getSize(str);
        return isPalindrome(str, n, 0).result;
    }

    private Result isPalindrome(ListNode<Character> str, int n, int index) {
        if (n % 2 == 0 && (index + 1) == (n / 2)) {
            return new Result(str.getData() == str.getNext().getData(), str.getNext().getNext());
        } else if (n % 2 != 0 && (index + 1) == (n / 2)) {
            return new Result(str.getData() == str.getNext().getNext().getData(), str.getNext().getNext().getNext());
        } else {
            Result r = isPalindrome(str.getNext(), n, index + 1);

            return new Result(r.result ? str.getData() == r.next.getData() : false, r.next.getNext());
        }
    }

    private int getSize(ListNode<Character> str) {
        int result = 1;
        ListNode<Character> p = str;
        while (p.getNext() != null) {
            p = p.getNext();
            result++;
        }

        return result;
    }

    private class Result {

        private boolean result;

        private ListNode<Character> next;

        public Result(boolean result, ListNode<Character> next) {
            this.result = result;
            this.next = next;
        }

    }

}

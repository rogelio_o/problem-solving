package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem8 {

    public ListNode<Integer> getStartOfCycle(ListNode<Integer> head) {
        ListNode<Integer> slower = head;
        ListNode<Integer> faster = head;

        while (faster != null && faster.getNext() != null) {
            slower = slower.getNext();
            faster = faster.getNext().getNext();

            if (faster == slower) {
                break;
            }
        }

        if (faster == null || faster.getNext() == null) {
            return null;
        }

        slower = head;

        while (slower != null && faster != null) {
            if (slower == faster) {
                return slower;
            }

            slower = slower.getNext();
            faster = faster.getNext();
        }

        return null;
    }

}

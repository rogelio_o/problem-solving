package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem4 {

    public boolean isBalanced(BinarySearchTreeNode<Integer> root) {
        return isBalancedRec(root) != null;
    }

    private Integer isBalancedRec(BinarySearchTreeNode<Integer> root) {
        if (root == null) {
            return 0;
        }

        Integer leftDepth = isBalancedRec(root.getLeft());
        Integer rightDepth = isBalancedRec(root.getRight());

        if (!isValidDiff(leftDepth, rightDepth)) {
            return null;
        }

        return Math.max(leftDepth, rightDepth) + 1;
    }

    private boolean isValidDiff(Integer leftDepth, Integer rightDepth) {
        return leftDepth != null && rightDepth != null && Math.abs(leftDepth - rightDepth) <= 1;
    }

}

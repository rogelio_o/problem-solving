package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import java.util.HashMap;

public class Problem5 {

  public boolean isOneModificationAway(String str1, String str2) {
    int lenDiff = str1.length() - str2.length();
    if (Math.abs(lenDiff) > 1) {
      return false;
    }

    int charDiff = getNumDiffs(str1, str2);

    if (lenDiff == 0) { // UPDATE
      return charDiff == 2;
    } else { // REMOVE/INSERT
      return lenDiff == 1;
    }
  }

  private int getNumDiffs(String str1, String str2) {
    HashMap<Character, Integer> ocurrences = new HashMap<>();

    for (char c : str1.toCharArray()) {
      if (ocurrences.containsKey(c)) {
        ocurrences.put(c, ocurrences.get(c) + 1);
      } else {
        ocurrences.put(c, 1);
      }
    }

    for (char c : str2.toCharArray()) {
      if (ocurrences.containsKey(c)) {
        ocurrences.put(c, ocurrences.get(c) - 1);
      } else {
        ocurrences.put(c, 1);
      }
    }

    int result = 0;
    for (Integer num : ocurrences.values()) {
      result += Math.abs(num);
    }

    return result;
  }

}

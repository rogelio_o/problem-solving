package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem12 {

    public int getNumPathWithSum(BinarySearchTreeNode<Integer> root, int sum) {
        if (root == null) {
            return 0;
        }

        int pathsFromRoot = countPathsWithSumFromNode(root, sum, 0);
        int pathsOnLeft = getNumPathWithSum(root.getLeft(), sum);
        int pathsOnRight = getNumPathWithSum(root.getRight(), sum);

        return pathsFromRoot + pathsOnLeft + pathsOnRight;
    }

    private int countPathsWithSumFromNode(BinarySearchTreeNode<Integer> node, int sum, int acc) {
        if (node == null) {
            return 0;
        }

        int currentSum = acc + node.getData();

        int totalPaths = 0;
        if (currentSum == sum) {
            totalPaths++;
        }

        totalPaths += countPathsWithSumFromNode(node.getLeft(), sum, currentSum);
        totalPaths += countPathsWithSumFromNode(node.getRight(), sum, currentSum);

        return totalPaths;
    }

}

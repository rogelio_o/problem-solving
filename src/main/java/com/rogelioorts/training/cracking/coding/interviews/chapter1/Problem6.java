package com.rogelioorts.training.cracking.coding.interviews.chapter1;

public class Problem6 {

  public String compress(String str) {
    if (str.length() == 0) {
      return str;
    }

    StringBuilder builder = new StringBuilder();

    builder.append(str.charAt(0));
    int counter = 1;

    for (int i = 1; i < str.length(); i++) {
      if (str.charAt(i) == str.charAt(i - 1)) {
        counter++;
      } else {
        builder.append(counter);
        builder.append(str.charAt(i));
        counter = 1;
      }
    }

    builder.append(counter);

    return builder.length() > str.length() ? str : builder.toString();
  }

}

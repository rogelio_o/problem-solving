package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import com.rogelioorts.training.cracking.coding.interviews.utils.BitMap;

public class Problem7 {

    private byte[] map = new byte[Integer.MAX_VALUE / 8];

    // 4 billion integers 2^32
    // 1GB 2^33 bits
    // We have enough memory to map the integers
    public int findNotPresent(int[] arr) {
        for (int num : arr) {
            setMap(num);
        }

        for (int i = 1; i <= Integer.MAX_VALUE; i++) {
            if (!checkMap(i)) {
                return i;
            }
        }

        return -1;
    }

    // 4 billion integers 2^32
    // 10MB 2^23 bits
    // We have enough memory to map the integers
    public int findNotPresentFollowUp(int[] arr) {
        int maxSize = (int) Math.pow(2, 23);
        int offset = 0;
        while (offset < Integer.MAX_VALUE) {
            BitMap map = new BitMap(maxSize);

            for (int num : arr) {
                map.set(num - offset);
            }

            for (int i = 0; i < maxSize; i++) {
                if (!map.get(i)) {
                    return i + 1 + offset;
                }
            }

            offset += maxSize;
        }

        return -1;
    }

    private boolean checkMap(int num) {
        return (map[num / 8] & (1 << (num % 8))) != 0;
    }

    private void setMap(int num) {
        map[num / 8] |= 1 << (num % 8);
    }

}

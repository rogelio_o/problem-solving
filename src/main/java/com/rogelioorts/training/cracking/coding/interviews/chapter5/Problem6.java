package com.rogelioorts.training.cracking.coding.interviews.chapter5;

public class Problem6 {

  public int countNumFlips(int a, int b) {
    int result = 0;
    for (int i = 0; i < 32; i++) {
      if (bitAt(a, i) != bitAt(b, i)) {
        result++;
      }
    }

    return result;
  }

  private boolean bitAt(int n, int i) {
    return (n & (1 << i)) != 0;
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import java.util.Stack;

public class Problem4 {

  private Stack<Integer> stack1;
  
  private Stack<Integer> stack2;
  
  public Problem4() {
    stack1 = new Stack<>();
    stack2 = new Stack<>();
  }
  
  public void add(int element) {
    while(!stack1.isEmpty()) {
      stack2.push(stack1.pop());
    }
    stack1.push(element);
    while(!stack2.isEmpty()) {
      stack1.push(stack2.pop());
    }
  }
  
  public int remove() {
    return stack1.pop();
  }
  
  public int peek() {
    return stack1.peek();
  }
  
}

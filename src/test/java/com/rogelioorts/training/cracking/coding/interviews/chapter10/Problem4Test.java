package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem4Test {

  private Problem4 solver;
  
  @Before
  public void initialize() {
    solver = new Problem4();
  }
  
  @Test
  public void testA() {
    int[] arr = new int[] {1, 3, 4, 5, 7, 10, 14, 15, 16, 19, 20, 25};
    
    int result = solver.findElement(new Problem4.Listy(arr), 15);
    
    Assert.assertEquals(7, result);
  }
  
}

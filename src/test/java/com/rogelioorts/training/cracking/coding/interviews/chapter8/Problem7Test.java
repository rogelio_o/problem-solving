package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem7Test {

  private Problem7 solver;
  
  @Before
  public void initialize() {
    solver = new Problem7();
  }
  
  @Test
  public void testA() {
    Assert.assertThat(solver.getPermutations("ab"), is(Arrays.asList("ba", "ab")));
  }
  
  @Test
  public void testB() {
    Assert.assertThat(solver.getPermutations("abc"), is(Arrays.asList("cba", "bca", "bac", "cab", "acb", "abc")));
  }
  
}

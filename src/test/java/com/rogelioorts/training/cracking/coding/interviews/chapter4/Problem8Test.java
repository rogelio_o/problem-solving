package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem8Test {

    private Problem8 solver;

    @Before
    public void initialize() {
        solver = new Problem8();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        BinarySearchTreeNode<Integer> ancestor = new BinarySearchTreeNode<>(2);
        root.setLeft(ancestor);
        root.insert(1);
        BinarySearchTreeNode<Integer> node1 = new BinarySearchTreeNode<>(0);
        root.getLeft().getLeft().setLeft(node1);
        BinarySearchTreeNode<Integer> node2 = new BinarySearchTreeNode<>(3);
        root.getLeft().setRight(node2);
        root.insert(6);
        root.insert(7);

        Assert.assertEquals(ancestor, solver.searchCommonAncestor(node1, node2));
    }

}

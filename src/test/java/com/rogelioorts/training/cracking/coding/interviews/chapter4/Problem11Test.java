package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem11Test {

    private Problem11 solver;

    @Before
    public void initialize() {
        solver = new Problem11();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(10);
        root.insert(5);
        root.insert(4);
        root.insert(8);
        root.insert(20);
        root.insert(35);

        Integer result = solver.getRandomNode(root);
        System.out.println(result);
        Assert.assertNotNull(result);
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem4Test {

    private Problem4 solver;

    @Before
    public void initialize() {
        solver = new Problem4();
    }

    @Test
    public void testA() {
        ListNode<Integer> head = new ListNode<>(3);
        head.add(5).add(8).add(5).add(10).add(2).add(1);

        ListNode<Integer> newHead = solver.partition(head, 5);

        Assert.assertEquals((Integer) 1, newHead.getData());
        Assert.assertEquals((Integer) 2, newHead.getNext().getData());
        Assert.assertEquals((Integer) 3, newHead.getNext().getNext().getData());
        Assert.assertEquals((Integer) 10, newHead.getNext().getNext().getNext().getData());
        Assert.assertEquals((Integer) 5, newHead.getNext().getNext().getNext().getNext().getData());
        Assert.assertEquals((Integer) 8, newHead.getNext().getNext().getNext().getNext().getNext().getData());
        Assert.assertEquals((Integer) 5, newHead.getNext().getNext().getNext().getNext().getNext().getNext().getData());
        Assert.assertNull(newHead.getNext().getNext().getNext().getNext().getNext().getNext().getNext());
    }

}

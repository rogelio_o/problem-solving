package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem2Test {

    private Problem2 solver;

    @Before
    public void initialize() {
        solver = new Problem2();
    }

    @Test
    public void testA() {
        int[] arr = new int[] { 1, 2, 3, 4, 5, 6 };

        BinarySearchTreeNode<Integer> root = solver.createMinBSTree(arr);

        Assert.assertEquals((Integer) 4, root.getData());
        Assert.assertEquals((Integer) 2, root.getLeft().getData());
        Assert.assertEquals((Integer) 1, root.getLeft().getLeft().getData());
        Assert.assertEquals((Integer) 3, root.getLeft().getRight().getData());
        Assert.assertEquals((Integer) 6, root.getRight().getData());
        Assert.assertEquals((Integer) 5, root.getRight().getLeft().getData());
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.Coordinate;

public class Problem2Test {

    private Problem2 solver;

    @Before
    public void initialize() {
        solver = new Problem2();
    }

    @Test
    public void testA() {
        int[][] grid = new int[][] { { 0, 0, 1 }, { 1, 0, 0 }, { 1, 1, 0 } };

        Assert.assertThat(solver.goToEnd(grid), is(Arrays.asList(Coordinate.create(0, 0), Coordinate.create(0, 1),
                Coordinate.create(1, 1), Coordinate.create(1, 2), Coordinate.create(2, 2))));
    }

}

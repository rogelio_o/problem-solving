package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem5Test {

    private Problem5 solver;

    @Before
    public void initialize() {
        solver = new Problem5();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        root.setLeft(2);
        root.getLeft().setLeft(1);
        root.getLeft().setRight(5);
        root.setRight(6);

        Assert.assertFalse(solver.isBinarySearchTree(root));
    }

    @Test
    public void testB() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        root.setLeft(2);
        root.getLeft().setLeft(1);
        root.getLeft().setRight(3);
        root.setRight(6);

        Assert.assertTrue(solver.isBinarySearchTree(root));
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Test;

public class Problem10Test {

  @Test
  public void testA() {
    Problem10 solver = new Problem10();
    
    solver.track(5);
    solver.track(1);
    solver.track(4);
    solver.track(5);
    solver.track(9);
    solver.track(7);
    solver.track(13);
    solver.track(3);
    
    Assert.assertEquals(0, solver.getRankOfNumber(1));
    Assert.assertEquals(1, solver.getRankOfNumber(3));
    Assert.assertEquals(2, solver.getRankOfNumber(4)); 
  }
  
}

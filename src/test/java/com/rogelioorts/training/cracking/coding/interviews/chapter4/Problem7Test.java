package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.Graph;

public class Problem7Test {

    private Problem7 solver;

    @Before
    public void initialize() {
        solver = new Problem7();
    }

    @Test
    public void testA() {
        Graph<Character> g = new Graph<>();
        g.addVertex('a');
        g.addVertex('b');
        g.addVertex('c');
        g.addVertex('d');
        g.addVertex('e');
        g.addVertex('f');

        g.addEdge('a', 'd');
        g.addEdge('f', 'b');
        g.addEdge('b', 'd');
        g.addEdge('f', 'a');
        g.addEdge('d', 'c');

        Assert.assertThat(solver.getProjectsOrder(g), is(Arrays.asList('f', 'e', 'b', 'a', 'd', 'c')));
    }

}

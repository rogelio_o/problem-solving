package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem6Test {

    private Problem6 solver;

    @Before
    public void initialize() {
        solver = new Problem6();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        root.insert(2);
        root.insert(1);
        BinarySearchTreeNode<Integer> searchable = new BinarySearchTreeNode<>(3);
        root.getLeft().setRight(searchable);
        root.insert(6);
        root.insert(7);

        Assert.assertEquals((Integer) 4, solver.findSuccesor(searchable).getData());
    }

}

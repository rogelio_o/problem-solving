package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem7Test {

	private Problem7 solver;

	@Before
	public void initialize() {
		solver = new Problem7();
	}

	@Test
	public void testA() {
		int[] arr = new int[Integer.MAX_VALUE - 1];
		for(int num = 1, index = 0; index < arr.length; num++) {
		  if(num != 245654) {
		    arr[index++] = num;
		  }
		}

		Assert.assertEquals(245654, solver.findNotPresent(arr));
	}

    @Test
    public void testB() {
        int[] arr = new int[Integer.MAX_VALUE - 1];
        for(int num = 1, index = 0; index < arr.length; num++) {
          if(num != 245654) {
            arr[index++] = num;
          }
        }

        Assert.assertEquals(245654, solver.findNotPresentFollowUp(arr));
    }

}

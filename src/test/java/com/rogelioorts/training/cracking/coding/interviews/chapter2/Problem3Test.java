package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem3Test {

    private Problem3 solver;

    @Before
    public void initialize() {
        solver = new Problem3();
    }

    @Test
    public void testA() {
        ListNode<Character> head = new ListNode<>('a');
        head.add('b');
        ListNode<Character> toDelete = new ListNode<>('c');
        head.getNext().setNext(toDelete);
        toDelete.add('d').add('e');

        Assert.assertTrue(solver.deleteMiddleNode(toDelete));

        Assert.assertEquals((Character) 'a', head.getData());
        Assert.assertEquals((Character) 'b', head.getNext().getData());
        Assert.assertEquals((Character) 'd', head.getNext().getNext().getData());
        Assert.assertEquals((Character) 'e', head.getNext().getNext().getNext().getData());
        Assert.assertNull(head.getNext().getNext().getNext().getNext());
    }

    @Test
    public void testB() {
        ListNode<Character> head = new ListNode<>('a');
        head.add('b');
        ListNode<Character> toDelete = new ListNode<>('c');
        head.getNext().setNext(toDelete);

        Assert.assertFalse(solver.deleteMiddleNode(toDelete));

        Assert.assertEquals((Character) 'a', head.getData());
        Assert.assertEquals((Character) 'b', head.getNext().getData());
        Assert.assertEquals((Character) 'c', head.getNext().getNext().getData());
        Assert.assertNull(head.getNext().getNext().getNext());
    }

}

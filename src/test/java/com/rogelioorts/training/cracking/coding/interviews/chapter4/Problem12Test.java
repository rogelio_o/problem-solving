package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem12Test {

    private Problem12 solver;

    @Before
    public void initialize() {
        solver = new Problem12();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(5);
        root.insert(4);
        root.insert(2);
        root.insert(1);
        root.insert(3);
        root.insert(3);
        root.insert(6);

        Assert.assertEquals(3, solver.getNumPathWithSum(root, 6));
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem1Test {

  private Problem1 solver;
  
  @Before
  public void initialize() {
    solver = new Problem1();
  }
  
  @Test
  public void testA() {
    int[] a = new int[] {1, 4, 8, 20, 0, 0, 0};
    int[] b = new int[] {2, 10, 25};
    
    solver.sortedMerge(a, b);
    
    Assert.assertArrayEquals(new int[] {1, 2, 4, 8, 10, 20, 25}, a);
  }
  
}

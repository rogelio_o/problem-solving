package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem8Test {

private Problem8 solver;
  
  @Before
  public void initialize() {
    solver = new Problem8();
  }
  
  @Test
  public void testA() {
    Assert.assertThat(solver.getPermutations("aba"), is(Arrays.asList("ba", "ab")));
  }
  
  @Test
  public void testB() {
    Assert.assertThat(solver.getPermutations("abca"), is(Arrays.asList("cba", "bca", "bac", "cab", "acb", "abc")));
  }
  
}

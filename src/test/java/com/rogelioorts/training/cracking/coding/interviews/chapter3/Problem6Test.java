package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem6Test {

  private Problem6 solver;
  
  @Before
  public void initialize() {
    solver = new Problem6();
  }
  
  @Test
  public void testA() {
    solver.enqueue(new Problem6.Cat("cat1"));
    solver.enqueue(new Problem6.Dog("dog1"));
    solver.enqueue(new Problem6.Cat("cat2"));
    
    Assert.assertEquals("cat1", solver.dequeueAny().getName());
    Assert.assertEquals("dog1", solver.dequeueAny().getName());
  }
  
  @Test
  public void testB() {
    solver.enqueue(new Problem6.Cat("cat1"));
    solver.enqueue(new Problem6.Dog("dog1"));
    solver.enqueue(new Problem6.Cat("cat2"));
    
    Assert.assertEquals("dog1", solver.dequeueDog().getName());
  }
  
  @Test
  public void testC() {
    solver.enqueue(new Problem6.Dog("dog1"));
    solver.enqueue(new Problem6.Cat("cat1"));
    
    Assert.assertEquals("cat1", solver.dequeueCat().getName());
  }
  
}

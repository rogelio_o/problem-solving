package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem2Test {

  private Problem2 solver;

  @Before
  public void initialize() {
    solver = new Problem2();
  }

  @Test
  public void testA() {
    Assert.assertEquals(".001", solver.printBinary(0.125));
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem3Test {

    private Problem3 solver;

    @Before
    public void initializer() {
        solver = new Problem3();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        root.insert(2);
        root.insert(1);
        root.insert(3);
        root.insert(6);
        root.insert(5);

        ArrayList<LinkedList<Integer>> result = solver.getDataOfEachLevel(root);

        Assert.assertThat(result.get(0), is(Arrays.asList(4)));
        Assert.assertThat(result.get(1), is(Arrays.asList(2, 6)));
        Assert.assertThat(result.get(2), is(Arrays.asList(1, 3, 5)));
    }

}

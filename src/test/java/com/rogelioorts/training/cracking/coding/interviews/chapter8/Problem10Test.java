package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.Coordinate;

public class Problem10Test {

    private Problem10 solver;

    @Before
    public void initialize() {
        solver = new Problem10();
    }

    @Test
    public void testA() {
        int[][] picture = new int[][] { { 1, 2, 3, 4 }, { 5, 4, 4, 4 }, { 7, 4, 9, 4 } };

        solver.fill(picture, Coordinate.create(1, 3), 10);

        Assert.assertArrayEquals(new int[][] { { 1, 2, 3, 10 }, { 5, 10, 10, 10 }, { 7, 10, 9, 10 } }, picture);
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem5Test {

    private Problem5 solver;

    @Before
    public void initialize() {
        solver = new Problem5();
    }

    @Test
    public void testA() {
        ListNode<Integer> head1 = new ListNode<>(7);
        head1.add(1).add(6);
        ListNode<Integer> head2 = new ListNode<>(5);
        head2.add(9).add(2);

        ListNode<Integer> sumHead = solver.sum(head1, head2);

        Assert.assertEquals((Integer) 2, sumHead.getData());
        Assert.assertEquals((Integer) 1, sumHead.getNext().getData());
        Assert.assertEquals((Integer) 9, sumHead.getNext().getNext().getData());
        Assert.assertNull(sumHead.getNext().getNext().getNext());
    }

    @Test
    public void testB() {
        ListNode<Integer> head1 = new ListNode<>(7);
        head1.add(1).add(6).add(1);
        ListNode<Integer> head2 = new ListNode<>(5);
        head2.add(9).add(2);

        ListNode<Integer> sumHead = solver.sum(head1, head2);

        Assert.assertEquals((Integer) 2, sumHead.getData());
        Assert.assertEquals((Integer) 1, sumHead.getNext().getData());
        Assert.assertEquals((Integer) 9, sumHead.getNext().getNext().getData());
        Assert.assertEquals((Integer) 1, sumHead.getNext().getNext().getNext().getData());
        Assert.assertNull(sumHead.getNext().getNext().getNext().getNext());
    }

}

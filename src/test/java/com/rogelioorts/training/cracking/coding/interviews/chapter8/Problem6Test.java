package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem6Test {

  private Problem6 solver;
  
  @Before
  public void initialize() {
    solver = new Problem6();
  }
  
  @Test
  public void testA() {
    LinkedList<Integer> tower1 = new LinkedList<Integer>();
    tower1.add(1);
    tower1.add(2);
    tower1.add(3);
    tower1.add(4);
    LinkedList<Integer> tower2 = new LinkedList<Integer>();
    LinkedList<Integer> tower3 = new LinkedList<Integer>();
    solver.solveHanoi(tower1, tower2, tower3);
    
    Assert.assertTrue(tower1.isEmpty());
    Assert.assertTrue(tower2.isEmpty());
    Assert.assertThat(tower3, is(Arrays.asList(1, 2, 3, 4)));
  }
  
}

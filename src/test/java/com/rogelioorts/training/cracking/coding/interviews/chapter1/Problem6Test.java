package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem6Test {

  private Problem6 solver;

  @Before
  public void initialize() {
    solver = new Problem6();
  }

  @Test
  public void testA() {
    Assert.assertEquals("a2b1c5a3", solver.compress("aabcccccaaa"));
  }

  @Test
  public void testB() {
    Assert.assertEquals("abcd", solver.compress("abcd"));
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class Problem14Test {

  private Problem14 solver;
  
  @Before
  public void initialize() {
    solver = new Problem14();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(14, solver.countNumWaysToAddParentheses("0&0&0&1^1|0", true));
  }
  
}

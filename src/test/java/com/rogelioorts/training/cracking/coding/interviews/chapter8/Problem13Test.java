package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.Box;

public class Problem13Test {

    private Problem13 solver;

    @Before
    public void initialize() {
        solver = new Problem13();
    }

    @Test
    public void testA() {
        ArrayList<Box> stack = new ArrayList<>();
        stack.add(new Box(1, 1, 1));
        stack.add(new Box(2, 2, 2));
        stack.add(new Box(5, 3, 3));
        stack.add(new Box(3, 4, 4));

        Assert.assertEquals(7, solver.getMaxStackHeight(stack));
    }

}

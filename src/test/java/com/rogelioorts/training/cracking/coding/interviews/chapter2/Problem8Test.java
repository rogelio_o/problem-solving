package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem8Test {

    private Problem8 solver;

    @Before
    public void initialize() {
        solver = new Problem8();
    }

    @Test
    public void testA() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(2).add(3).add(4).add(5).add(6);

        ListNode<Integer> cycleStart = head.getNext().getNext().getNext();
        head.getNext().getNext().getNext().getNext().getNext().setNext(cycleStart);

        Assert.assertEquals(cycleStart, solver.getStartOfCycle(head));
    }

    @Test
    public void testB() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(2).add(3).add(4).add(5).add(6);

        Assert.assertNull(solver.getStartOfCycle(head));
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem3Test {
  
  private static final int STACK_MAX_SIZE = 1;

  private Problem3 solver;
  
  @Before
  public void initialize() {
    solver = new Problem3(STACK_MAX_SIZE);
  }
  
  @Test
  public void testPushPop() {
    solver.push(1);
    solver.push(2);
    
    Assert.assertEquals(2, solver.pop());
    Assert.assertEquals(1, solver.pop());
  }
  
  @Test(expected = IllegalStateException.class)
  public void testEmpty() {
    solver.push(1);
    solver.push(2);
    
    solver.pop();
    solver.pop();
    solver.pop();
  }
  
  @Test
  public void testPushPopAt() {
    solver.push(1);
    solver.push(2);
    solver.push(3);
    
    Assert.assertEquals(1, solver.popAt(0));
    Assert.assertEquals(2, solver.size());
    Assert.assertEquals(2, solver.stacksSize());
  }
  
}

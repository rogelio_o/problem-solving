package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem11Test {

  private Problem11 solver;
  
  @Before
  public void initialize() {
    solver = new Problem11();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(242, solver.countWaysToObtain(100));
  }
  
}

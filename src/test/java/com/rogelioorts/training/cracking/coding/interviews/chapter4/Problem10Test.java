package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem10Test {

    private Problem10 solver;

    @Before
    public void initialize() {
        solver = new Problem10();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root1 = new BinarySearchTreeNode<>(10);
        root1.insert(5);
        root1.insert(4);
        root1.insert(6);
        root1.insert(3);
        root1.insert(2);
        root1.insert(15);
        root1.insert(20);

        BinarySearchTreeNode<Integer> root2 = new BinarySearchTreeNode<>(5);
        root2.insert(4);
        root2.insert(6);
        root2.insert(3);
        root2.insert(2);

        Assert.assertTrue(solver.isSubtree(root1, root2));
    }

    @Test
    public void testB() {
        BinarySearchTreeNode<Integer> root1 = new BinarySearchTreeNode<>(10);
        root1.insert(5);
        root1.insert(4);
        root1.insert(6);
        root1.insert(3);
        root1.insert(15);
        root1.insert(20);

        BinarySearchTreeNode<Integer> root2 = new BinarySearchTreeNode<>(5);
        root2.insert(4);
        root2.insert(6);
        root2.insert(3);
        root2.insert(2);

        Assert.assertFalse(solver.isSubtree(root1, root2));
    }

}

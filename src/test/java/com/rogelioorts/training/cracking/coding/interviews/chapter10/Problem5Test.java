package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem5Test {

	private Problem5 solver;

	@Before
	public void initialize() {
		solver = new Problem5();
	}

	@Test
	public void testA() {
		String[] arr = new String[] { "at", "", "", "", "ball", "", "", "car", "", "", "dad", "", "" };
		Assert.assertEquals(4, solver.search(arr, "ball"));
	}

}

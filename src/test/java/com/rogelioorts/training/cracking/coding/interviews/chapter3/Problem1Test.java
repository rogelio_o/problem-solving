package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem1Test {
  
  private static final int SIZE =2;

  private Problem1 solver;
  
  @Before
  public void initialize() {
    solver = new Problem1(SIZE);
  }
  
  @Test(expected = IllegalStateException.class)
  public void testPushOverflow() {
    solver.push(1, 2);
    solver.push(1, 3);
    solver.push(1, 4);
  }
  
  @Test
  public void testPopRemove() {
    solver.push(1, 2);
    solver.push(1, 3);
    
    Assert.assertEquals(3, solver.pop(1));
    Assert.assertEquals(2, solver.peek(1));
  }
  
  @Test(expected = IllegalStateException.class)
  public void testPopEmpty() {
    solver.pop(1);
  }
  
  @Test
  public void testPushPopMultipleStacks() {
    solver.push(1, 2);
    solver.push(2, 3);
    solver.push(3, 4);
    
    Assert.assertEquals(2, solver.pop(1));
    Assert.assertEquals(3, solver.pop(2));
    Assert.assertEquals(4, solver.pop(3));
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testPushErrorNotValidStack() {
    solver.push(4, 1);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testPopErrorNotValidStack() {
    solver.pop(4);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testPeekErrorNotValidStack() {
    solver.peek(4);
  }
  
}

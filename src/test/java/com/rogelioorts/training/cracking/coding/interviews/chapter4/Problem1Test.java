package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.Graph;

public class Problem1Test {

    private Problem1 solver;

    @Before
    public void initialize() {
        solver = new Problem1();
    }

    @Test
    public void testA() {
        Graph<Character> g = new Graph<>();
        g.addVertex('a');
        g.addVertex('b');
        g.addVertex('c');
        g.addVertex('d');

        g.addEdge('a', 'c');
        g.addEdge('b', 'a');
        g.addEdge('c', 'd');

        Assert.assertTrue(solver.existsPath(g, 'a', 'd'));
    }

    @Test
    public void testB() {
        Graph<Character> g = new Graph<>();
        g.addVertex('a');
        g.addVertex('b');
        g.addVertex('c');
        g.addVertex('d');

        g.addEdge('a', 'c');
        g.addEdge('b', 'a');

        Assert.assertFalse(solver.existsPath(g, 'a', 'd'));
    }

}

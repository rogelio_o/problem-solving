package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem3Test {

  private Problem3 solver;

  @Before
  public void initialize() {
    solver = new Problem3();
  }

  @Test
  public void testA() {
    Assert.assertEquals(8, solver.getLengthOfMathOnesSequence(0b11011101111));
  }

}

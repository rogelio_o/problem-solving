package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem7Test {

    private Problem7 solver;

    @Before
    public void initialize() {
        solver = new Problem7();
    }

    @Test
    public void testA() {
        ListNode<Integer> head1 = new ListNode<>(1);
        head1.add(2).add(3);

        ListNode<Integer> head2 = new ListNode<>(0);

        ListNode<Integer> commonHead = new ListNode<>(4);
        commonHead.add(5).add(6);

        head1.getNext().getNext().setNext(commonHead);
        head2.setNext(commonHead);

        Assert.assertEquals(commonHead, solver.findIntersection(head1, head2));
    }

    @Test
    public void testB() {
        ListNode<Integer> head1 = new ListNode<>(1);
        head1.add(2).add(3);

        ListNode<Integer> head2 = new ListNode<>(0);
        head2.add(5).add(6);

        Assert.assertNull(solver.findIntersection(head1, head2));
    }

}

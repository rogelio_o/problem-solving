package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem7Test {

  private Problem7 solver;

  @Before
  public void initialize() {
    solver = new Problem7();
  }

  @Test
  public void testA() {
    int[][] screen = new int[][] {
        { 1, 2, 3, 5 },
        { 6, 7, 8, 9 },
        { 10, 11, 12, 13 },
        { 14, 15, 16, 17 }
    };
    solver.rotate(screen);
    Assert.assertArrayEquals(new int[][] {
        { 5, 9, 13, 17 },
        { 3, 8, 12, 16 },
        { 2, 7, 11, 15 },
        { 1, 6, 10, 14 }
    }, screen);
  }

}

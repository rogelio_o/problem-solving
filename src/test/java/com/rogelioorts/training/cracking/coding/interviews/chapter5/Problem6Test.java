package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem6Test {

  private Problem6 solver;

  @Before
  public void initialize() {
    solver = new Problem6();
  }

  @Test
  public void testA() {
    Assert.assertEquals(2, solver.countNumFlips(0b11101, 0b01111));
  }

}

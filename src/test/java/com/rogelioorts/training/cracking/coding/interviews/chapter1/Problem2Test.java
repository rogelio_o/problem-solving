package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem2Test {

  private Problem2 solver;

  @Before
  public void initialize() {
    solver = new Problem2();
  }

  @Test
  public void testA() {
    Assert.assertFalse(solver.isPermutation("abcdef", "ghijkl"));
  }

  @Test
  public void testB() {
    Assert.assertTrue(solver.isPermutation("abbdef", "febbda"));
  }

  @Test
  public void testC() {
    Assert.assertFalse(solver.isPermutation("abcdef", "abc"));
  }

}

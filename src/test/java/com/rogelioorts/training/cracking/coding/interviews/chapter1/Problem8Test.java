package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem8Test {

  private Problem8 solver;

  @Before
  public void initialize() {
    solver = new Problem8();
  }

  @Test
  public void testA() {
    int[][] screen = new int[][] {
        { 0, 2, 3 },
        { 6, 0, 8 },
        { 10, 11, 12 },
        { 14, 15, 16 }
    };
    solver.zerify(screen);
    Assert.assertArrayEquals(new int[][] {
        { 0, 0, 0 },
        { 0, 0, 0 },
        { 0, 0, 12 },
        { 0, 0, 16 }
    }, screen);
  }

}

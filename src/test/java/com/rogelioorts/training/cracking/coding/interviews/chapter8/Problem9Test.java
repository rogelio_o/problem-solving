package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem9Test {

  private Problem9 solver;
  
  @Before
  public void initialize() {
    solver = new Problem9();
  }
  
  @Test
  public void testA() {
    Assert.assertThat(
        new ArrayList<>(solver.getParenthesesCombinations(3)), 
        is(Arrays.asList("()()()", "(()())", "()(())", "((()))", "(())()"))
    );
  }
  
}

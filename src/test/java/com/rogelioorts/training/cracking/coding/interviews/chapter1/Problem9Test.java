package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem9Test {

  private Problem9 solver;

  @Before
  public void initialize() {
    solver = new Problem9();
  }

  @Test
  public void testA() {
    Assert.assertTrue(solver.isRotation("waterbottle", "erbottlewat"));
  }

  @Test
  public void testB() {
    Assert.assertFalse(solver.isRotation("waterbottle", "water"));
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem4Test {

  private Problem4 solver;
  
  @Before
  public void initialize() {
    solver = new Problem4();
  }
  
  @Test
  public void testA() {
    solver.add(1);
    solver.add(2);
    solver.add(3);
    
    Assert.assertEquals(1, solver.remove());
    Assert.assertEquals(2, solver.remove());
    Assert.assertEquals(3, solver.peek());
  }
  
}

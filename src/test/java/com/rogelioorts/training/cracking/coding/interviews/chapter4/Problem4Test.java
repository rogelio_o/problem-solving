package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem4Test {

    private Problem4 solver;

    @Before
    public void initialize() {
        solver = new Problem4();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        root.insert(2);
        root.insert(1);
        root.insert(3);
        root.insert(6);
        root.insert(5);
        root.insert(7);
        root.insert(8);

        Assert.assertTrue(solver.isBalanced(root));
    }

    @Test
    public void testB() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(4);
        root.insert(2);
        root.insert(1);
        root.insert(3);
        root.insert(6);
        root.insert(5);
        root.insert(7);
        root.insert(8);
        root.insert(9);

        Assert.assertFalse(solver.isBalanced(root));
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem1Test {

  private Problem1 solver;

  @Before
  public void initialize() {
    solver = new Problem1();
  }

  @Test
  public void testA1() {
    Assert.assertFalse(solver.isUnique("abca"));
  }

  @Test
  public void testB1() {
    Assert.assertTrue(solver.isUnique("abcd"));
  }

  @Test
  public void testA2() {
    Assert.assertFalse(solver.isUniqueWithoutExtraDataStructure("abca"));
  }

  @Test
  public void testB2() {
    Assert.assertTrue(solver.isUniqueWithoutExtraDataStructure("abcd"));
  }

}

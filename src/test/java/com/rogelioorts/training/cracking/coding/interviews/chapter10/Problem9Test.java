package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.Coordinate;

public class Problem9Test {

    private Problem9 solver;

    @Before
    public void initialize() {
        solver = new Problem9();
    }

    @Test
    public void testA() {
        int[][] matrix = new int[][] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
        Coordinate result = solver.findElement(matrix, 11);
        Assert.assertEquals(2, result.getI());
        Assert.assertEquals(2, result.getJ());
    }

}

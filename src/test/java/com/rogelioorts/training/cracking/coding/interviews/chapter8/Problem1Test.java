package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem1Test {

  private Problem1 solver;

  @Before
  public void initialize() {
    solver = new Problem1();
  }

  @Test
  public void testA() {
    Assert.assertEquals(6, solver.countWaysToRunningUpAStaircase(4));
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem12Test {

  private Problem12 solver;
  
  @Before
  public void initialize() {
    solver = new Problem12();
  }
  
  @Test
  public void testA() {
    ArrayList<Integer[]> result = solver.solveQueens(4);
    
    Assert.assertArrayEquals(result.get(0), new Integer[] {1, 3, 0, 2});
    Assert.assertArrayEquals(result.get(1), new Integer[] {2, 0, 3, 1});
    Assert.assertEquals(2, result.size());
  }
  
}

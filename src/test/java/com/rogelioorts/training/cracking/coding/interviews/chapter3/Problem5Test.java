package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem5Test {

  private Problem5 solver;
  
  @Before
  public void initialize() {
    solver = new Problem5();
  }
  
  @Test
  public void testA() {
    solver.push(1);
    solver.push(3);
    solver.push(2);
    solver.push(5);
    
    Assert.assertEquals(1, solver.pop());
    Assert.assertEquals(2, solver.pop());
    Assert.assertEquals(3, solver.pop());
    Assert.assertEquals(5, solver.pop());
  }
  
}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem4Test {

  private Problem4 solver;

  @Before
  public void initialize() {
    solver = new Problem4();
  }

  @Test
  public void testA() {
    LinkedList<Integer> set = new LinkedList<>();
    set.add(1);
    set.add(2);
    set.add(3);

    ArrayList<LinkedList<Integer>> result = solver.getSubsets(set);

    Assert.assertThat(result.get(0), is(Arrays.asList(1)));
    Assert.assertThat(result.get(1), is(Arrays.asList(1, 2)));
    Assert.assertThat(result.get(2), is(Arrays.asList(2)));
    Assert.assertThat(result.get(3), is(Arrays.asList(1, 3)));
    Assert.assertThat(result.get(4), is(Arrays.asList(1, 2, 3)));
    Assert.assertThat(result.get(5), is(Arrays.asList(2, 3)));
    Assert.assertThat(result.get(6), is(Arrays.asList(3)));
  }

}

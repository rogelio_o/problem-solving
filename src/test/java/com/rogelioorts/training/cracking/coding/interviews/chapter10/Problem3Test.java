package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem3Test {

  private Problem3 solver;
  
  @Before
  public void initialize() {
    solver = new Problem3();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(
        8, 
        solver.findIndex(new int[] {15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14}, 5)
    );
  }
  
}

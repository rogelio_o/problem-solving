package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem3Test {

  private Problem3 solver;

  @Before
  public void initialize() {
    solver = new Problem3();
  }

  @Test
  public void testA() {
    char[] str = new char[] { 'M', 'r', ' ', 'J', 'o', 'h', 'n', ' ', 'S', 'm', 'i', 't', 'h', ' ', ' ', ' ', ' ' };
    solver.urlify(str, 13);
    Assert.assertArrayEquals(new char[] { 'M', 'r', '%', '2', '0', 'J', 'o', 'h', 'n', '%', '2', '0', 'S', 'm', 'i', 't', 'h' }, str);
  }

}

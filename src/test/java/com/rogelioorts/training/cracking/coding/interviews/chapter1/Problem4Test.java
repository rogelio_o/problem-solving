package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem4Test {

  private Problem4 solver;

  @Before
  public void initialize() {
    solver = new Problem4();
  }

  @Test
  public void testA() {
    Assert.assertTrue(solver.hasPalindrome("Tact Coa"));
  }

  @Test
  public void testB() {
    Assert.assertFalse(solver.hasPalindrome("Hello"));
  }

}

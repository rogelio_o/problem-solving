package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem11Test {

	private Problem11 solver;

	@Before
	public void initialize() {
		solver = new Problem11();
	}

	@Test
	public void testA() {
		int[] arr = new int[] { 5, 3, 1, 2, 3 };
		solver.convertPeaksAndValleys(arr);
		Assert.assertArrayEquals(new int[] { 3, 5, 1, 3, 2 }, arr);
	}

	@Test
	public void testB() {
		int[] arr = new int[] { 6, 5, 5, 7, 3 };
		solver.convertPeaksAndValleys(arr);
		Assert.assertArrayEquals(new int[] { 5, 6, 5, 7, 3 }, arr);
	}

}

package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem1Test {

  private Problem1 solver;

  @Before
  public void initialize() {
    solver = new Problem1();
  }

  @Test
  public void testA() {
    Assert.assertEquals(0b10001001100, solver.insertNumber(0b10000010000, 0b10011, 2, 6));
  }

}

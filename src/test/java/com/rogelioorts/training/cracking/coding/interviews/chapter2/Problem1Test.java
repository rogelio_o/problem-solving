package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem1Test {

    private Problem1 solver;

    @Before
    public void initialize() {
        solver = new Problem1();
    }

    @Test
    public void testA1() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(1).add(1).add(2).add(3).add(2).add(4).add(3);
        solver.removeRepeated(head);

        Assert.assertEquals((Integer) 1, head.getData());
        Assert.assertEquals((Integer) 2, head.getNext().getData());
        Assert.assertEquals((Integer) 3, head.getNext().getNext().getData());
        Assert.assertEquals((Integer) 4, head.getNext().getNext().getNext().getData());
        Assert.assertNull(head.getNext().getNext().getNext().getNext());
    }

    @Test
    public void testA2() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(1).add(1).add(2).add(3).add(2).add(4).add(3);
        solver.removeRepeatedWithoutDataStructure(head);

        Assert.assertEquals((Integer) 1, head.getData());
        Assert.assertEquals((Integer) 2, head.getNext().getData());
        Assert.assertEquals((Integer) 3, head.getNext().getNext().getData());
        Assert.assertEquals((Integer) 4, head.getNext().getNext().getNext().getData());
        Assert.assertNull(head.getNext().getNext().getNext().getNext());
    }

}

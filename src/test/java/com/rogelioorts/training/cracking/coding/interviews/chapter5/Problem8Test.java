package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem8Test {

  private Problem8 solver;

  @Before
  public void initialize() {
    solver = new Problem8();
  }

  @Test
  public void testA() {
    byte[] screen = new byte[] {
        (byte) 0xaa, (byte) 0xaa, (byte) 0xaa,
        (byte) 0xaa, (byte) 0xaa, (byte) 0xaa,
        (byte) 0xaa, (byte) 0xaa, (byte) 0xaa
    };
    solver.drawLine(screen, 24, 5, 17, 1);
    Assert.assertArrayEquals(new byte[] {
        (byte) 0xaa, (byte) 0xaa, (byte) 0xaa,
        (byte) 0b10101111, (byte) 0xff, (byte) 0b11101010,
        (byte) 0xaa, (byte) 0xaa, (byte) 0xaa
    }, screen);
  }

  @Test
  public void testB() {
    byte[] screen = new byte[] {
        (byte) 0xaa,
        (byte) 0xaa,
        (byte) 0xaa,
    };
    solver.drawLine(screen, 8, 1, 6, 1);
    Assert.assertArrayEquals(new byte[] {
        (byte) 0xaa,
        (byte) 0b11111110,
        (byte) 0xaa,
    }, screen);
  }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem5Test {

  private Problem5 solver;
  
  @Before
  public void initialize() {
    solver = new Problem5();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(8, solver.multiply(2, 4));
  }
  
  @Test
  public void testB() {
    Assert.assertEquals(27, solver.multiply(3, 9));
  }
  
}

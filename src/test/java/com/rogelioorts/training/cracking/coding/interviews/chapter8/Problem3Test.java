package com.rogelioorts.training.cracking.coding.interviews.chapter8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem3Test {

  private Problem3 solver;

  @Before
  public void initialize() {
    solver = new Problem3();
  }

  @Test
  public void testA() {
    Assert.assertEquals(2, solver.findFirstMagicIndex(new int[] { -10, -5, 2, 2, 2, 3, 4, 7, 9, 12, 13 }));
  }

}

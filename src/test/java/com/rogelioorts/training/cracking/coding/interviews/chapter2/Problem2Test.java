package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem2Test {

    private Problem2 solver;

    @Before
    public void initialize() {
        solver = new Problem2();
    }

    @Test
    public void testA() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(10).add(9).add(8).add(3).add(2).add(4).add(7);

        Assert.assertEquals(2, solver.findLast(head, 3));
    }

    @Test
    public void testB() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(5).add(6).add(2);

        Assert.assertEquals(1, solver.findLast(head, 4));
    }

    @Test
    public void testC() {
        ListNode<Integer> head = new ListNode<>(1);
        head.add(5).add(6).add(2);

        Assert.assertEquals(-1, solver.findLast(head, 5));
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.cracking.coding.interviews.utils.ListNode;

public class Problem6Test {

    private Problem6 solver;

    @Before
    public void initialize() {
        solver = new Problem6();
    }

    @Test
    public void testA() {
        ListNode<Character> str1 = new ListNode<>('a');
        str1.add('b').add('c').add('b').add('a');

        Assert.assertTrue(solver.isPalindrome(str1));
    }

    @Test
    public void testB() {
        ListNode<Character> str1 = new ListNode<>('a');
        str1.add('b').add('b').add('a');

        Assert.assertTrue(solver.isPalindrome(str1));
    }

    @Test
    public void testC() {
        ListNode<Character> str1 = new ListNode<>('a');
        str1.add('b').add('c').add('b').add('d');

        Assert.assertFalse(solver.isPalindrome(str1));
    }

}

package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem2Test {

  private Problem2 solver;
  
  @Before
  public void initialize() {
    solver = new Problem2();
  }
  
  @Test
  public void testA() {
    String[] arr = new String[] {"abc", "dda", "cba", "dad", "add"};
    
    solver.sortAnagrams(arr);
    
    Assert.assertArrayEquals(new String[] {"dda", "dad", "add", "abc", "cba"}, arr);
  }
  
}

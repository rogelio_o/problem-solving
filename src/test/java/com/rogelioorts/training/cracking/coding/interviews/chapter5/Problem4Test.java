package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem4Test {

  private Problem4 solver;

  @Before
  public void initialize() {
    solver = new Problem4();
  }

  @Test
  public void testA() {
    Assert.assertEquals(0b1100, solver.getNextLargest(0b1010));
  }

  @Test
  public void testB() {
    Assert.assertEquals(0b10011, solver.getNextLargest(0b1110));
  }

  @Test
  public void testC() {
    Assert.assertEquals(0b1100100, solver.getNextSmallest(0b1101000));
  }

}

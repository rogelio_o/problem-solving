package com.rogelioorts.training.cracking.coding.interviews.chapter1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem5Test {

  private Problem5 solver;

  @Before
  public void initialize() {
    solver = new Problem5();
  }

  @Test
  public void testA() {
    Assert.assertTrue(solver.isOneModificationAway("pale", "ple"));
  }

  @Test
  public void testB() {
    Assert.assertTrue(solver.isOneModificationAway("pales", "pale"));
  }

  @Test
  public void testC() {
    Assert.assertTrue(solver.isOneModificationAway("pale", "bale"));
  }

  @Test
  public void testD() {
    Assert.assertFalse(solver.isOneModificationAway("pale", "bake"));
  }

  @Test
  public void testE() {
    Assert.assertFalse(solver.isOneModificationAway("pales", "pal"));
  }

}

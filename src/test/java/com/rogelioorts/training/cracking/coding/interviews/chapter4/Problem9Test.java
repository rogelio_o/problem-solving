package com.rogelioorts.training.cracking.coding.interviews.chapter4;

import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.BinarySearchTreeNode;

public class Problem9Test {

    private Problem9 solver;

    @Before
    public void initialize() {
        solver = new Problem9();
    }

    @Test
    public void testA() {
        BinarySearchTreeNode<Integer> root = new BinarySearchTreeNode<>(2);
        root.insert(0);
        root.insert(1);
        root.insert(3);
        root.insert(4);

        ArrayList<LinkedList<Integer>> result = solver.allSequences(root);

        Assert.assertEquals(6, result.size());
        Assert.assertThat(result.get(0), is(Arrays.asList(2, 0, 1, 3, 4)));
        Assert.assertThat(result.get(1), is(Arrays.asList(2, 0, 3, 1, 4)));
        Assert.assertThat(result.get(2), is(Arrays.asList(2, 0, 3, 4, 1)));
        Assert.assertThat(result.get(3), is(Arrays.asList(2, 3, 0, 1, 4)));
        Assert.assertThat(result.get(4), is(Arrays.asList(2, 3, 0, 4, 1)));
        Assert.assertThat(result.get(5), is(Arrays.asList(2, 3, 4, 0, 1)));
    }

}

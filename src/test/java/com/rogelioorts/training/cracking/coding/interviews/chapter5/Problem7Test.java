package com.rogelioorts.training.cracking.coding.interviews.chapter5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem7Test {

  private Problem7 solver;

  @Before
  public void initialize() {
    solver = new Problem7();
  }

  @Test
  public void testA() {
    Assert.assertEquals(0b10110, solver.swapOddsAndEvens(0b101001));
  }

}

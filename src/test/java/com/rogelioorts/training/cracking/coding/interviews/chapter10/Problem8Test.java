package com.rogelioorts.training.cracking.coding.interviews.chapter10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem8Test {

  private Problem8 solver;

  @Before
  public void initialize() {
      solver = new Problem8();
  }

  @Test
  public void testA() {
      int[] arr = new int[] { 1, 2, 2, 3, 4 };

      Assert.assertEquals(2, solver.findDuplicated(arr));
  }
  
}

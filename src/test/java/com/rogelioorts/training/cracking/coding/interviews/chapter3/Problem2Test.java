package com.rogelioorts.training.cracking.coding.interviews.chapter3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem2Test {

  private Problem2 solver;
  
  @Before
  public void initialize() {
    solver = new Problem2();
  }
  
  @Test
  public void testA() {
    solver.push(2);
    solver.push(3);
    solver.push(1);
    
    Assert.assertEquals(1, solver.min());
    Assert.assertEquals(1, solver.pop());
    Assert.assertEquals(2, solver.min());
  }
  
}

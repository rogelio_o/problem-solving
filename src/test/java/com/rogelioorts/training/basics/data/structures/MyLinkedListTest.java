package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyLinkedListTest {
  
  private MyLinkedList<Integer> list;
  
  @Before
  public void initialize() {
    list = new MyLinkedList<>();
  }

  @Test
  public void testAdd() {
    list.add(1);
    list.add(2);
    
    Assert.assertArrayEquals(new int[] {2, 1}, getResult());
  }
  
  private int[] getResult() {
    int[] result = new int[list.size()];
    int index = 0;
    for(Integer num : list) {
      result[index] = num;
      index++;
    }
    
    return result;
  }
  
  @Test
  public void testRemove() {
    list.add(1);
    list.add(2);
    list.remove();
    
    Assert.assertArrayEquals(new int[] {1}, getResult());
  }
  
  @Test
  public void testRemoveValue() {
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(2);
    list.add(4);
    list.remove(2);
    
    Assert.assertArrayEquals(new int[] {4, 3, 2, 1}, getResult());
  }
  
  @Test
  public void testRemoveValue2() {
    list.add(1);
    list.remove(1);
    
    Assert.assertArrayEquals(new int[] {}, getResult());
  }
  
  @Test
  public void testPeek() {
    list.add(1);
    list.add(2);
    
    Assert.assertEquals((Integer) 2, list.peek());
  }
  
}

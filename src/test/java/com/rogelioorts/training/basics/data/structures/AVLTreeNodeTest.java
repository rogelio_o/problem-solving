package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Test;

public class AVLTreeNodeTest {

  @Test
  public void testA() {
    AVLTreeNode<Integer> root = new AVLTreeNode<>(5);
    root.insert(4);
    root.insert(3);
    
    Assert.assertEquals((Integer) 4, root.getData());
    Assert.assertEquals((Integer) 5, root.getRight().getData());
    Assert.assertEquals((Integer) 3, root.getLeft().getData());
  }

  @Test
  public void testB() {
    AVLTreeNode<Integer> root = new AVLTreeNode<>(1);
    root.insert(4);
    root.insert(5);
    
    Assert.assertEquals((Integer) 4, root.getData());
    Assert.assertEquals((Integer) 5, root.getRight().getData());
    Assert.assertEquals((Integer) 1, root.getLeft().getData());
  }

  @Test
  public void testC() {
    AVLTreeNode<Integer> root = new AVLTreeNode<>(9);
    root.insert(10);
    root.insert(15);
    root.insert(12);
    
    Assert.assertEquals((Integer) 10, root.getData());
    Assert.assertEquals((Integer) 9, root.getLeft().getData());
    Assert.assertEquals((Integer) 15, root.getRight().getData());
    Assert.assertEquals((Integer) 12, root.getRight().getLeft().getData());
  }
  
}

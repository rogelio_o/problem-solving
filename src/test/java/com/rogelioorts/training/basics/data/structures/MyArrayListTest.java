package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Test;

public class MyArrayListTest {

  @Test
  public void testAdd() {
    MyArrayList<Integer> list = new MyArrayList<>(2);
    list.add(1);
    list.add(2);
    
    Assert.assertArrayEquals(new Object[] {1, 2}, list.getArray());
  }
  
  @Test
  public void testAddAtLimit() {
    MyArrayList<Integer> list = new MyArrayList<>(2);
    list.add(1);
    list.add(2);
    list.add(3);
    
    Assert.assertArrayEquals(new Object[] {1, 2, 3, null}, list.getArray());
  }
  
  @Test
  public void testRemove() {
    MyArrayList<Integer> list = new MyArrayList<>(2);
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);
    list.remove(1);
    list.add(5);
    
    Assert.assertArrayEquals(new Object[] {1, 3, 4, 5}, list.getArray());
  }
  
  @Test
  public void testSize() {
    MyArrayList<Integer> list = new MyArrayList<>(2);
    list.add(1);
    list.add(2);
    list.add(3);
    list.remove(1);
    
    Assert.assertEquals(2, list.size());
  }
  
  @Test
  public void testGet() {
    MyArrayList<Integer> list = new MyArrayList<>(2);
    list.add(1);
    list.add(2);
    list.add(3);
    list.remove(1);
    
    Assert.assertEquals((Integer) 3, list.get(1));
  }
  
}

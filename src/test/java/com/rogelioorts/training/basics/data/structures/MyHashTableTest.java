package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyHashTableTest {
  
  private MyHashTable<String, Integer> table;

  @Before
  public void initialize() {
    table = new MyHashTable<>(10);
  }
  
  @Test
  public void testPutAndGet() {
    table.put("a", 0);
    table.put("k", 1);
    table.put("m", 1);
    table.put(null, 2);

    Assert.assertEquals((Integer) 0, table.get("a"));
    Assert.assertEquals((Integer) 1, table.get("k"));
    Assert.assertEquals((Integer) 1, table.get("m"));
    Assert.assertEquals((Integer) 2, table.get(null));
  }
  
  @Test
  public void testRemove() {
    table.put("a", 0);
    table.put("k", 1);
    table.put("m", 1);
    
    table.remove("k");

    Assert.assertNull(table.get("k"));
  }
  
}

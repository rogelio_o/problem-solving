package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyQueueTest {

  private MyQueue<Integer> queue;
  
  @Before
  public void initialize() {
    queue = new MyQueue<Integer>();
  }
  
  @Test
  public void test() {
    queue.add(1);
    
    Assert.assertEquals((Integer) 1, queue.remove());
  }
  
  @Test
  public void testB() {
    queue.add(1);
    queue.add(2);
    
    Assert.assertEquals((Integer) 1, queue.remove());
    Assert.assertEquals((Integer) 2, queue.remove());
  }
  
}

package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyStringBuilderTest {
  
  private MyStringBuilder builder;
  
  @Before
  public void initialize() {
    builder = new MyStringBuilder(4);
  }

  @Test
  public void testAppend() {
    builder.append('a');
    builder.append('b');
    builder.append('c');
    builder.append('d');
    builder.append('e');
    
    Assert.assertArrayEquals(
        new Object[] {'a', 'b', 'c', 'd', 'e', null, null, null}, 
        builder.getStr().getArray()
    );
  }
  
  @Test
  public void testToString() {
    builder.append('a');
    builder.append('b');
    builder.append('c');
    builder.append('d');
    builder.append('e');
    
    Assert.assertEquals("abcde", builder.toString());
  }
  
}

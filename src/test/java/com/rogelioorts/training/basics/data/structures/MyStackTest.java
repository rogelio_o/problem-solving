package com.rogelioorts.training.basics.data.structures;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyStackTest {

  private MyStack<Integer> stack;
  
  @Before
  public void initialize() {
    stack = new MyStack<>();
  }
  
  @Test
  public void test() {
    stack.push(1);
    
    Assert.assertEquals((Integer) 1, stack.pop());
  }
  
  @Test
  public void testB() {
    stack.push(1);
    stack.push(2);

    Assert.assertEquals((Integer) 2, stack.pop());
    Assert.assertEquals((Integer) 1, stack.pop());
  }
  
}

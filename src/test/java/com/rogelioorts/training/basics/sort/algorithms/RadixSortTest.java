package com.rogelioorts.training.basics.sort.algorithms;

import org.junit.Assert;
import org.junit.Test;

public class RadixSortTest {

  @Test
  public void testA() {
    int[] array = new int[] {12, 10, 2, 84, 23, 15, 8, 4, 5};
    
    RadixSort.sort(array);
    
    Assert.assertArrayEquals(new int[] {2, 4, 5, 8, 10, 12, 15, 23, 84}, array);
  }
  
}

package com.rogelioorts.training.basics.sort.algorithms;

import org.junit.Assert;
import org.junit.Test;

public class BubbleSortTest {

  @Test
  public void testA() {
    int[] array = new int[] {6, 5, 3, 1, 8, 7, 2, 4};
    
    BubbleSort.sort(array);
    
    Assert.assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8}, array);
  }
  
}

package com.rogelioorts.training.basics.algorithms;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RabinKarpAlgorithmTest {

	private RabinKarpAlgorithm solver;

	@Before
	public void initialize() {
		solver = new RabinKarpAlgorithm();
	}

	@Test
	public void testA() {
		Assert.assertEquals(10, solver.indexOf("THIS IS A TEST TEXT", "TEST"));
	}

	@Test
	public void testB() {
		Assert.assertEquals(10, solver.indexOf("THIS IS A TEST", "TEST"));
	}

	@Test
	public void testC() {
		Assert.assertEquals(-1, solver.indexOf("THIS IS A TEXT", "TEST"));
	}

	@Test
	public void testD() {
		Assert.assertEquals(0, solver.indexOf("TEST", "TEST"));
	}

}

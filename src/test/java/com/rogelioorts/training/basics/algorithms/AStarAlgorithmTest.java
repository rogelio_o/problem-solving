package com.rogelioorts.training.basics.algorithms;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.algorithms.AStarAlgorithm.Point;

public class AStarAlgorithmTest {

  private AStarAlgorithm solver;

  @Before
  public void initialize() {
    solver = new AStarAlgorithm();
  }

  @Test
  public void testA() {
    boolean[][] grid = new boolean[50][50];
    for (int j = 0; j < 50; j++) {
      if (j != 20) {
        grid[48][j] = true;
      }
    }

    LinkedList<Point> result = solver.findShortestPath(grid, new Point(0, 21), new Point(49, 49));
    Assert.assertThat(result,
        is(Arrays.asList(new Point(0, 21), new Point(1, 21), new Point(2, 21), new Point(3, 21), new Point(4, 21),
            new Point(5, 21), new Point(6, 21), new Point(7, 21), new Point(8, 21), new Point(9, 21), new Point(10, 21),
            new Point(11, 21), new Point(12, 21), new Point(13, 21), new Point(14, 21), new Point(15, 21),
            new Point(16, 21), new Point(17, 21), new Point(18, 21), new Point(19, 21), new Point(20, 21),
            new Point(21, 21), new Point(22, 21), new Point(23, 21), new Point(24, 21), new Point(25, 21),
            new Point(26, 21), new Point(27, 21), new Point(28, 21), new Point(29, 21), new Point(30, 21),
            new Point(31, 21), new Point(32, 21), new Point(33, 21), new Point(34, 21), new Point(35, 21),
            new Point(36, 21), new Point(37, 21), new Point(38, 21), new Point(39, 21), new Point(40, 21),
            new Point(41, 21), new Point(42, 21), new Point(43, 21), new Point(44, 21), new Point(45, 21),
            new Point(46, 21), new Point(47, 21), new Point(47, 20), new Point(48, 20), new Point(49, 20),
            new Point(49, 21), new Point(49, 22), new Point(49, 23), new Point(49, 24), new Point(49, 25),
            new Point(49, 26), new Point(49, 27), new Point(49, 28), new Point(49, 29), new Point(49, 30),
            new Point(49, 31), new Point(49, 32), new Point(49, 33), new Point(49, 34), new Point(49, 35),
            new Point(49, 36), new Point(49, 37), new Point(49, 38), new Point(49, 39), new Point(49, 40),
            new Point(49, 41), new Point(49, 42), new Point(49, 43), new Point(49, 44), new Point(49, 45),
            new Point(49, 46), new Point(49, 47), new Point(49, 48), new Point(49, 49))));
  }

}

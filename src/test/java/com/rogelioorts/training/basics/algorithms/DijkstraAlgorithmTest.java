package com.rogelioorts.training.basics.algorithms;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.WeightedGraph;

public class DijkstraAlgorithmTest {

  private DijkstraAlgorithm solver;

  @Before
  public void initialize() {
    solver = new DijkstraAlgorithm();
  }

  @Test
  public void testA() {
    WeightedGraph<Character> g = new WeightedGraph<>();

    g.addVertex('a');
    g.addVertex('b');
    g.addVertex('c');
    g.addVertex('d');
    g.addVertex('e');
    g.addVertex('f');
    g.addVertex('g');
    g.addVertex('h');
    g.addVertex('i');

    g.addEdge('a', 'b', 5);
    g.addEdge('a', 'c', 3);
    g.addEdge('a', 'e', 2);
    g.addEdge('b', 'd', 2);
    g.addEdge('c', 'b', 1);
    g.addEdge('c', 'd', 1);
    g.addEdge('d', 'a', 1);
    g.addEdge('d', 'g', 2);
    g.addEdge('d', 'h', 1);
    g.addEdge('e', 'a', 1);
    g.addEdge('e', 'h', 4);
    g.addEdge('e', 'i', 7);
    g.addEdge('f', 'b', 3);
    g.addEdge('f', 'g', 1);
    g.addEdge('g', 'c', 3);
    g.addEdge('g', 'i', 2);
    g.addEdge('h', 'c', 2);
    g.addEdge('h', 'f', 2);
    g.addEdge('h', 'g', 2);

    LinkedList<Character> path = solver.findShortestPath(g, 'a', 'i');

    Assert.assertThat(path, is(Arrays.asList('a', 'c', 'd', 'g', 'i')));
  }

}

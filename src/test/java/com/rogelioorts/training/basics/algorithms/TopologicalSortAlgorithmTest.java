package com.rogelioorts.training.basics.algorithms;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.basics.data.structures.Graph;

public class TopologicalSortAlgorithmTest {

  private TopologicalSortAlgorithm solver;

  @Before
  public void initialize() {
    solver = new TopologicalSortAlgorithm();
  }

  @Test
  public void testA() {
    Graph<Character> g = new Graph<>();
    g.addVertex('a');
    g.addVertex('b');
    g.addVertex('c');
    g.addVertex('d');
    g.addVertex('e');

    g.addEdge('b', 'a');
    g.addEdge('a', 'c');
    g.addEdge('b', 'c');
    g.addEdge('d', 'e');

    LinkedList<Character> result = solver.sort(g);

    Assert.assertThat(result, is(Arrays.asList('b', 'd', 'a', 'e', 'c')));
  }

}

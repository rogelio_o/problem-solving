package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article36Test {

  private Article36 solver;
  
  @Before
  public void initialize() {
    solver = new Article36();
  }
  
  @Test
  public void testA() {
    int[] arr = new int[41];
    for(int i = 1; i < 41; i++) {
      arr[i - 1] = i;
    }
    arr[40] = 6;
    
    Assert.assertEquals(6, solver.duplicated(arr));
  }
  
}

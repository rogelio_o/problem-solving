package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article25Test {

  private Article25 solver;
  
  @Before
  public void initialize() {
    solver = new Article25();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(4, solver.minDropsInWorstCase(2, 10));
  }
  
}

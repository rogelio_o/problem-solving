package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article5Test {

  private Article5 article5;
  
  @Before
  public void initialize() {
    article5 = new Article5();
  }
  
  @Test
  public void testA() {
    Assert.assertTrue(article5.solve("geeks", "keegs"));
  }
  
  @Test
  public void testB() {
    Assert.assertFalse(article5.solve("rsting", "string"));
  }
  
  @Test
  public void testC() {
    Assert.assertTrue(article5.solve("Converse", "Converse"));
  }
  
}

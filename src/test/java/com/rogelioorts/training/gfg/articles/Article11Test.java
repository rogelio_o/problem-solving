package com.rogelioorts.training.gfg.articles;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article11Test {

  private Article11 article11;
  
  @Before
  public void initialize() {
    article11 = new Article11();
  }
  
  @Test
  public void testA() {
    int[] input = new int[] {0, -1, 2, -3, 1};
    
    List<int[]> result = article11.solve(input, 5);
    
    Assert.assertEquals(2, result.size());
    Assert.assertArrayEquals(new int[] {-3, 1, 2}, result.get(0));
    Assert.assertArrayEquals(new int[] {-1, 0, 1}, result.get(1));
  }
  
  @Test
  public void testB() {
    int[] input = new int[] {1, -2, 1, 0, 5};
    
    List<int[]> result = article11.solve(input, 5);
    
    Assert.assertEquals(1, result.size());
    Assert.assertArrayEquals(new int[] {-2, 1, 1}, result.get(0));
  }
  
}

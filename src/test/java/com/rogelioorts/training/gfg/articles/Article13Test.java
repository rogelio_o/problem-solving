package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article13Test {

  private Article13 article13;
  
  @Before
  public void initialize() {
    article13 = new Article13();
  }
  
  @Test
  public void testA() {
    int[] coins = new int[] {5, 3, 7, 10};
    
    int result = article13.solve(coins, 4);
    Assert.assertEquals(15, result);
  }
  
  @Test
  public void testB() {
    int[] coins = new int[] {8, 15, 3, 7};
    
    int result = article13.solve(coins, 4);
    Assert.assertEquals(22, result);
  }
  
}

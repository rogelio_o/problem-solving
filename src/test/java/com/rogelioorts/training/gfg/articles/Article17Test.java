package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article17Test {

  private Article17 article17;
  
  @Before
  public void initialize() {
    article17 = new Article17();
  }
  
  @Test
  public void testA() {
    int[] input = new int[] {34, 8, 10, 3, 2, 80, 30, 33, 1};
    Assert.assertEquals(6, article17.solve(input, input.length));
  }
  
  @Test
  public void testB() {
    int[] input = new int[] {9, 2, 3, 4, 5, 6, 7, 8, 18, 0};
    Assert.assertEquals(8, article17.solve(input, input.length));
  }
  
  @Test
  public void testC() {
    int[] input = new int[] {1, 2, 3, 4, 5, 6};
    Assert.assertEquals(5, article17.solve(input, input.length));
  }@Test
  public void testD() {
    int[] input = new int[] {6, 5, 4, 3, 2, 1};
    Assert.assertEquals(-1, article17.solve(input, input.length));
  }
  
}

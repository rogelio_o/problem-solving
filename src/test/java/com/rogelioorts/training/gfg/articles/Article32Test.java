package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article32Test {

  private Article32 solver;
  
  @Before
  public void initialize() {
    solver = new Article32();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(5, solver.numSquares(36, 30));
  }
  
}

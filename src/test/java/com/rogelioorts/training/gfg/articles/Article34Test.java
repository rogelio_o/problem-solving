package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article34Test {

  private Article34 solver;
  
  @Before
  public void initialize() {
    solver = new Article34();
  }
  
  @Test
  public void testA() {
    int[] result = solver.getRangeOfSum(new int[] {1, 4, 20, 3, 10, 5}, 33);
    Assert.assertArrayEquals(new int[] {2, 4}, result);
  }
  
  @Test
  public void testB() {
    int[] result = solver.getRangeOfSum(new int[] {1, 4, 22, 3, 10, 5}, 33);
    Assert.assertNull(result);
  }
  
}

package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article1Test {
  
  private Article1 article1;
  
  @Before
  public void initialize() {
    article1 = new Article1();
  }

  @Test
  public void testA() {
    int[] input = new int[] {1, 2};
    int output = article1.solve(input, 2);
    
    Assert.assertEquals(4, output);
  }
  
  @Test
  public void testB() {
    int[] input = new int[] {1, 3, 5};
    int output = article1.solve(input, 3);
    
    Assert.assertEquals(8, output);
  }
  
}

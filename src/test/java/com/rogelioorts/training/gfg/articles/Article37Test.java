package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article37Test {

  private Article37 solver;
  
  @Before
  public void initialize() {
    solver = new Article37();
  }
  
  @Test
  public void testA() {
    int[][] screen = new int[][] {
        {1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 0, 0},
        {1, 0, 0, 1, 1, 0, 1, 1},
        {1, 2, 2, 2, 2, 0, 1, 0},
        {1, 1, 1, 2, 2, 0, 1, 0},
        {1, 1, 1, 2, 2, 2, 2, 0},
        {1, 1, 1, 1, 1, 2, 1, 1},
        {1, 1, 1, 1, 1, 2, 2, 1}
   };
   
   solver.fill(screen, 4, 4, 3);
   
   Assert.assertArrayEquals(new int[][] {
     {1, 1, 1, 1, 1, 1, 1, 1},
     {1, 1, 1, 1, 1, 1, 0, 0},
     {1, 0, 0, 1, 1, 0, 1, 1},
     {1, 3, 3, 3, 3, 0, 1, 0},
     {1, 1, 1, 3, 3, 0, 1, 0},
     {1, 1, 1, 3, 3, 3, 3, 0},
     {1, 1, 1, 1, 1, 3, 1, 1},
     {1, 1, 1, 1, 1, 3, 3, 1}
   }, screen);
  }
  
}

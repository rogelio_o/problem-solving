package com.rogelioorts.training.gfg.articles;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article27Test {

  private Article27 solver;
  
  @Before
  public void initialize() {
    solver = new Article27();
  }
  
  @Test
  public void testA() {
    int[] arr = new int[] {10, 2, 3, 4, 5, 9, 7, 8};
    
    ArrayList<int[]> result = solver.possibleSumsOf4(arr, 23);
    
    Assert.assertEquals(18, result.size());
    Assert.assertArrayEquals(new int[] {2, 3, 8, 10}, result.get(0));
  }
  
}

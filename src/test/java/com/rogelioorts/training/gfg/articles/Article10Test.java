package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article10Test {

    private Article10 article10;

    @Before
    public void initialize() {
        article10 = new Article10();
    }

    @Test
    public void testA() {
        BTreeNode<Integer> rootA = createTreeA1();
        BTreeNode<Integer> rootB = createTreeB1();

        List<Integer> result = article10.solve(rootA, rootB);
        Assert.assertThat(result, is(Arrays.asList(1, 2, 3, 4, 5, 6)));
    }

    private BTreeNode<Integer> createTreeA1() {
        BTreeNode<Integer> root = new BTreeNode<>(3);
        root.setLeft(new BTreeNode<>(1));
        root.setRight(new BTreeNode<>(5));

        return root;
    }

    private BTreeNode<Integer> createTreeB1() {
        BTreeNode<Integer> root = new BTreeNode<>(4);
        root.setLeft(new BTreeNode<>(2));
        root.setRight(new BTreeNode<>(6));

        return root;
    }

    @Test
    public void testB() {
        BTreeNode<Integer> rootA = createTreeA2();
        BTreeNode<Integer> rootB = createTreeB2();

        List<Integer> result = article10.solve(rootA, rootB);
        Assert.assertThat(result, is(Arrays.asList(0, 1, 2, 3, 5, 8, 10)));
    }

    private BTreeNode<Integer> createTreeA2() {
        BTreeNode<Integer> root = new BTreeNode<>(8);

        BTreeNode<Integer> left = new BTreeNode<>(2);
        root.setLeft(left);
        root.setRight(new BTreeNode<>(10));

        left.setLeft(new BTreeNode<>(1));

        return root;
    }

    private BTreeNode<Integer> createTreeB2() {
        BTreeNode<Integer> root = new BTreeNode<>(5);

        BTreeNode<Integer> left = new BTreeNode<>(3);
        root.setLeft(left);

        left.setLeft(new BTreeNode<>(0));

        return root;
    }

    @Test
    public void testC() {
        BTreeNode<Integer> rootA = createTreeA3();

        List<Integer> result = article10.solve(rootA, null);
        Assert.assertThat(result, is(Arrays.asList(1, 3, 4, 6, 7, 8, 10, 13, 14)));
    }

    private BTreeNode<Integer> createTreeA3() {
        BTreeNode<Integer> root = new BTreeNode<>(8);

        BTreeNode<Integer> left = new BTreeNode<>(3);
        root.setLeft(left);

        left.setLeft(new BTreeNode<>(1));

        BTreeNode<Integer> right1 = new BTreeNode<>(6);
        left.setRight(right1);

        right1.setLeft(new BTreeNode<>(4));
        right1.setRight(new BTreeNode<>(7));

        BTreeNode<Integer> right = new BTreeNode<>(10);
        root.setRight(right);

        BTreeNode<Integer> right2 = new BTreeNode<>(14);
        right.setRight(right2);

        right2.setLeft(new BTreeNode<>(13));

        return root;
    }

    @Test
    public void testD() {
        BTreeNode<Integer> rootA = createTreeA4();
        BTreeNode<Integer> rootB = createTreeB4();

        List<Integer> result = article10.solve(rootA, rootB);
        Assert.assertThat(result, is(Arrays.asList(0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14)));
    }

    private BTreeNode<Integer> createTreeA4() {
        BTreeNode<Integer> root = new BTreeNode<>(8);

        BTreeNode<Integer> left = new BTreeNode<>(3);
        root.setLeft(left);

        left.setLeft(new BTreeNode<>(1));

        BTreeNode<Integer> right1 = new BTreeNode<>(6);
        left.setRight(right1);

        right1.setLeft(new BTreeNode<>(4));
        right1.setRight(new BTreeNode<>(7));

        BTreeNode<Integer> right = new BTreeNode<>(10);
        root.setRight(right);

        BTreeNode<Integer> right2 = new BTreeNode<>(14);
        right.setRight(right2);

        right2.setLeft(new BTreeNode<>(13));

        return root;
    }

    private BTreeNode<Integer> createTreeB4() {
        BTreeNode<Integer> root = new BTreeNode<>(5);
        root.setLeft(new BTreeNode<>(0));
        root.setRight(new BTreeNode<>(9));

        return root;
    }

}

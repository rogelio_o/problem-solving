package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Test;

public class Article19Test {
  
  @Test
  public void testA() {
    Article19 article18 = new Article19(3);
    article18.reference(1);
    Assert.assertArrayEquals(new int[] {1, -1, -1}, article18.getStatus());
    article18.reference(2);
    Assert.assertArrayEquals(new int[] {2, 1, -1}, article18.getStatus());
    article18.reference(3);
    Assert.assertArrayEquals(new int[] {3, 2, 1}, article18.getStatus());
    article18.reference(4);
    Assert.assertArrayEquals(new int[] {4, 3, 2}, article18.getStatus());
    article18.reference(1);
    Assert.assertArrayEquals(new int[] {1, 4, 3}, article18.getStatus());
    article18.reference(2);
    Assert.assertArrayEquals(new int[] {2, 1, 4}, article18.getStatus());
    article18.reference(5);
    Assert.assertArrayEquals(new int[] {5, 2, 1}, article18.getStatus());
  }
  
}

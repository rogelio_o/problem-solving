package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article14Test {

  private Article14 article14;
  
  @Before
  public void initialize() {
    article14 = new Article14();
  }
  
  @Test
  public void testA() {
    String[] input = new String[] {"baa", "abcd", "abca", "cab", "cad"};
    
    List<Character> result = article14.solve(input, 4);
    Assert.assertThat(result, is(Arrays.asList('b', 'd', 'a', 'c')));
  }
  
  @Test
  public void testB() {
    String[] input = new String[] {"caa", "aaa", "aab"};
    
    List<Character> result = article14.solve(input, 3);
    Assert.assertThat(result, is(Arrays.asList('c', 'a', 'b')));
  }
  
}

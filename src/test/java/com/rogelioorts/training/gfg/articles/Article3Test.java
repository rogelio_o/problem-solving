package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article3Test {
  
  private Article3 article3;
  
  @Before
  public void initialize() {
    article3 = new Article3();
  }

  @Test
  public void testA() {
    int[][] matrix = new int[][] {
        {1,  2,  3},
        {4,  5,  6},
        {7,  8,  9}
    };
    
    article3.solve(matrix, 3);
    
    Assert.assertArrayEquals(new int[][] {
        {3,  6,  9},
        {2,  5,  8},
        {1,  4,  7}
    }, matrix);
  }
  
  @Test
  public void testB() {
    int[][] matrix = new int[][] {
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12}, 
        {13, 14, 15, 16}
    };
    
    article3.solve(matrix, 4);
    
    Assert.assertArrayEquals(new int[][] {
        {4, 8, 12, 16},
        {3, 7, 11, 15},
        {2, 6, 10, 14}, 
        {1, 5, 9, 13}
    }, matrix);
  }
  
}

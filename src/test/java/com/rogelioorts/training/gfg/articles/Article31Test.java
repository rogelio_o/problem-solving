package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article31Test {

  private Article31 solver;
  
  @Before
  public void initialize() {
    solver = new Article31();
  }
  
  @Test
  public void testA() {
    ArrayList<Article31.Interval> intervals = new ArrayList<>();
    intervals.add(new Article31.Interval(1, 3));
    intervals.add(new Article31.Interval(2, 4));
    intervals.add(new Article31.Interval(5, 7));
    intervals.add(new Article31.Interval(6, 8));
    ArrayList<Article31.Interval> result = solver.intervals(intervals);
    Assert.assertThat(
        result, 
        is(Arrays.asList(new Article31.Interval(1, 4), new Article31.Interval(5, 8)))
    );
  }
  
}

package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article20Test {

  private Article20 article20;
  
  @Before
  public void initialize() {
    article20 = new Article20();
  }
  
  @Test
  public void testA() {
   Assert.assertEquals(2, article20.solve("((()")); 
  }
  
  @Test
  public void testB() {
   Assert.assertEquals(4, article20.solve(")()())")); 
  }
  
  @Test
  public void testC() {
   Assert.assertEquals(6, article20.solve("()(()))))")); 
  }
  
}

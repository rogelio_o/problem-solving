package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article33Test {

  private Article33 solver;
  
  @Before
  public void initialize() {
    solver = new Article33();
  }
  
  @Test
  public void testA() {
    ArrayList<String> result = solver.findBinariesFromWildcard("?0");
    Assert.assertThat(result, is(Arrays.asList("00", "10")));
  }
  
  @Test
  public void testB() {
    ArrayList<String> result = solver.findBinariesFromWildcard("?01?");
    Assert.assertThat(result, is(Arrays.asList("0010", "0011", "1010", "1011")));
  }
  
}

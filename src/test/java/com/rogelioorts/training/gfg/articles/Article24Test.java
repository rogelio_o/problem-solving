package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article24Test {

  private Article24 solver;
  
  @Before
  public void initialize() {
    solver = new Article24();
  }
  
  @Test
  public void testA() {
    Assert.assertTrue(solver.isInterleaving("abc", "def", "adbcef"));
  }
  
  @Test
  public void testB() {
    Assert.assertTrue(solver.isInterleaving("abc", "adef", "adabcef"));
  }
  
  @Test
  public void testC() {
    Assert.assertFalse(solver.isInterleaving("abc", "def", "adbcfe"));
  }
  
  @Test
  public void testD() {
    Assert.assertFalse(solver.isInterleaving("abc", "def", "adbcf"));
  }
  
}

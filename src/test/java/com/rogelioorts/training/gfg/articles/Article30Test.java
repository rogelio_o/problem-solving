package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article30Test {

  private Article30 solver;
  
  @Before
  public void initialize() {
    solver = new Article30();
  }
  
  @Test
  public void testA() {
    int[] arr = new int[] {-2, -3, 4, -1, -2, 1, 5, -3};
    Assert.assertEquals(12, solver.findMaxDiffBetweenSubarrays(arr));
  }
  
  @Test
  public void testB() {
    int[] arr = new int[] {2, -1, -2, 1, -4, 2, 8};
    Assert.assertEquals(16, solver.findMaxDiffBetweenSubarrays(arr));
  }
  
}

package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article35Test {

  private Article35 solver;
  
  @Before
  public void initialize() {
    solver = new Article35();
  }
  
  @Test
  public void testA() {
    Assert.assertThat(
        solver.maxSubstring("aabbcc", 1), 
        is(Arrays.asList("aa" , "bb" , "cc"))
    );
  }
  
  @Test
  public void testB() {
    Assert.assertThat(
        solver.maxSubstring("aabbcc", 2), 
        is(Arrays.asList("aabb" , "bbcc"))
    );
  }
  
  @Test
  public void testC() {
    Assert.assertThat(
        solver.maxSubstring("aabbcc", 3), 
        is(Arrays.asList("aabbcc"))
    );
  }
  
  @Test
  public void testD() {
    Assert.assertTrue(solver.maxSubstring("aaabbb", 3).isEmpty());
  }
  
}

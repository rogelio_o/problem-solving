package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article7Test {

  private Article7 article7;
  
  @Before
  public void initialize() {
    article7 = new Article7();
  }
  
  @Test
  public void testA() {
    int result = article7.solve(3);
    
    Assert.assertEquals(19, result);
  }
  
  @Test
  public void testB() {
    int result = article7.solve(4);
    
    Assert.assertEquals(39, result);
  }
  
}

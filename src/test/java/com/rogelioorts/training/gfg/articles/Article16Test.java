package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article16Test {

  private Article16 article16;
  
  @Before
  public void initialize() {
    article16 = new Article16();
  }
  
  @Test
  public void testA() {
    int[] pages = new int[] {12, 34, 67, 90};
    int m = 2;
    
    int result = article16.solve(pages, 4, m);
    Assert.assertEquals(113, result);
  }
  
}

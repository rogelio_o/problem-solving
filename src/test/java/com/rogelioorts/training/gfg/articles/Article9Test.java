package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article9Test {

    private Article9 article9;

    @Before
    public void initialize() {
        article9 = new Article9();
    }

    @Test
    public void testA() {
        BTreeNode<Integer> root = createTree();

        int result = article9.solve(root);
        Assert.assertEquals(13997, result);
    }

    private BTreeNode<Integer> createTree() {
        BTreeNode<Integer> root = new BTreeNode<>(6);

        BTreeNode<Integer> child1Left = new BTreeNode<>(3);
        root.setLeft(child1Left);
        BTreeNode<Integer> child1Right = new BTreeNode<>(5);
        root.setRight(child1Right);

        child1Left.setLeft(new BTreeNode<>(2));
        BTreeNode<Integer> child2Right = new BTreeNode<>(5);
        child1Left.setRight(child2Right);

        child2Right.setLeft(new BTreeNode<>(7));
        child2Right.setRight(new BTreeNode<>(4));

        child1Right.setRight(new BTreeNode<>(4));

        return root;
    }

}

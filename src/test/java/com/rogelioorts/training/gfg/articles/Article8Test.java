package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article8Test {

    private Article8 article8;

    @Before
    public void initialize() {
        article8 = new Article8();
    }

    @Test
    public void testA() {
        BTreeNode<Integer> root = createBSTree();
        int result = article8.solve(root, 5, 45);

        Assert.assertEquals(3, result);
    }

    private BTreeNode<Integer> createBSTree() {
        BTreeNode<Integer> root = new BTreeNode<>(10);

        BTreeNode<Integer> fiveNode = new BTreeNode<>(5);
        root.setLeft(fiveNode);
        fiveNode.setLeft(new BTreeNode<>(1));

        BTreeNode<Integer> fivetyNode = new BTreeNode<>(50);
        root.setRight(fivetyNode);
        fivetyNode.setLeft(new BTreeNode<>(40));
        fivetyNode.setRight(new BTreeNode<>(100));

        return root;
    }

}

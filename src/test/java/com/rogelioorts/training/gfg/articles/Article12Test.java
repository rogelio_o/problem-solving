package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article12Test {

  private Article12 article12;
  
  @Before
  public void initialize() {
    article12 = new Article12();
  }
  
  @Test
  public void testA() {
    int[][] matrix = new int[][] {
      {0, 0, 1, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 0},
      {0, 0, 1, 0}
    };
    
    int result = article12.solve(matrix, 4);
    
    Assert.assertEquals(2, result);
  }
  
}

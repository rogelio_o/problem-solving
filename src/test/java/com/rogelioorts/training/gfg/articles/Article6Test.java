package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article6Test {
  
  private Article6 article6;

  @Before
  public void initialize() {
    article6 = new Article6();
  }
  
  @Test
  public void testA() {
    String result = article6.solve(new String[] {"ale", "apple", "monkey", "plea"}, "abpcplea");
    
    Assert.assertEquals("apple", result);
  }
  
  @Test
  public void testB() {
    String result = article6.solve(new String[] {"pintu", "geeksfor", "geeksgeeks", "forgeek"}, "geeksforgeeks");
    
    Assert.assertEquals("geeksgeeks", result);
  }
  
}

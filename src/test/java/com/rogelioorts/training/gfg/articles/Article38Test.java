package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article38Test {

  private Article38 solver;
  
  @Before
  public void initialize() {
    solver = new Article38();
  }
  
  @Test
  public void testA() {
    Assert.assertTrue(solver.isJumpNumber(4343456));
  }
  
  @Test
  public void testB() {
    Assert.assertFalse(solver.isJumpNumber(89098));
  }
  
}

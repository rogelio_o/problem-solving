package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article28Test {

  private Article28 solver;
  
  @Before
  public void initialize() {
    solver = new Article28();
  }
  
  @Test
  public void testA() {
    char[][] board = new char[][] {
        {'X', 'O', 'X', 'X', 'X', 'X'},
        {'X', 'O', 'X', 'X', 'O', 'X'},
        {'X', 'X', 'X', 'O', 'O', 'X'},
        {'O', 'X', 'X', 'X', 'X', 'X'},
        {'X', 'X', 'X', 'O', 'X', 'O'},
        {'O', 'O', 'X', 'O', 'O', 'O'}
    };
    
    solver.transformSurroundedO(board);
    
    Assert.assertArrayEquals(new char[][] {
        {'X', 'O', 'X', 'X', 'X', 'X'},
        {'X', 'O', 'X', 'X', 'X', 'X'},
        {'X', 'X', 'X', 'X', 'X', 'X'},
        {'O', 'X', 'X', 'X', 'X', 'X'},
        {'X', 'X', 'X', 'O', 'X', 'O'},
        {'O', 'O', 'X', 'O', 'O', 'O'}
    }, board);
  }
  
}

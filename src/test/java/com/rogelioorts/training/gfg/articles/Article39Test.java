package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article39Test {

  private Article39 solver;
  
  @Before
  public void initialize() {
    solver = new Article39();
  }
  
  @Test
  public void testA() {
    int w = 100;
    int[] values = {1, 30};
    int[] weights = {1, 50};
    
    int[] result = solver.getNumItems(values, weights, w);
    
    Assert.assertArrayEquals(new int[] {100, 0}, result);
  }
  
  @Test
  public void testB() {
    int w = 8;
    int[] values = {10, 40, 50, 70};
    int[] weights = {1, 3, 4, 5} ;
    
    int[] result = solver.getNumItems(values, weights, w);
    
    Assert.assertArrayEquals(new int[] {0, 1, 0, 1}, result);
  }
  
}

package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article22Test {
  
  private static final String[] DICTIONARY = new String[] {"i", "like", "sam", "sung", "samsung", "mobile", "ice", 
      "cream", "icecream", "man", "go", "mango", "and"};

  private Article22 article22;
  
  @Before
  public void initialize() {
    article22 = new Article22();
  }
  
  @Test
  public void testA() {
    String input = "ilikesamsungmobile";
    
    Assert.assertThat(article22.solve(DICTIONARY, input), is(Arrays.asList(
        "i like sam sung mobile",
        "i like samsung mobile"
    )));
  }
  
  @Test
  public void testB() {
    String input = "ilikeicecreamandmango";
    
    Assert.assertThat(article22.solve(DICTIONARY, input), is(Arrays.asList(
        "i like ice cream and man go",
        "i like ice cream and mango", 
        "i like icecream and man go",
        "i like icecream and mango"
    )));
  }
  
}

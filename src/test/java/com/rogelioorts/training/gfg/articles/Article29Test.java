package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article29Test {

  private Article29 solver;
  
  @Before
  public void initialize() {
    solver = new Article29();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(3, solver.maxAs(3));
  }
  
  @Test
  public void testB() {
    Assert.assertEquals(9, solver.maxAs(7));
  }
  
  @Test
  public void testC() {
    Assert.assertEquals(27, solver.maxAs(11));
  }
  
}

package com.rogelioorts.training.gfg.articles;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article23Test {

  private Article23 solver;
  
  @Before
  public void initialize() {
    solver = new Article23();
  }
  
  @Test
  public void testA() {
    String[] dictionary = new String[] {"GEEKS", "FOR", "QUIZ", "GO"};
    char[][] boggle = new char[][] {
      {'G','I','Z'},
      {'U','E','K'},
      {'Q','S','E'}
    };
    
    Assert.assertThat(
        solver.getWordsInBoggle(dictionary, boggle), 
        is(Arrays.asList("GEEKS", "QUIZ"))
    );
  }
  
}

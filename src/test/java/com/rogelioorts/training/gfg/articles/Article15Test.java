package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Article15Test {

    private Article15 article15;

    @Before
    public void initialize() {
        article15 = new Article15();
    }

    @Test
    public void testA() {
        BTreeNode<Character> root = createTree();

        boolean result = article15.solve(root);

        Assert.assertTrue(result);
    }

    private BTreeNode<Character> createTree() {
        BTreeNode<Character> root = new BTreeNode<>('A');

        BTreeNode<Character> left1 = new BTreeNode<>('B');
        root.setLeft(left1);
        left1.setLeft(new BTreeNode<>('D'));
        left1.setRight(new BTreeNode<>('E'));

        BTreeNode<Character> right1 = new BTreeNode<>('C');
        root.setRight(right1);
        BTreeNode<Character> right2 = new BTreeNode<>('B');
        right1.setRight(right2);
        right2.setLeft(new BTreeNode<>('D'));
        right2.setRight(new BTreeNode<>('E'));

        return root;
    }

}

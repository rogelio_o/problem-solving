package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article2Test {
  
  private Article2 article2;
  
  @Before
  public void initialize() {
    article2 = new Article2();
  }

  @Test
  public void testA() {
    int output = article2.solve(2, 3, 5);
    
    Assert.assertEquals(3, output);
  }
  
  @Test
  public void testB() {
    int output = article2.solve(2, 5, 13);
    
    Assert.assertEquals(6, output);
  }
  
}

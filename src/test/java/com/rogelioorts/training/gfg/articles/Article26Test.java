package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article26Test {

  private Article26 solver;
  
  @Before
  public void initialize() {
    solver = new Article26();
  }
  
  @Test
  public void testA() {
    Assert.assertEquals(1, solver.numInsertionsToPalindrome("ab"));
  }
  
  @Test
  public void testB() {
    Assert.assertEquals(0, solver.numInsertionsToPalindrome("aa"));
  }
  
  @Test
  public void testC() {
    Assert.assertEquals(3, solver.numInsertionsToPalindrome("abcd"));
  }
  
  @Test
  public void testD() {
    Assert.assertEquals(2, solver.numInsertionsToPalindrome("abcda"));
  }
  
  @Test
  public void testE() {
    Assert.assertEquals(4, solver.numInsertionsToPalindrome("abcde"));
  }
  
}

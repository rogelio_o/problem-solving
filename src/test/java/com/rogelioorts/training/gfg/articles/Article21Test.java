package com.rogelioorts.training.gfg.articles;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Article21Test {

  private Article21 article21;
  
  @Before
  public void initialize() {
    article21 = new Article21();
  }
  
  @Test
  public void testA() {
    article21.add(5);
    Assert.assertEquals(5, article21.getMedian());
    article21.add(15);
    Assert.assertEquals(10, article21.getMedian());
    article21.add(1);
    Assert.assertEquals(5, article21.getMedian());
    article21.add(3);
    Assert.assertEquals(4, article21.getMedian());
  }
  
}

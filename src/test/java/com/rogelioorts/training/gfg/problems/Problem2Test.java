package com.rogelioorts.training.gfg.problems;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem2Test {

  private Problem2 problem2;
  
  @Before
  public void initialize() {
    problem2 = new Problem2();
  }
  
  @Test
  public void testA() {
    int[] array = new int[] {1, 2, 3, 2, 1, 4};
    
    Assert.assertArrayEquals(new Integer[] {3, 4}, problem2.getNotUniqueNumbers(array));
  }
  
  @Test
  public void testB() {
    int[] array = new int[] {2, 1, 3, 2};
    
    Assert.assertArrayEquals(new Integer[] {1, 3}, problem2.getNotUniqueNumbers(array));
  }
  
}

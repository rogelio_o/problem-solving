package com.rogelioorts.training.gfg.problems;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rogelioorts.training.gfg.utils.BTreeNode;

public class Problem4Test {

    private Problem4 problem4;

    @Before
    public void initialize() {
        problem4 = new Problem4();
    }

    @Test
    public void testA() {
        BTreeNode<Integer> root = createTree();

        problem4.addsNextRight(root);

        Assert.assertNull(root.getNextRight());
        Assert.assertEquals((Integer) 5, root.getLeft().getNextRight().getValue());
        Assert.assertNull(root.getRight().getNextRight());
        Assert.assertEquals((Integer) 1, root.getLeft().getLeft().getNextRight().getValue());
        Assert.assertEquals((Integer) 2, root.getLeft().getRight().getNextRight().getValue());
        Assert.assertNull(root.getRight().getRight().getNextRight());
    }

    private BTreeNode<Integer> createTree() {
        BTreeNode<Integer> root = new BTreeNode<>(10);
        root.setLeft(new BTreeNode<>(3));
        root.setRight(new BTreeNode<>(5));
        root.getLeft().setLeft(new BTreeNode<>(4));
        root.getLeft().setRight(new BTreeNode<>(1));
        root.getRight().setRight(new BTreeNode<>(2));

        return root;
    }

}

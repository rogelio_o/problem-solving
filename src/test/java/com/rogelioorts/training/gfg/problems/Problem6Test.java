package com.rogelioorts.training.gfg.problems;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem6Test {

  private Problem6 problem6;
  
  @Before
  public void initialize() {
    problem6 = new Problem6();
  }
  
  @Test
  public void testA() {
    Assert.assertTrue(problem6.isPalindrome("I am :IronnorI Ma, i"));
  }
  
  @Test
  public void testB() {
    Assert.assertTrue(problem6.isPalindrome("Ab?/Ba"));
  }
  
}

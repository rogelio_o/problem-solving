package com.rogelioorts.training.gfg.problems;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem3Test {

  private Problem3 problem3;
  
  @Before
  public void initialize() {
    problem3 = new Problem3();
  }
  
  @Test
  public void testA() {
    String[] dictionary = {"GEEKS", "FOR", "QUIZ", "GO"};
    char[][] boggle = new char[][] {
      {'G','I','Z'},
      {'U','E','K'},
      {'Q','S','E'}
    };
    
    Assert.assertThat(
        problem3.findWordsInBoggle(dictionary, boggle, 3, 3), 
        is(Arrays.asList("GEEKS", "QUIZ"))
    );
  }
  
}

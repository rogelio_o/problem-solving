package com.rogelioorts.training.gfg.problems;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem1Test {
  
  private Problem1 problem1;
  
  @Before
  public void initialize() {
    problem1 = new Problem1();
  }

  @Test
  public void testA() {
    int[] array = new int[] {1, 2, 3, 4, 5};
    int sum = 6;
    
    Assert.assertArrayEquals(new int[] {1, 2, 3}, problem1.findFirstSubArrayWithSum(array, sum));
  }

  @Test
  public void testB() {
    int[] array = new int[] {4, 7, 1, 2, 3};
    int sum = 6;
    
    Assert.assertArrayEquals(new int[] {1, 2, 3}, problem1.findFirstSubArrayWithSum(array, sum));
  }
  
}

package com.rogelioorts.training.gfg.problems;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Problem5Test {

  private Problem5 problem5;
  
  @Before
  public void initialize() {
    problem5 = new Problem5();
  }
  
  @Test
  public void testA() {
    int[] array = new int[] {0, -1, 2, -3, 1};
    Assert.assertTrue(problem5.hasTripletsZeroSum(array, array.length));
  }
  
  @Test
  public void testB() {
    int[] array = new int[] {1, 2, 3};
    Assert.assertFalse(problem5.hasTripletsZeroSum(array, array.length));
  }
  
}
